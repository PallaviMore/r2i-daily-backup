function ExchangeRate(_self) {
	Ti.App.Properties.setString('pg', 'exchange');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen(Ti.App.Properties.getString('pg'));

	var _obj = {
		style : require('/styles/ExchangeRate').ExchangeRate, // style
		exchangeView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		tabView : null,
		tabSelView : null,
		lblGuaranteed : null,
		lblIndicative : null,
		tabSelIconView : null,
		imgTabSel : null,
		scrollableView : null,
		
		// Guaranteed Rate
		guaranteedView : null,
		guaranteedRateView : null,
		lblGuaranteedRate : null,
		lblGuaranteedPaymode : null,
		txtGuaranteedRateView : null,
		txtGuaranteedRate : null,
		doneGuaranteedMobile : null,
		lblGuaranteedRateCountry : null,
		borderViewGuaranteed1 : null,
		txtGuaranteedValueView : null,
		txtGuaranteedValue : null,
		lblGuaranteedValueCountry : null,
		borderViewGuaranteed2 : null,
		lblTransferTxt : null,
		btnGuaranteedSendMoney : null,
		lblRateInfo : null,
		lblPromo2 : null, //by sanjivani on 26 oct 16
		imgTransferFeesView : null,
		
		
		// Indicative Rate
		indicativeView : null,
		indicativeRateView : null,
		lblIndicativeRate : null,
		lblIndicativePaymode : null,
		txtIndicativeRateView : null,
		txtIndicativeRate : null,
		doneIndicativeMobile : null,
		lblIndicativeRateCountry : null,
		borderViewIndicative1 : null,
		txtIndicativeValueView : null,
		txtIndicativeValue : null,
		lblIndicativeValueCountry : null,
		borderViewIndicative2 : null,
		lblTransferTxt1 : null,
		btnIndicativeSendMoney : null,
		lblRateInfo1 : null,
		lblPromo1 : null, //by sanjivani on 26 oct 16
		imgTransferFeesView1 : null,
		
		// Variables
		tab : null,
		indicativePaymodeName : null,
		indicativePaymodeCode : null,
		guaranteedPaymodeName : null,
		guaranteedPaymodeCode : null,
		
		indicativeExchangeResult : null,
		guaranteedExchangeResult : null,
		guaranteedRateDefault : null,
		indicativeRateDefault : null,
		minGValue : null,
		maxGValue : null,
		minIValue : null,
		maxIValue : null,
		rate : null,
		feeDtls : [],
		netFee : 0,
	};
	
	_obj.tab = 'guaranteed';
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	var guaranteed_rate = '';
	var guaranteed_value = [];
	var indicative_rate = '';
	var indicative_value = [];
	
	// CreateUI
	
	_obj.exchangeView = Ti.UI.createView(_obj.style.exchangeView);
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Exchange Rates & Fees';
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	
	_obj.imgBack.addEventListener('click',function(){
		popView();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.mainView.add(_obj.headerView);
	_obj.exchangeView.add(_obj.mainView);
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.lblGuaranteed = Ti.UI.createLabel(_obj.style.lblGuaranteed);
	_obj.lblGuaranteed.text = 'Guaranteed Rate';
	_obj.lblIndicative = Ti.UI.createLabel(_obj.style.lblIndicative);
	_obj.lblIndicative.text = 'Indicative Rate';
	
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
		
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	_obj.mainView.add(_obj.tabView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.mainView.add(_obj.scrollableView);
	
	function createGuaranteedUI()
	{	
		// Guaranteed Rate
		
		// Post Login
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{		
			if(_obj.guaranteedPaymodeName !== null)
			{				
				_obj.guaranteedView = Ti.UI.createScrollView(_obj.style.guaranteedView);
				_obj.guaranteedRateView = Ti.UI.createView(_obj.style.rateView);
				_obj.lblGuaranteedRate = Ti.UI.createLabel(_obj.style.lblRate);
				_obj.lblGuaranteedPaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
				_obj.txtGuaranteedRateView = Ti.UI.createView(_obj.style.txtRateView);
				_obj.txtGuaranteedRate = Ti.UI.createTextField(_obj.style.txtRate);
				_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value';
				_obj.txtGuaranteedRate.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
				_obj.doneGuaranteedMobile = Ti.UI.createButton(_obj.style.done);
				_obj.txtGuaranteedRate.keyboardToolbar = [_obj.doneGuaranteedMobile];
				_obj.lblGuaranteedRateCountry = Ti.UI.createLabel(_obj.style.lblRateCountry);
				_obj.lblGuaranteedRateCountry.text = origSplit[1];
				_obj.borderViewGuaranteed1 = Ti.UI.createView(_obj.style.borderView);
				_obj.txtGuaranteedValueView = Ti.UI.createView(_obj.style.txtValueView);
				_obj.txtGuaranteedValue = Ti.UI.createTextField(_obj.style.txtValue);
				_obj.txtGuaranteedValue.hintText = 'Receiving amount';
				_obj.txtGuaranteedValue.editable = false;
				_obj.lblGuaranteedValueCountry = Ti.UI.createLabel(_obj.style.lblValueCountry);
				_obj.lblGuaranteedValueCountry.text = destSplit[1];
				_obj.borderViewGuaranteed2 = Ti.UI.createView(_obj.style.borderView);
				_obj.lblTransferTxt = Ti.UI.createLabel(_obj.style.lblTransferTxt);
				_obj.lblTransferTxt.text = '*Transfer Fees,Service charges and taxes apply';
				_obj.lblPromo2 = Ti.UI.createLabel(_obj.style.lblRateInfo);
				_obj.lblPromo2.color = 'RED';
				_obj.lblPromo2.text = '"Rates inclusive of extra paisa offer"';
				
				_obj.btnGuaranteedSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
				_obj.btnGuaranteedSendMoney.title = 'SEND MONEY NOW';
				_obj.lblRateInfo = Ti.UI.createLabel(_obj.style.lblRateInfo);
				_obj.lblRateInfo.text = 'Exchange rates mentioned above are fixed rates only';
				_obj.imgTransferFeesView = Ti.UI.createImageView(_obj.style.imgTransferFeesView);
			}
			
			_obj.btnGuaranteedSendMoney.addEventListener('click',function(e){
				if(Ti.App.Properties.getString('loginStatus') !== '0')
				{
					require('/utils/PageController').pageController('transfer');
				}
				else
				{
					if(Ti.App.Properties.getString('mPIN') === '1')
					{
						require('/js/Passcode').Passcode();
					}
					else
					{
						require('/js/LoginModal').LoginModal();
					}
				}
			});
			
			_obj.txtGuaranteedRate.addEventListener('change',function(e){
				
				if(_obj.txtGuaranteedRate.value.charAt(0) == '0')
				{
					_obj.txtGuaranteedRate.value = _obj.minGValue;
					return;
				}
				
				if((parseInt(_obj.txtGuaranteedRate.value) < _obj.minGValue))
				{
					_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
					_obj.txtGuaranteedValue.value = '0.00';
				}
				else if((parseInt(_obj.txtGuaranteedRate.value) > _obj.maxGValue))
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Value cannot be greater than '+_obj.maxGValue);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Value cannot be greater than '+_obj.maxGValue);
					}
					
					_obj.txtGuaranteedRate.value = _obj.maxGValue;
					_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
				}
				else
				{
					//getExchangeRate('guaranteed');
					getConvExchangeRate('guaranteed');	
				}
			});
		}
		else
		{
			// Pre login
			
			if(_obj.guaranteedPaymodeName !== null)
			{
				_obj.guaranteedView = Ti.UI.createScrollView(_obj.style.guaranteedView);
				_obj.guaranteedRateView = Ti.UI.createView(_obj.style.rateView);
				_obj.lblGuaranteedRate = Ti.UI.createLabel(_obj.style.lblRate);
				_obj.lblGuaranteedPaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
				_obj.txtGuaranteedRateView = Ti.UI.createView(_obj.style.txtRateView);
				_obj.txtGuaranteedRate = Ti.UI.createTextField(_obj.style.txtRate);
				_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value';
				_obj.txtGuaranteedRate.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
				_obj.doneGuaranteedMobile = Ti.UI.createButton(_obj.style.done);
				_obj.txtGuaranteedRate.keyboardToolbar = [_obj.doneGuaranteedMobile];
				_obj.lblGuaranteedRateCountry = Ti.UI.createLabel(_obj.style.lblRateCountry);
				_obj.lblGuaranteedRateCountry.text = origSplit[1];
				_obj.borderViewGuaranteed1 = Ti.UI.createView(_obj.style.borderView);
				_obj.txtGuaranteedValueView = Ti.UI.createView(_obj.style.txtValueView);
				_obj.txtGuaranteedValue = Ti.UI.createTextField(_obj.style.txtValue);
				_obj.txtGuaranteedValue.hintText = 'Receiving amount';
				_obj.txtGuaranteedValue.editable = false;
				_obj.lblGuaranteedValueCountry = Ti.UI.createLabel(_obj.style.lblValueCountry);
				_obj.lblGuaranteedValueCountry.text = destSplit[1];
				_obj.borderViewGuaranteed2 = Ti.UI.createView(_obj.style.borderView);
				_obj.lblTransferTxt = Ti.UI.createLabel(_obj.style.lblTransferTxt);
				_obj.lblTransferTxt.text = '*Transfer Fees,Service charges and taxes apply';
				_obj.lblPromo2 = Ti.UI.createLabel(_obj.style.lblRateInfo);
				_obj.lblPromo2.color = 'RED';
				_obj.lblPromo2.text = '"Rates inclusive of extra paisa offer"'; //Added by sanjivani on 27 Oct 16
				_obj.btnGuaranteedSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
				_obj.btnGuaranteedSendMoney.title = 'SEND MONEY NOW';
				_obj.lblRateInfo = Ti.UI.createLabel(_obj.style.lblRateInfo);
				_obj.lblRateInfo.text = 'Exchange rates mentioned above are fixed rates only';
				
				_obj.imgTransferFeesView = Ti.UI.createImageView(_obj.style.imgTransferFeesView);
				
			}
			
			_obj.btnGuaranteedSendMoney.addEventListener('click',function(e){
				if(Ti.App.Properties.getString('loginStatus') !== '0')
				{
					require('/utils/PageController').pageController('transfer');
				}
				else
				{
					if(Ti.App.Properties.getString('mPIN') === '1')
					{
						require('/js/Passcode').Passcode();
					}
					else
					{
						require('/js/LoginModal').LoginModal();
					}
				}
			});
			
			_obj.txtGuaranteedRate.addEventListener('change',function(e){
				
				if(_obj.txtGuaranteedRate.value.charAt(0) == '0')
				{
					_obj.txtGuaranteedRate.value = _obj.minGValue;
					return;
				}
				
				if((parseInt(_obj.txtGuaranteedRate.value) < _obj.minGValue))
				{
					_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
					_obj.txtGuaranteedValue.value = '0.00';
				}
				else if((parseInt(_obj.txtGuaranteedRate.value) > _obj.maxGValue))
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Value cannot be greater than '+_obj.maxGValue);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Value cannot be greater than '+_obj.maxGValue);
					}
					
					_obj.txtGuaranteedRate.value = _obj.maxGValue;
					_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
				}
				/*else if(_obj.txtGuaranteedRate.value.length === 1)
				{
					_obj.txtGuaranteedRate.value = _obj.minGValue;
				}*/
				else
				{
					getConvExchangeRate('guaranteed');	
				}
			});
			
		}
			
		_obj.guaranteedRateView.add(_obj.lblGuaranteedRate);
		_obj.guaranteedRateView.add(_obj.lblGuaranteedPaymode);
		_obj.guaranteedView.add(_obj.guaranteedRateView);
		_obj.txtGuaranteedRateView.add(_obj.txtGuaranteedRate);
		_obj.txtGuaranteedRateView.add(_obj.lblGuaranteedRateCountry);
		_obj.guaranteedView.add(_obj.txtGuaranteedRateView);
		_obj.guaranteedView.add(_obj.borderViewGuaranteed1);
		_obj.txtGuaranteedValueView.add(_obj.txtGuaranteedValue);
		_obj.txtGuaranteedValueView.add(_obj.lblGuaranteedValueCountry);
		_obj.guaranteedView.add(_obj.txtGuaranteedValueView);
		_obj.guaranteedView.add(_obj.borderViewGuaranteed2);
		_obj.guaranteedView.add(_obj.lblTransferTxt);
		if(origSplit[0] == 'US'){
		_obj.guaranteedView.add(_obj.lblPromo2); //27-oct-16 by sanjivani
		}
		_obj.guaranteedView.add(_obj.btnGuaranteedSendMoney);
		_obj.guaranteedView.add(_obj.lblRateInfo);
		
		
		_obj.guaranteedView.add(_obj.imgTransferFeesView);
		
		_obj.doneGuaranteedMobile.addEventListener('click',function(){
			_obj.txtGuaranteedRate.blur();
		});
		
		_obj.imgTransferFeesView.addEventListener('click',function(){
			require('/js/TransferFeesModal').TransferFeesModal();
		});
		
		if(_obj.guaranteedPaymodeName !== null && _obj.guaranteedPaymodeCode !== null){
			_obj.scrollableView.addView(_obj.guaranteedView);
		}
	}
		
	// Indicative Rate
	
	function createIndicativeUI()
	{
		// Post Login
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{
			try{		
			if(_obj.indicativePaymodeName !== null)
			{
				_obj.indicativeView = Ti.UI.createScrollView(_obj.style.indicativeView);
				_obj.indicativeRateView = Ti.UI.createView(_obj.style.rateView);
				_obj.lblIndicativeRate = Ti.UI.createLabel(_obj.style.lblRate);
				_obj.lblIndicativePaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
				_obj.txtIndicativeRateView = Ti.UI.createView(_obj.style.txtRateView);
				_obj.txtIndicativeRate = Ti.UI.createTextField(_obj.style.txtRate);
				_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value';
				_obj.txtIndicativeRate.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
				_obj.doneIndicativeMobile = Ti.UI.createButton(_obj.style.done);
				_obj.txtIndicativeRate.keyboardToolbar = [_obj.doneIndicativeMobile];
				_obj.lblIndicativeRateCountry = Ti.UI.createLabel(_obj.style.lblRateCountry);
				_obj.lblIndicativeRateCountry.text = origSplit[1];
				_obj.borderViewIndicative1 = Ti.UI.createView(_obj.style.borderView);
				_obj.txtIndicativeValueView = Ti.UI.createView(_obj.style.txtValueView);
				_obj.txtIndicativeValue = Ti.UI.createTextField(_obj.style.txtValue);
				_obj.txtIndicativeValue.hintText = 'Receiving amount';
				_obj.txtIndicativeValue.editable = false;
				_obj.lblIndicativeValueCountry = Ti.UI.createLabel(_obj.style.lblValueCountry);
				_obj.lblIndicativeValueCountry.text = destSplit[1];
				_obj.borderViewIndicative2 = Ti.UI.createView(_obj.style.borderView);
				_obj.lblTransferTxt1 = Ti.UI.createLabel(_obj.style.lblTransferTxt);
				_obj.lblTransferTxt1.text = '*Transfer Fees,Service charges and taxes apply';
				_obj.lblPromo1 = Ti.UI.createLabel(_obj.style.lblRateInfo);///Added by sanjivani on 27 Oct 16
				_obj.lblPromo1.color = 'RED';                               ///Added by sanjivani on 27 Oct 16
				_obj.lblPromo1.text = '"Rates inclusive of extra paisa offer"'; //Added by sanjivani on 27 Oct 16
				_obj.btnIndicativeSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
				_obj.btnIndicativeSendMoney.title = 'SEND MONEY NOW';
				_obj.lblRateInfo1 = Ti.UI.createLabel(_obj.style.lblRateInfo);
				_obj.lblRateInfo1.text = 'Exchange rates mentioned above are indicative rates only. The actual rate applied to your transaction need not be the same on the site as rates change on a dynamic basis, several times a day. Send more and get higher exchange rates';
				_obj.imgTransferFeesView1 = Ti.UI.createImageView(_obj.style.imgTransferFeesView);
			}
			
			_obj.btnIndicativeSendMoney.addEventListener('click',function(e){
				if(Ti.App.Properties.getString('loginStatus') !== '0')
				{
					require('/utils/PageController').pageController('transfer');
				}
				else
				{
					if(Ti.App.Properties.getString('mPIN') === '1')
					{
						require('/js/Passcode').Passcode();
					}
					else
					{
						require('/js/LoginModal').LoginModal();
					}
				}
			});
			
			_obj.txtIndicativeRate.addEventListener('change',function(e){
				
				if(_obj.txtIndicativeRate.value.charAt(0) == '0')
				{
					_obj.txtIndicativeRate.value = _obj.minIValue;
					return;
				}
				
				if((parseInt(_obj.txtIndicativeRate.value) < _obj.minIValue))
				{
					_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minIValue + ')';
					_obj.txtIndicativeValue.value = '0.00';
				}
				else if((parseInt(_obj.txtIndicativeRate.value) > _obj.maxIValue))
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Value cannot be greater than '+_obj.maxIValue);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Value cannot be greater than '+_obj.maxIValue);
					}
					
					_obj.txtIndicativeRate.value = _obj.maxIValue;
				}
				/*else if(_obj.txtIndicativeRate.value.length === 1)
				{
					_obj.txtIndicativeRate.value = _obj.minIValue;
				}*/
				else
				{
					//getExchangeRate('indicative');
					getConvExchangeRate('indicative');	
				}
			});
			}catch(e){}
		}
		else
		{
			try{
			if(_obj.indicativePaymodeName !== null)
			{
				// Pre login
				_obj.indicativeView = Ti.UI.createScrollView(_obj.style.indicativeView);
				_obj.indicativeRateView = Ti.UI.createView(_obj.style.rateView);
				_obj.lblIndicativeRate = Ti.UI.createLabel(_obj.style.lblRate);
				_obj.lblIndicativePaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
				_obj.txtIndicativeRateView = Ti.UI.createView(_obj.style.txtRateView);
				_obj.txtIndicativeRate = Ti.UI.createTextField(_obj.style.txtRate);
				_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value';
				_obj.txtIndicativeRate.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
				_obj.doneIndicativeMobile = Ti.UI.createButton(_obj.style.done);
				_obj.txtIndicativeRate.keyboardToolbar = [_obj.doneIndicativeMobile];
				_obj.lblIndicativeRateCountry = Ti.UI.createLabel(_obj.style.lblRateCountry);
				_obj.lblIndicativeRateCountry.text = origSplit[1];
				_obj.borderViewIndicative1 = Ti.UI.createView(_obj.style.borderView);
				_obj.txtIndicativeValueView = Ti.UI.createView(_obj.style.txtValueView);
				_obj.txtIndicativeValue = Ti.UI.createTextField(_obj.style.txtValue);
				_obj.txtIndicativeValue.hintText = 'Receiving amount';
				_obj.txtIndicativeValue.editable = false;
				_obj.lblIndicativeValueCountry = Ti.UI.createLabel(_obj.style.lblValueCountry);
				_obj.lblIndicativeValueCountry.text = destSplit[1];
				_obj.borderViewIndicative2 = Ti.UI.createView(_obj.style.borderView);
				_obj.lblTransferTxt1 = Ti.UI.createLabel(_obj.style.lblTransferTxt);
				_obj.lblTransferTxt1.text = '*Transfer Fees,Service charges and taxes apply';
				_obj.lblPromo1 = Ti.UI.createLabel(_obj.style.lblRateInfo);///Added by sanjivani on 27 Oct 16
				_obj.lblPromo1.color = 'RED';                               ///Added by sanjivani on 27 Oct 16
				_obj.lblPromo1.text = '"Rates inclusive of extra paisa offer"'; //Added by sanjivani on 27 Oct 16
				_obj.btnIndicativeSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
				_obj.btnIndicativeSendMoney.title = 'SEND MONEY NOW';
				_obj.lblRateInfo1 = Ti.UI.createLabel(_obj.style.lblRateInfo);
				_obj.lblRateInfo1.text = 'Exchange rates mentioned above are indicative rates only. The actual rate applied to your transaction need not be the same on the site as rates change on a dynamic basis, several times a day. Send more and get higher exchange rates';
				_obj.imgTransferFeesView1 = Ti.UI.createImageView(_obj.style.imgTransferFeesView);
			}
			
			_obj.btnIndicativeSendMoney.addEventListener('click',function(e){
				if(Ti.App.Properties.getString('loginStatus') !== '0')
				{
					require('/utils/PageController').pageController('transfer');
				}
				else
				{
					if(Ti.App.Properties.getString('mPIN') === '1')
					{
						require('/js/Passcode').Passcode();
					}
					else
					{
						require('/js/LoginModal').LoginModal();
					}
				}
			});
			
			_obj.txtIndicativeRate.addEventListener('change',function(e){
				
				if(_obj.txtIndicativeRate.value.charAt(0) == '0')
				{
					_obj.txtIndicativeRate.value = _obj.minIValue;
					return;
				}
				
				if((parseInt(_obj.txtIndicativeRate.value) < _obj.minIValue))
				{
					_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minIValue + ')';
					_obj.txtIndicativeValue.value = '0.00';
				}
				else if((parseInt(_obj.txtIndicativeRate.value) > _obj.maxIValue))
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Value cannot be greater than '+_obj.maxIValue);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Value cannot be greater than '+_obj.maxIValue);
					}
					
					_obj.txtIndicativeRate.value = _obj.maxIValue;
				}
				/*else if(_obj.txtIndicativeRate.value.length === 1)
				{
					_obj.txtIndicativeRate.value = _obj.minIValue;
				}*/
				else
				{
					getConvExchangeRate('indicative');	
				}
			});
			}catch(e){}
		}	
		_obj.indicativeRateView.add(_obj.lblIndicativeRate);
		_obj.indicativeRateView.add(_obj.lblIndicativePaymode);
		_obj.indicativeView.add(_obj.indicativeRateView);
		_obj.txtIndicativeRateView.add(_obj.txtIndicativeRate);
		_obj.txtIndicativeRateView.add(_obj.lblIndicativeRateCountry);
		_obj.indicativeView.add(_obj.txtIndicativeRateView);
		_obj.indicativeView.add(_obj.borderViewIndicative1);
		_obj.txtIndicativeValueView.add(_obj.txtIndicativeValue);
		_obj.txtIndicativeValueView.add(_obj.lblIndicativeValueCountry);
		_obj.indicativeView.add(_obj.txtIndicativeValueView);
		_obj.indicativeView.add(_obj.borderViewIndicative2);
		_obj.indicativeView.add(_obj.lblTransferTxt1);
		if(origSplit[0] == 'US'){
			_obj.indicativeView.add(_obj.lblPromo1); //27-oct-16 by sanjivani
			}
		_obj.indicativeView.add(_obj.btnIndicativeSendMoney);
		_obj.indicativeView.add(_obj.lblRateInfo1);
		_obj.indicativeView.add(_obj.imgTransferFeesView1);
		
		_obj.doneIndicativeMobile.addEventListener('click',function(){
			_obj.txtIndicativeRate.blur();
		});
		
		_obj.imgTransferFeesView1.addEventListener('click',function(){
			require('/js/TransferFeesModal').TransferFeesModal();
		});
		try{
		if(_obj.indicativePaymodeName !== null && _obj.indicativePaymodeCode !== null){
			_obj.scrollableView.addView(_obj.indicativeView);
		}
		}catch(e){}
	}
	
	// Call UI
	try{
	if(Ti.App.Properties.getString('guaranteedPaymodeName') !== '' && Ti.App.Properties.getString('indicativePaymodeName') !== '')
	{
		_obj.indicativePaymodeName = Ti.App.Properties.getString('indicativePaymodeName');
		_obj.indicativePaymodeCode = Ti.App.Properties.getString('indicativePaymodeCode');
		_obj.guaranteedPaymodeName = Ti.App.Properties.getString('guaranteedPaymodeName');
		_obj.guaranteedPaymodeCode = Ti.App.Properties.getString('guaranteedPaymodeCode');
		
		createGuaranteedUI();
		createIndicativeUI();
		
		_obj.tabView.add(_obj.lblIndicative);
		_obj.tabView.add(_obj.tabSelView);
		_obj.tabView.add(_obj.lblGuaranteed);
		_obj.tabView.add(_obj.tabSelView);
		_obj.mainView.add(_obj.tabSelIconView);
		
	}
	else if(Ti.App.Properties.getString('guaranteedPaymodeName') !== '')
	{
		_obj.guaranteedPaymodeName = Ti.App.Properties.getString('guaranteedPaymodeName');
		_obj.guaranteedPaymodeCode = Ti.App.Properties.getString('guaranteedPaymodeCode');
		createGuaranteedUI();
		
		_obj.lblGuaranteed.width = '100%';
		_obj.tabView.width = '100%';
		_obj.tabSelIconView.width = '100%';
		_obj.tabSelIconView.left = 0;
		_obj.tabSelIconView.right = 0;
		_obj.tabSelView.width = '100%';
		_obj.tabSelView.left = 0;
		_obj.tabSelView.right = 0;
		
		_obj.tabView.add(_obj.lblGuaranteed);
		_obj.tabView.add(_obj.tabSelView);
		_obj.mainView.add(_obj.tabSelIconView);
	}
	else if(Ti.App.Properties.getString('indicativePaymodeName') !== '')
	{
		_obj.indicativePaymodeName = Ti.App.Properties.getString('indicativePaymodeName');
		_obj.indicativePaymodeCode = Ti.App.Properties.getString('indicativePaymodeCode');	        	
		createIndicativeUI();
		
		_obj.lblIndicative.width = '100%';
		_obj.tabView.width = '100%';
		_obj.tabSelIconView.width = '100%';
		_obj.tabSelIconView.left = 0;
		_obj.tabSelIconView.right = 0;
		_obj.tabSelView.width = '100%';
		_obj.tabSelView.left = 0;
		_obj.tabSelView.right = 0;
		
		_obj.tabView.add(_obj.lblIndicative);
		_obj.tabView.add(_obj.tabSelView);
		_obj.mainView.add(_obj.tabSelIconView);
	}
	else
	{
		
	}}catch(e){}
	
	function changeTabs(selected)
	{
		if(selected === 'guaranteed')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '50%';
			_obj.lblGuaranteed.color = TiFonts.FontStyle('whiteFont');
			_obj.lblGuaranteed.backgroundColor = '#6F6F6F';
			_obj.lblIndicative.color = TiFonts.FontStyle('blackFont');
			_obj.lblIndicative.backgroundColor = '#E9E9E9';
		}
		else
		{
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			_obj.tabSelIconView.width = '50%';
			_obj.lblIndicative.color = TiFonts.FontStyle('whiteFont');
			_obj.lblIndicative.backgroundColor = '#6F6F6F';
			_obj.lblGuaranteed.color = TiFonts.FontStyle('blackFont');
			_obj.lblGuaranteed.backgroundColor = '#E9E9E9';
		}
	}
	
	_obj.tabView.addEventListener('click',function(e){
		if(e.source.sel === 'guaranteed' && _obj.tab !== 'guaranteed')
		{
			_obj.scrollableView.movePrevious();
		}
		
		if(e.source.sel === 'indicative' && _obj.tab !== 'indicative')
		{
			_obj.scrollableView.moveNext();
		}
	});
	
	_obj.scrollableView.addEventListener('scroll',function(e){
		try{
		if(_obj.indicativePaymodeName !== null && _obj.indicativePaymodeCode !== null && _obj.guaranteedPaymodeName !== null && _obj.guaranteedPaymodeCode !== null)
		{
			if(e.currentPage === 0)
			{
				changeTabs('guaranteed');
				_obj.tab = 'guaranteed';
			}
			
			if(e.currentPage === 1)
			{
				changeTabs('indicative');
				_obj.tab = 'indicative';
			}
		}
		}catch(e){}
	});
	
	// Call Corridor
	
	function corridorPaymode() {
				
		// For Post Login
					
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{		
			if(_obj.guaranteedPaymodeName !== null)
			{
				activityIndicator.showIndicator();
				var xhr1 = require('/utils/XHR');
				
				xhr1.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"originatingCountryCode":"'+origSplit[0]+'",'+ 
						'"originatingCurrencyCode":"'+origSplit[1]+'",'+
						'"destinationCountryCode":"'+destSplit[0]+'",'+
						'"destinationCurrencyCode":"'+destSplit[1]+'",'+
						'"payModeCode":"'+_obj.guaranteedPaymodeCode+'",'+
						'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
						'}',
					success : xhr1Success,
					error : xhr1Error,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhr1Success(evt) {
					try{
						require('/utils/Console').info('Result ======== ' + evt.result);
						
						activityIndicator.hideIndicator();
						
						if(evt.result.responseFlag === "S")
						{
							_obj.guaranteedExchangeResult = evt.result;
							_obj.lblGuaranteedPaymode.text = 'For ' + _obj.guaranteedPaymodeName;
							guaranteed_rate = parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate);
							
							if(_obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit !== '' && _obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit !== null)
							{
								_obj.guaranteed_rate = (Math.floor((parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate)+parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit)) * 100)/100);						
							}
							else
							{
								_obj.guaranteed_rate = ((Math.floor(parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate)) * 100)/100);
							}
							
							guaranteed_value = [];
							
							try{
						     	for(var gr = 0; gr < _obj.guaranteedExchangeResult.exchangeRateDtls.length; gr++){
						     		guaranteed_value.push(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + (Math.floor((parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].netConversionRate)+parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].fxBenefit)) * 100)/100));
						     	}
						     	
						     	for(var fd = 0; fd < _obj.guaranteedExchangeResult.feeDtls.length; fd++)
					     		{
					     			_obj.feeDtls.push(_obj.guaranteedExchangeResult.feeDtls[fd].rangeFrom + '-' + _obj.guaranteedExchangeResult.feeDtls[fd].rangeTo + '-' + _obj.guaranteedExchangeResult.feeDtls[fd].netFee);
					     		}
						     	
						    }catch(e){
						    	for(var gr = 0; gr < _obj.guaranteedExchangeResult.exchangeRateDtls.length; gr++){
						     		guaranteed_value.push(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].netConversionRate + '~' + '0');
						     	}
						    }
							
							var minExchange = [];
	     					var maxExchange = [];
					     	
					     	for(var s=0; s<_obj.guaranteedExchangeResult.exchangeRateDtls.length;s++)
	     					{
	     						minExchange.push(parseInt(_obj.guaranteedExchangeResult.exchangeRateDtls[s].rangeFrom));
	     						maxExchange.push(parseInt(_obj.guaranteedExchangeResult.exchangeRateDtls[s].rangeTo));
	     					}
	     					
	     					minExchange.sort(function(a,b){return a-b;});
	     					maxExchange.sort(function(a,b){return b-a;});
	     					
	     					_obj.minGValue = minExchange[0];
	     					_obj.maxGValue = maxExchange[0];
					     	
					     	Ti.API.info(_obj.minGValue+'-------'+_obj.maxGValue);
					     /*	if(_obj.maxGValue == 3000)
					     		{
					     		 _obj.guaranteed_rate = 95.88;
					     		 Ti.API.info("Guaranteed rate is " +_obj.guaranteed_rate);
					     		}*/
					     	
					     	_obj.rate = _obj.rate + '#' +  _obj.guaranteed_rate + '-' + destSplit[1] + '-' + 'Guaranteed';
					     	//_obj.rate = _obj.rate + '#' + '95.88'+ '-' + destSplit[1] + '-' + 'Guaranteed';
					     	
							_obj.txtGuaranteedRate.value = _obj.minGValue;
							
							_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + _obj.guaranteed_rate + ' ' + destSplit[1];
							_obj.guaranteedRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.guaranteed_rate + ' ' + destSplit[1];	
							
							_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
							try{
								var strFeeDtl = [];
								
								if(_obj.feeDtls.length > 0)
								{
									for(var fd = 0; fd < _obj.feeDtls.length; fd++)
						     		{
						     			strFeeDtl = _obj.feeDtls[fd].split('-');
						     			
						     			if(parseInt(_obj.minGValue) >= parseInt(strFeeDtl[0]) && parseInt(_obj.minGValue) <= parseInt(strFeeDtl[1])){	
											require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
											_obj.netFee = (Math.floor(parseFloat(strFeeDtl[2]))*100/100);
											break;
										}
						     		}
						     	}
					     		
					     		/*var amt = ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate));
					     		
					     		setTimeout(function(){
									Ti.API.info('IF ' + ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate)));
						     		_obj.txtGuaranteedValue.value = amt.toString();	
								},200);*/
					     		
						     }catch(e){
						     	/*setTimeout(function(){
									Ti.API.info('ELSE ' + ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate)));
						     		_obj.txtGuaranteedValue.value = (_obj.minGValue*parseFloat(_obj.guaranteed_rate));	
								},200);*/
						     }
						}
						xhr1 = null;
					}catch(e){}
				}

				function xhr1Error(evt) {
					try{
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr1 = null;
					}catch(e){}
				}
				
			}
			
			try{
			if(_obj.indicativePaymodeName !== null)
			{
				
					activityIndicator.showIndicator();
					var xhr2 = require('/utils/XHR');

					xhr2.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"originatingCountryCode":"'+origSplit[0]+'",'+ 
							'"originatingCurrencyCode":"'+origSplit[1]+'",'+
							'"destinationCountryCode":"'+destSplit[0]+'",'+
							'"destinationCurrencyCode":"'+destSplit[1]+'",'+
							'"payModeCode":"'+_obj.indicativePaymodeCode+'",'+
							'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
							'}',
						success : xhr2Success,
						error : xhr2Error,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
	
					function xhr2Success(evt) {
						try{
							require('/utils/Console').info('Result ======== ' + evt.result);
							
							activityIndicator.hideIndicator();
							if(evt.result.responseFlag === "S")
							{
								
								_obj.indicativeExchangeResult = evt.result;
								_obj.lblIndicativePaymode.text = 'For ' + _obj.indicativePaymodeName;
								
								if(_obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit !== '' && _obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit !== null)
								{
									_obj.indicative_rate = (Math.floor((parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate)+parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit)) * 100)/100);						
								}
								else
								{
									_obj.indicative_rate = ((Math.floor(parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate)) * 100)/100);
								}
								
								indicative_value = [];
								
								try{
							     	for(var gr = 0; gr < _obj.indicativeExchangeResult.exchangeRateDtls.length; gr++){
							     		indicative_value.push(_obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + (Math.floor((parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[gr].netConversionRate)+parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[gr].fxBenefit)) * 100)/100));
							     	}
							     	
							     	for(var fd = 0; fd < _obj.indicativeExchangeResult.feeDtls.length; fd++)
						     		{
						     			_obj.feeDtls.push(_obj.indicativeExchangeResult.feeDtls[fd].rangeFrom + '-' + _obj.indicativeExchangeResult.feeDtls[fd].rangeTo + '-' + _obj.indicativeExchangeResult.feeDtls[fd].netFee);
						     		}
							     	
							    }catch(e){
							    	for(var gr = 0; gr < _obj.indicativeExchangeResult.exchangeRateDtls.length; gr++){
							     		indicative_value.push(_obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].netConversionRate + '~' + '0');
							     	}
							    }
							    
							    var minExchange = [];
	     						var maxExchange = [];
							    
							    for(var s=0; s<_obj.indicativeExchangeResult.exchangeRateDtls.length;s++)
		     					{
		     						minExchange.push(parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[s].rangeFrom));
		     						maxExchange.push(parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[s].rangeTo));
		     					}
		     					
		     					minExchange.sort(function(a,b){return a-b;});
		     					maxExchange.sort(function(a,b){return b-a;});
		     					
		     					_obj.minIValue = minExchange[0];
		     					_obj.maxIValue = maxExchange[0];
								
						     	_obj.rate = _obj.rate + '#' +  _obj.indicative_rate + '-' + destSplit[1] + '-' + 'Indicative';
								_obj.txtIndicativeRate.value = _obj.minIValue;
								
								_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];
								_obj.indicativeRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];	
								
								_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minIValue + ')';
								try{
									var strFeeDtl = [];
									
									if(_obj.feeDtls.length > 0)
									{
										for(var fd = 0; fd < _obj.feeDtls.length; fd++)
							     		{
							     			strFeeDtl = _obj.feeDtls[fd].split('-');
							     			
							     			if(parseInt(_obj.minIValue) >= parseInt(strFeeDtl[0]) && parseInt(_obj.minIValue) <= parseInt(strFeeDtl[1])){	
												require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
												_obj.netFee = (Math.floor(parseFloat(strFeeDtl[2]))*100/100);
												break;
											}
							     		}
							     	}
						     		
						     		var amt = ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate));
									//Sanju code here
									//var amt = ((parseFloat(_obj.minIValue))*(_obj.indicative_rate));
									
									                        
						     		_obj.txtIndicativeValue.value = amt.toString();
									
							     }catch(e){
							     	Ti.API.info('ELSE ' + ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate)));
							     	//Ti.API.info('ELSE ' + (parseFloat(_obj.minIValue)*(_obj.indicative_rate)));
							     	_obj.txtIndicativeValue.value = (_obj.minIValue*parseFloat(_obj.indicative_rate));
							     }
							}
							xhr2 = null;
						}catch(e){}
					}
	
					function xhr2Error(evt) {
						try{
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr2 = null;
						}catch(e){}
					}
				
			}
			}catch(e){}
			
			
			/*if(_obj.guaranteedPaymodeName !== null)
			{
				///////////// Call Pre login to get min and max amount /////////////
				
				activityIndicator.showIndicator();
				var xhrPre = require('/utils/XHR');
				
				xhrPre.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"originatingCountryCode":"'+origSplit[0]+'",'+ 
						'"originatingCurrencyCode":"'+origSplit[1]+'",'+
						'"destinationCountryCode":"'+destSplit[0]+'",'+
						'"destinationCurrencyCode":"'+destSplit[1]+'",'+
						'"payModeCode":"'+_obj.guaranteedPaymodeCode+'",'+
						'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
						'}',
					success : xhrPreSuccess,
					error : xhrPreError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhrPreSuccess(evt) {
					try{
						require('/utils/Console').info('Result ======== ' + evt.result);
						
						activityIndicator.hideIndicator();
						
						var minExchange = [];
						var maxExchange = [];
						
						if(evt.result.responseFlag === "S")
						{
							for(var s=0; s<evt.result.exchangeRateDtls.length;s++)
	     					{
	     						minExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeFrom));
	     						maxExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeTo));
	     					}
	     					
	     					minExchange.sort(function(a,b){return a-b;});
	     					maxExchange.sort(function(a,b){return b-a;});
	     					
	     					_obj.minGValue = minExchange[0];
	     					_obj.maxGValue = maxExchange[0];
	     					
	     					TiGlobals.minExchangeValue = _obj.minGValue; 
							TiGlobals.maxExchangeValue = _obj.maxGValue;
	     					
							activityIndicator.showIndicator();
							var xhr1 = require('/utils/XHR');
	
							xhr1.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"GETEXCHANGERATE",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"originatingCountry":"'+origSplit[0]+'",'+ 
									'"originatingCurrency":"'+origSplit[1]+'",'+
									'"destinationCountry":"'+destSplit[0]+'",'+
									'"destinationCurrency":"'+destSplit[1]+'",'+
									'"paymodeCode":"'+_obj.guaranteedPaymodeCode+'",'+
									'"amount":"'+_obj.minGValue+'",'+
									'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
									'}',
								success : xhr1Success,
								error : xhr1Error,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
			
							function xhr1Success(evtG1) {
								try{
									require('/utils/Console').info('Result ======== ' + evtG1.result);
									
									activityIndicator.hideIndicator();
									
									if(evtG1.result.responseFlag === "S")
									{
										_obj.guaranteedExchangeResult = evtG1.result;
										
										_obj.lblGuaranteedPaymode.text = 'For ' + _obj.guaranteedPaymodeName;
										_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + _obj.guaranteedExchangeResult.exchangeRate + ' ' + destSplit[1];
										_obj.guaranteedRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.guaranteedExchangeResult.exchangeRate + ' ' + destSplit[1];
										_obj.txtGuaranteedRate.value = _obj.minGValue;
										_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
										_obj.txtGuaranteedValue.value = parseFloat(_obj.guaranteedExchangeResult.indicativeDestAmount).toFixed(2);
										
									}
									xhr1 = null;
								}catch(e){}
							}
			
							function xhr1Error(evtG1) {
								try{
									activityIndicator.hideIndicator();
									require('/utils/Network').Network();
									xhr1 = null;
								}catch(e){}
							}
							
						}
						xhrPre = null;
					}catch(e){}
				}

				function xhrPreError(evt) {
					try{
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhrPre = null;
					}catch(e){}
				}
				
				////////////////////////////////////////////////////////////////////
				
			}
			
			if(_obj.indicativePaymodeName !== null)
			{
				///////////// Call Pre login to get min and max amount /////////////
				
				activityIndicator.showIndicator();
				var xhrPre2 = require('/utils/XHR');

				xhrPre2.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"originatingCountryCode":"'+origSplit[0]+'",'+ 
						'"originatingCurrencyCode":"'+origSplit[1]+'",'+
						'"destinationCountryCode":"'+destSplit[0]+'",'+
						'"destinationCurrencyCode":"'+destSplit[1]+'",'+
						'"payModeCode":"'+_obj.indicativePaymodeCode+'",'+
						'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
						'}',
					success : xhrPre2Success,
					error : xhrPre2Error,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhrPre2Success(evt) {
					try{
						require('/utils/Console').info('Result ======== ' + evt.result);
						
						activityIndicator.hideIndicator();
						
						var minExchange = [];
						var maxExchange = [];
						
						if(evt.result.responseFlag === "S")
						{
							for(var s=0; s<evt.result.exchangeRateDtls.length;s++)
	     					{
	     						minExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeFrom));
	     						maxExchange.push(parseInt(evt.result.exchangeRateDtls[s].rangeTo));
	     					}
	     					
	     					minExchange.sort(function(a,b){return a-b;});
	     					maxExchange.sort(function(a,b){return b-a;});
	     					
	     					_obj.minIValue = minExchange[0];
	     					_obj.maxIValue = maxExchange[0];
	     					
	     					TiGlobals.minExchangeValue = _obj.minIValue; 
							TiGlobals.maxExchangeValue = _obj.maxIValue;
	     					
	     					activityIndicator.showIndicator();
							var xhr2 = require('/utils/XHR');
	
							xhr2.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"GETEXCHANGERATE",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"originatingCountry":"'+origSplit[0]+'",'+ 
									'"originatingCurrency":"'+origSplit[1]+'",'+
									'"destinationCountry":"'+destSplit[0]+'",'+
									'"destinationCurrency":"'+destSplit[1]+'",'+
									'"paymodeCode":"'+_obj.indicativePaymodeCode+'",'+
									'"amount":"'+_obj.minIValue+'",'+
									'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
									'}',
								success : xhr2Success,
								error : xhr2Error,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
			
							function xhr2Success(evtI1) {
								try{
									require('/utils/Console').info('Result ======== ' + evtI1.result);
									
									activityIndicator.hideIndicator();
									if(evtI1.result.responseFlag === "S")
									{
										_obj.indicativeExchangeResult = evtI1.result;
										_obj.lblIndicativePaymode.text = 'For ' + _obj.indicativePaymodeName;
										_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.indicativeExchangeResult.exchangeRate + ' ' + destSplit[1];
										_obj.indicativeRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.indicativeExchangeResult.exchangeRate + ' ' + destSplit[1];
										_obj.txtIndicativeRate.value = _obj.minIValue;
										_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minIValue + ')';
										_obj.txtIndicativeValue.value = parseFloat(_obj.indicativeExchangeResult.indicativeDestAmount).toFixed(2);
									
									}
									xhr2 = null;
								}catch(e){}
							}
			
							function xhr2Error(evtI1) {
								try{
									activityIndicator.hideIndicator();
									require('/utils/Network').Network();
									xhr2 = null;
								}catch(e){}
							}
						}
						xhrPre2 = null;
					}catch(e){}
				}

				function xhrPre2Error(evt) {
					try{
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhrPre2 = null;
					}catch(e){}
				}
				
				////////////////////////////////////////////////////////////////////
			}*/
		}
		else
		{
			// Pre-login	
			
			if(_obj.guaranteedPaymodeName !== null)
			{
				activityIndicator.showIndicator();
				var xhr1 = require('/utils/XHR');
				
				xhr1.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"originatingCountryCode":"'+origSplit[0]+'",'+ 
						'"originatingCurrencyCode":"'+origSplit[1]+'",'+
						'"destinationCountryCode":"'+destSplit[0]+'",'+
						'"destinationCurrencyCode":"'+destSplit[1]+'",'+
						'"payModeCode":"'+_obj.guaranteedPaymodeCode+'",'+
						'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
						'}',
					success : xhr1Success,
					error : xhr1Error,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhr1Success(evt) {
					try{
						require('/utils/Console').info('Result ======== ' + evt.result);
						
						activityIndicator.hideIndicator();
						
						if(evt.result.responseFlag === "S")
						{
							_obj.guaranteedExchangeResult = evt.result;
							_obj.lblGuaranteedPaymode.text = 'For ' + _obj.guaranteedPaymodeName;
							guaranteed_rate = parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate);
							
							if(_obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit !== '' && _obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit !== null)
							{
								_obj.guaranteed_rate = (Math.floor((parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate)+parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit)) * 100)/100);						
							}
							else
							{
								_obj.guaranteed_rate = ((Math.floor(parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate)) * 100)/100);
							}
							
							guaranteed_value = [];
							
							try{
						     	for(var gr = 0; gr < _obj.guaranteedExchangeResult.exchangeRateDtls.length; gr++){
						     		guaranteed_value.push(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + (Math.floor((parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].netConversionRate)+parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].fxBenefit)) * 100)/100));
						     	}
						     	
						     	for(var fd = 0; fd < _obj.guaranteedExchangeResult.feeDtls.length; fd++)
					     		{
					     			_obj.feeDtls.push(_obj.guaranteedExchangeResult.feeDtls[fd].rangeFrom + '-' + _obj.guaranteedExchangeResult.feeDtls[fd].rangeTo + '-' + _obj.guaranteedExchangeResult.feeDtls[fd].netFee);
					     		}
						     	
						    }catch(e){
						    	for(var gr = 0; gr < _obj.guaranteedExchangeResult.exchangeRateDtls.length; gr++){
						     		guaranteed_value.push(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].netConversionRate + '~' + '0');
						     	}
						    }
							
							var minExchange = [];
	     					var maxExchange = [];
					     	
					     	for(var s=0; s<_obj.guaranteedExchangeResult.exchangeRateDtls.length;s++)
	     					{
	     						minExchange.push(parseInt(_obj.guaranteedExchangeResult.exchangeRateDtls[s].rangeFrom));
	     						maxExchange.push(parseInt(_obj.guaranteedExchangeResult.exchangeRateDtls[s].rangeTo));
	     					}
	     					
	     					minExchange.sort(function(a,b){return a-b;});
	     					maxExchange.sort(function(a,b){return b-a;});
	     					
	     					_obj.minGValue = minExchange[0];
	     					_obj.maxGValue = maxExchange[0];
					     	
					     	Ti.API.info(_obj.minGValue+'-------'+_obj.maxGValue);
					     /*	if(_obj.maxGValue == 3000)
					     		{
					     		 _obj.guaranteed_rate = 95.88;
					     		 Ti.API.info("Guaranteed rate is " +_obj.guaranteed_rate);
					     		}*/
					     	
					     	_obj.rate = _obj.rate + '#' +  _obj.guaranteed_rate + '-' + destSplit[1] + '-' + 'Guaranteed';
					     	//_obj.rate = _obj.rate + '#' + '95.88'+ '-' + destSplit[1] + '-' + 'Guaranteed';
					     	
							_obj.txtGuaranteedRate.value = _obj.minGValue;
							
							_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + _obj.guaranteed_rate + ' ' + destSplit[1];
							_obj.guaranteedRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.guaranteed_rate + ' ' + destSplit[1];	
							
							_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
							try{
								var strFeeDtl = [];
								
								if(_obj.feeDtls.length > 0)
								{
									for(var fd = 0; fd < _obj.feeDtls.length; fd++)
						     		{
						     			strFeeDtl = _obj.feeDtls[fd].split('-');
						     			
						     			if(parseInt(_obj.minGValue) >= parseInt(strFeeDtl[0]) && parseInt(_obj.minGValue) <= parseInt(strFeeDtl[1])){	
											require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
											_obj.netFee = (Math.floor(parseFloat(strFeeDtl[2]))*100/100);
											break;
										}
						     		}
						     	}
					     		
					     		/*var amt = ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate));
					     		
					     		setTimeout(function(){
									Ti.API.info('IF ' + ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate)));
						     		_obj.txtGuaranteedValue.value = amt.toString();	
								},200);*/
					     		
						     }catch(e){
						     	/*setTimeout(function(){
									Ti.API.info('ELSE ' + ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate)));
						     		_obj.txtGuaranteedValue.value = (_obj.minGValue*parseFloat(_obj.guaranteed_rate));	
								},200);*/
						     }
						}
						xhr1 = null;
					}catch(e){}
				}

				function xhr1Error(evt) {
					try{
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr1 = null;
					}catch(e){}
				}
				
			}
			
			if(_obj.indicativePaymodeName !== null)
			{
				try{
					activityIndicator.showIndicator();
					var xhr2 = require('/utils/XHR');

					xhr2.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"originatingCountryCode":"'+origSplit[0]+'",'+ 
							'"originatingCurrencyCode":"'+origSplit[1]+'",'+
							'"destinationCountryCode":"'+destSplit[0]+'",'+
							'"destinationCurrencyCode":"'+destSplit[1]+'",'+
							'"payModeCode":"'+_obj.indicativePaymodeCode+'",'+
							'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
							'}',
						success : xhr2Success,
						error : xhr2Error,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
	
					function xhr2Success(evt) {
						try{
							require('/utils/Console').info('Result ======== ' + evt.result);
							
							activityIndicator.hideIndicator();
							if(evt.result.responseFlag === "S")
							{
								
								_obj.indicativeExchangeResult = evt.result;
								_obj.lblIndicativePaymode.text = 'For ' + _obj.indicativePaymodeName;
								
								if(_obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit !== '' && _obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit !== null)
								{
									_obj.indicative_rate = (Math.floor((parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate)+parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit)) * 100)/100);						
								}
								else
								{
									_obj.indicative_rate = ((Math.floor(parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate)) * 100)/100);
								}
								
								indicative_value = [];
								
								try{
							     	for(var gr = 0; gr < _obj.indicativeExchangeResult.exchangeRateDtls.length; gr++){
							     		indicative_value.push(_obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + (Math.floor((parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[gr].netConversionRate)+parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[gr].fxBenefit)) * 100)/100));
							     	}
							     	
							     	for(var fd = 0; fd < _obj.indicativeExchangeResult.feeDtls.length; fd++)
						     		{
						     			_obj.feeDtls.push(_obj.indicativeExchangeResult.feeDtls[fd].rangeFrom + '-' + _obj.indicativeExchangeResult.feeDtls[fd].rangeTo + '-' + _obj.indicativeExchangeResult.feeDtls[fd].netFee);
						     		}
							     	
							    }catch(e){
							    	for(var gr = 0; gr < _obj.indicativeExchangeResult.exchangeRateDtls.length; gr++){
							     		indicative_value.push(_obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].netConversionRate + '~' + '0');
							     	}
							    }
							    
							    var minExchange = [];
	     						var maxExchange = [];
							    
							    for(var s=0; s<_obj.indicativeExchangeResult.exchangeRateDtls.length;s++)
		     					{
		     						minExchange.push(parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[s].rangeFrom));
		     						maxExchange.push(parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[s].rangeTo));
		     					}
		     					
		     					minExchange.sort(function(a,b){return a-b;});
		     					maxExchange.sort(function(a,b){return b-a;});
		     					
		     					_obj.minIValue = minExchange[0];
		     					_obj.maxIValue = maxExchange[0];
								
						     	_obj.rate = _obj.rate + '#' +  _obj.indicative_rate + '-' + destSplit[1] + '-' + 'Indicative';
								_obj.txtIndicativeRate.value = _obj.minIValue;
								
								_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];
								_obj.indicativeRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];	
								
								_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minIValue + ')';
								try{
									var strFeeDtl = [];
									
									if(_obj.feeDtls.length > 0)
									{
										for(var fd = 0; fd < _obj.feeDtls.length; fd++)
							     		{
							     			strFeeDtl = _obj.feeDtls[fd].split('-');
							     			
							     			if(parseInt(_obj.minIValue) >= parseInt(strFeeDtl[0]) && parseInt(_obj.minIValue) <= parseInt(strFeeDtl[1])){	
												require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
												_obj.netFee = (Math.floor(parseFloat(strFeeDtl[2]))*100/100);
												break;
											}
							     		}
							     	}
						     		
						     		var amt = ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate));
									//Sanju code here
									//var amt = ((parseFloat(_obj.minIValue))*(_obj.indicative_rate));
									
									                        
						     		_obj.txtIndicativeValue.value = amt.toString();
									
							     }catch(e){
							     	Ti.API.info('ELSE ' + ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate)));
							     	//Ti.API.info('ELSE ' + (parseFloat(_obj.minIValue)*(_obj.indicative_rate)));
							     	_obj.txtIndicativeValue.value = (_obj.minIValue*parseFloat(_obj.indicative_rate));
							     }
							}
							xhr2 = null;
						}catch(e){}
					}
	
					function xhr2Error(evt) {
						try{
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr2 = null;
						}catch(e){}
					}
				}catch(e){}
			}	
		}
	}
	
	setTimeout(function(){
		corridorPaymode();
	},300);
	
	// Pre login Calculations
	function getConvExchangeRate(type)
	{	
		var convAmt = null;
		var netFee = 0;
		var netConvRate = null;
		var strVal = [];
		var range = [];	
		var strFeeDtl = [];
		var amount = null;
		
		if(type == 'indicative')
		{
			amount = parseInt(_obj.txtIndicativeRate.value);
			
			for(var i=0; i<indicative_value.length; i++)
			{
				Ti.API.info('indicative_value[i] ==== ' + indicative_value[i]);
				strVal = indicative_value[i].split('~');	
				range = strVal[0].split('-');
				if(amount >= parseInt(range[0]) && amount <= parseInt(range[1])){
					
					require('/utils/Console').info('Range = ' + range[0] + " - " + range[1]);
					netConvRate = parseFloat(strVal[1]);
					break;
				}
			}
			
			if(_obj.feeDtls.length > 0)
			{
				for(var fd = 0; fd < _obj.feeDtls.length; fd++)
		 		{
		 			strFeeDtl = _obj.feeDtls[fd].split('-');
		 			
		 			if(amount >= parseInt(strFeeDtl[0]) && amount <= parseInt(strFeeDtl[1])){	
						require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
						netFee = parseFloat(strFeeDtl[2]);
						break;
					}
		 		}
		 	}
		}
		else if(type == 'guaranteed')
		{
			amount = parseInt(_obj.txtGuaranteedRate.value);
			
			for(var i=0; i<guaranteed_value.length; i++)
			{
				strVal = guaranteed_value[i].split('~');			
				range = strVal[0].split('-');
				if(amount >= parseInt(range[0]) && amount <= parseInt(range[1])){
					
					require('/utils/Console').info('Range = ' + range[0] + " - " + range[1]);
					netConvRate = parseFloat(strVal[1]);
					break;
				}
			}
			
			if(_obj.feeDtls.length > 0)
			{
				for(var fd = 0; fd < _obj.feeDtls.length; fd++)
		 		{
		 			strFeeDtl = _obj.feeDtls[fd].split('-');
		 			
		 			if(amount >= parseInt(strFeeDtl[0]) && amount <= parseInt(strFeeDtl[1])){	
						require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
						netFee = parseFloat(strFeeDtl[2]);
						break;
					}
		 		}
		 	}
		}
		
		Ti.API.info('netFee = '+netFee);
		
		if(isNaN(netFee))
		{
			netFee = 0;
		}
		
		//convAmt=(amount-netFee)*netConvRate;
		convAmt= amount * netConvRate;

		//convAmt = amount 
		convAmt = convAmt.toFixed(2);
		if(convAmt < 0){
			convAmt = '';
		}
		else if(amount == 0){
			convAmt = '';
		}
		if(type == 'indicative'){
			if(_obj.txtIndicativeRate.value !== '')
			{
				_obj.txtIndicativeValue.value = convAmt;
			}
			
			if(netConvRate === null || netConvRate === '')
			{
				_obj.lblIndicativeRate.text = _obj.indicativeRateDefault;
			}
			else
			{
				_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + netConvRate + ' ' + destSplit[1];
			}
			
		}
		else if(type == 'guaranteed')
		{
			if(_obj.txtGuaranteedRate.value !== '')
			{
				_obj.txtGuaranteedValue.value = convAmt;
			}
			
			if(netConvRate === null || netConvRate === '')
			{
				_obj.lblGuaranteedRate.text = _obj.guaranteedRateDefault;
			}
			else
			{
				_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + netConvRate + ' ' + destSplit[1];
			}
		}	
	}
	
	function getExchangeRate(type)
	{
		var amount = '';
		if(type == "indicative"){
			amount = parseInt(_obj.txtIndicativeRate.value);
		}
		else if(type == "guaranteed"){
			amount = parseInt(_obj.txtGuaranteedRate.value);
		}
		
		activityIndicator.showIndicator();
		
		var xhrConvert = require('/utils/XHR');
		
		if(type == 'indicative')
		{
			var pm = _obj.indicativePaymodeCode;
		}
		
		if(type == 'guaranteed')
		{
			var pm = _obj.guaranteedPaymodeCode;
		}

		xhrConvert.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETEXCHANGERATE",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'",'+
				'"paymodeCode":"'+pm+'",'+
				'"amount":"'+amount+'",'+
				'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
				'}',
			success : xhrConvertSuccess,
			error : xhrConvertError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrConvertSuccess(evt) {
			try{
				require('/utils/Console').info('Result ======== ' + evt.result);
				
				activityIndicator.hideIndicator();
				
				if(evt.result.responseFlag === "S")
				{
					if(type == 'indicative')
					{
						if(_obj.txtIndicativeRate.value !== '')
						{
							_obj.txtIndicativeValue.value = parseFloat(evt.result.indicativeDestAmount).toFixed(2);
						}
						else
						{
							_obj.txtIndicativeValue.value = '';
						}
						
						_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + evt.result.exchangeRate + ' ' + destSplit[1];
					}
					
					if(type == 'guaranteed')
					{
						if(_obj.txtGuaranteedRate.value !== '')
						{
							_obj.txtGuaranteedValue.value = parseFloat(evt.result.indicativeDestAmount).toFixed(2);
						}
						else
						{
							_obj.txtGuaranteedValue.value = '';
						}
						
						_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + evt.result.exchangeRate + ' ' + destSplit[1];
					}
				}
				xhrConvert = null;
			} catch(e) {}
		}

		function xhrConvertError(evt) {
			try{
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhrConvert = null;
			} catch(e) {}
		}
	}
	
	function destroy_exchange() {
		try {
			require('/utils/Console').info('############## Remove exchange start ##############');

			require('/utils/RemoveViews').removeViews(_obj.exchangeView);
			_obj = null;
			
			origSplit = null;
			origCC = null;
			destSplit = null;
			destCC = null;
			
			guaranteed_rate = null;
			guaranteed_value = null;
			indicative_rate = null;
			indicative_value = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_exchange', destroy_exchange);
			require('/utils/Console').info('############## Remove exchange end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}


	Ti.App.addEventListener('destroy_exchange', destroy_exchange);

	return _obj.exchangeView;

};// ExchangeRate()

module.exports = ExchangeRate;
