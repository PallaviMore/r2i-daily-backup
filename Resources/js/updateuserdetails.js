//exports.addressUpdate = function(logId,sesId,ownId)
exports.addressUpdate = function()
{
	require('/lib/analytics').GATrackScreen('Address Update');
	
	var _obj = {
		style : require('/styles/Register').Register,
		winAddrUpdate : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		progressView : null,
		progressMainView : null,
		lblPercent : null,
		lblComplete : null,
		imgProgress : null,
		scrollableView : null,
		
		logId : null,
		sesId : null,
		ownId: null,
		
		//Step 2
		stepView2 : null,
		txtAptNo : null,
		borderViewS21 : null,
		txtBldgNo : null,
		borderViewS22 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		cityView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,
		stateView : null,
		lblState : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,
		occupationView : null,
		lblOccupation : null,
		imgOccupation : null,
		borderViewS210 : null,
		btnStep2 : null,
	
		noteView : null,
		doneMobile : null,
		mobileView : null,
		lblMobile : null,
		txtMobile : null,
		borderViewS11 : null,
		doneDay : null,
		txtDayNo : null,
		borderViewS12 : null,
		
		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
		assurance : null,
		dob : null,
		
		referenceNo : null
	};
	
	/*_obj.sesId = sesId;
	_obj.ownId = ownId;
	_obj.logId = logId;*/
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winAddrUpdate = Titanium.UI.createWindow(_obj.style.winRegister);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winAddrUpdate);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Address Update';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobile.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	
	//note 
	_obj.noteView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblNote.text = 'Please Update your communication Details';
	_obj.borderViewS10 = Ti.UI.createView(_obj.style.borderView);
	
	
	
	
	//Mobile
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobile = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblMobile.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = 'Mobile Number';
	//_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
    _obj.txtMobile.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
   
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobile];
	_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);
	
	//Day time no.
	_obj.dayView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblDay = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblDay.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtDayNo = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtDayNo.hintText = 'Day Time Number';
	//_obj.txtDayNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDayNo.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
	
	_obj.txtDayNo.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
	
	// Set HintText for mobile and day nos
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'United Kingdom' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;			break;
		}
		case 'Australia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		 }
		case 'Singapore' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			_obj.txtDayNo.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDayNo.maxLength = 8;
			break;
		 }
		 case 'Germany' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			_obj.txtDayNo.hintText = 'Day Time Number*';
			_obj.txtDayNo.maxLength = 14;
			break;
		 }			 
		 case 'Canada' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Belgium' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDayNo.maxLength = 8;
			break;
		}		
		case 'Cyprus' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}			
		case 'Finland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}			
		case 'France' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Greece' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Ireland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Italy' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Japan' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Malta' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		}
		case 'Portugal' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		
		case 'Austria' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		
		case 'New Zealand' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		}
		
		case 'Spain' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		
		case 'United Arab Emirates' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		}
		
		default:
		{
			break;
		}
	}
	
	/////////////////// Step 2 ///////////////////
	
	_obj.stepView2 = Ti.UI.createScrollView(_obj.style.stepView2);
	
	_obj.txtAptNo = Ti.UI.createTextField(_obj.style.txtAptNo);
	
	if(countryName[0] === 'Germany' || countryName[0] === 'Singapore' || countryName[0] === 'United Arab Emirates' || countryName[0] === 'Australia' || countryName[0] === 'United States')
	{
		_obj.txtAptNo.hintText = "Flat/House No.*";
	}
	else if(countryName[0] === 'United Kingdom')
	{
		_obj.txtAptNo.hintText = 'House/Apartment No.*';
	}
	else
	{
		_obj.txtAptNo.hintText = 'Flat/Unit No.*';
	}
	_obj.borderViewS21 = Ti.UI.createView(_obj.style.borderView);
	
	
	if(countryName[0] !== 'Australia')
	{
		_obj.txtBldgNo = Ti.UI.createTextField(_obj.style.txtBldgNo);
		_obj.txtBldgNo.hintText = 'Building No./Name';
		
		_obj.borderViewS22 = Ti.UI.createView(_obj.style.borderView);
	}
	
	_obj.txtStreet1 = Ti.UI.createTextField(_obj.style.txtStreet1);
	
	if(countryName[0] === 'Australia' || countryName[0] === 'United Arab Emirates')
	{
		_obj.txtStreet1.hintText = "Street*";
		
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
 	}
	else 
	{
		_obj.txtStreet1.hintText = "Street 1*";
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtStreet2 = Ti.UI.createTextField(_obj.style.txtStreet2);
		_obj.txtStreet2.hintText = 'Street 2';
		_obj.borderViewS24 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtLocality = Ti.UI.createTextField(_obj.style.txtLocality);
		_obj.txtLocality.hintText = 'Locality';
		_obj.borderViewS25 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtSubLocality = Ti.UI.createTextField(_obj.style.txtSubLocality);
		_obj.txtSubLocality.hintText = 'Sub-Locality';
		_obj.borderViewS26 = Ti.UI.createView(_obj.style.borderView);
	}
	
	if(countryName[0] === 'United Arab Emirates')
	{
		_obj.cityView = Ti.UI.createView(_obj.style.cityView);
		_obj.txtCity = Ti.UI.createLabel(_obj.style.txtCity);
		_obj.txtCity.top = 0;
		_obj.txtCity.left = 0;
		_obj.txtCity.text = 'Select City*';
		_obj.imgCity = Ti.UI.createImageView(_obj.style.imgCity);
		
		_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.cityView.addEventListener('click',function(){
			activityIndicator.showIndicator();
			var cityOptions = [];
			
			var xhr = require('/utils/XHR_BCM');
	
			xhr.call({
				url : TiGlobals.appURLBCM,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"'+TiGlobals.bcmConfig+'",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"source":"'+TiGlobals.bcmSource+'",'+
					'"type":"citylisting",'+
					'"platform":"'+TiGlobals.osname+'",'+
					'"corridor":"'+countryName[0]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				if(e.result.status === "S")
				{
					for(var i=0;i<e.result.response[0].citylisting.UAE.length;i++)
					{
						cityOptions.push(e.result.response[0].citylisting.UAE[i]);
					}
					
					var optionDialogCity = require('/utils/OptionDialog').showOptionDialog('City',cityOptions,-1);
					optionDialogCity.show();
					
					setTimeout(function(){
						activityIndicator.hideIndicator();
					},200);
					
					optionDialogCity.addEventListener('click',function(evt){
						if(optionDialogCity.options[evt.index] !== L('btn_cancel'))
						{
							_obj.txtCity.text = optionDialogCity.options[evt.index];
							optionDialogCity = null;
						}
						else
						{
							optionDialogCity = null;
							_obj.txtCity.text = 'Select City*';
						}
					});	
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
				xhr = null;
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		});
	}
	else
	{
		_obj.txtCity = Ti.UI.createTextField(_obj.style.txtCity);
		_obj.txtCity.hintText = 'Select City*';
		_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
	}
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			
			_obj.stateView = Ti.UI.createView(_obj.style.stateView);
			_obj.lblState = Ti.UI.createLabel(_obj.style.lblState);
			_obj.imgState = Ti.UI.createImageView(_obj.style.imgState);
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
			
			if(countryName[0] === 'United Arab Emirates')
			{
				_obj.lblState.text = 'Select Emirates*';	
			}
			else
			{
				_obj.lblState.text = 'Select State*';	
			}
			
			_obj.stateView.addEventListener('click',function(){
				activityIndicator.showIndicator();
				var stateOptions = [];
				
				var xhr = require('/utils/XHR');
		
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"country":"'+countryName[0]+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhrSuccess(e) {
					
					if(e.result.responseFlag === "S")
					{
						var splitArr = e.result.countryRelatedStates.split('~');
				
						for(var i=0;i<splitArr.length;i++)
						{
							stateOptions.push(splitArr[i]);
						}
						
						var optionDialogState = require('/utils/OptionDialog').showOptionDialog('State',stateOptions,-1);
						optionDialogState.show();
						
						setTimeout(function(){
							activityIndicator.hideIndicator();
						},200);
						
						optionDialogState.addEventListener('click',function(evt){
							if(optionDialogState.options[evt.index] !== L('btn_cancel'))
							{
								_obj.lblState.text = optionDialogState.options[evt.index];
								optionDialogState = null;
								
								if(countryName[0] === 'United States' && _obj.lblState.text === 'Arizona')
								{
									_obj.occupationView.setTop(15);
									_obj.occupationView.setHeight(35);
									_obj.borderViewS210.setHeight(1);
									_obj.occupationView.visible = true;
									_obj.borderViewS210.visible = true;
								}
								else
								{
									_obj.occupationView.setTop(0);
									_obj.occupationView.setHeight(0);
									_obj.borderViewS210.setHeight(0);
									_obj.occupationView.visible = false;
									_obj.borderViewS210.visible = false;
								}
							}
							else
							{
								optionDialogState = null;
								
								if(countryName[0] === 'United Arab Emirates')
								{
									_obj.lblState.text = 'Select Emirates*';	
								}
								else
								{
									_obj.lblState.text = 'Select State*';	
								}
								
								_obj.occupationView.setTop(0);
								_obj.occupationView.setHeight(0);
								_obj.borderViewS210.setHeight(0);
								_obj.occupationView.visible = false;
								_obj.borderViewS210.visible = false;
								_obj.txtOccupation.setTop(0);
								_obj.txtOccupation.setHeight(0);
								_obj.borderViewS211.setHeight(0);
								_obj.txtOccupation.visible = false;
								_obj.borderViewS211.visible = false;
							}
						});	
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
					xhr = null;
				}
		
				function xhrError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}
			});

        }
        else
        {
			_obj.lblState = Ti.UI.createTextField(_obj.style.txtCity);
			_obj.lblState.hintText = 'State*';
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
        }
	}
	
	_obj.txtZip = Ti.UI.createTextField(_obj.style.txtZip);
	_obj.txtZip.hintText = 'Zip Code*';
	
	_obj.doneZip = Ti.UI.createButton(_obj.style.done);
	_obj.doneZip.addEventListener('click',function(){
		_obj.txtZip.blur();
	});
	
	////// Pin code hintText ///////
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtZip.hintText = 'Zip Code (5 digits)*';
			_obj.txtZip.maxLength = 5;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Canada' :
		{
			_obj.txtZip.hintText = 'Zip Code (7 alpha-numeric)*';
			_obj.txtZip.maxLength = 7;
			break;
		}
		case 'Australia' :
		{
			_obj.txtZip.hintText = 'Zip Code (4 digits)*';
			_obj.txtZip.maxLength = 4;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Japan' :
		{
			_obj.txtZip.hintText = 'Zip Code (7 digits)*';
			_obj.txtZip.maxLength = 7;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'New Zealand' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Singapore' :
		{
			_obj.txtZip.hintText = 'Postal Code (6 digits)*';
			_obj.txtZip.maxLength = 6;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Ireland' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'United Kingdom' :
		{
			//_obj.txtZip.hintText = 'Postcode (8 alpha-numeric)*';//changing as per desktop site
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'United Arab Emirates' :
		{
			_obj.txtZip.hintText = 'P. O. Box (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Austria' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Belgium' :
		{
			_obj.txtZip.hintText = 'Zip Code (4 digits)*';
			_obj.txtZip.maxLength = 4;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Cyprus' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Finland' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'France' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Germany' :
		{
			_obj.txtZip.hintText = 'Postal Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}		 
		case 'Greece' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Italy' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Malta' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Portugal' :
		{
			_obj.txtZip.hintText = 'Zip Code (25 alpha-numeric)*';
			_obj.txtZip.maxLength = 25;
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Spain' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		default:
		{
			break;
		}
	}
	
	//For Occupation
	_obj.borderViewS29 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.occupationView = Ti.UI.createView(_obj.style.occupationView);
	_obj.lblOccupation = Ti.UI.createLabel(_obj.style.lblOccupation);
	_obj.lblOccupation.text = 'Select Occupation*';
	_obj.imgOccupation = Ti.UI.createImageView(_obj.style.imgOccupation);
	_obj.borderViewS210 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.occupationView.setTop(0);
	_obj.occupationView.setHeight(0);
	_obj.borderViewS210.setHeight(0);
	_obj.occupationView.visible = false;
	_obj.borderViewS210.visible = false;
	
	_obj.txtOccupation = Ti.UI.createTextField(_obj.style.txtAptNo);
	_obj.txtOccupation.hintText = 'Other Occupation*';
	_obj.borderViewS211 = Ti.UI.createView(_obj.style.borderView);
	_obj.txtOccupation.setTop(0);
	_obj.txtOccupation.setHeight(0);
	_obj.borderViewS211.setHeight(0);
	_obj.txtOccupation.visible = false;
	_obj.borderViewS211.visible = false;
	
	_obj.btnStep2 = Ti.UI.createButton(_obj.style.btnStep2);
	//_obj.btnStep2.title = 'CONTINUE';
	_obj.btnStep2.title = 'UPDATE ADDRESS';
	
	_obj.stepView2.add(_obj.txtAptNo);
	_obj.stepView2.add(_obj.borderViewS21);
	
	if(countryName[0] !== 'Australia')
	{
		_obj.stepView2.add(_obj.txtBldgNo);
		_obj.stepView2.add(_obj.borderViewS22);
	}
	_obj.stepView2.add(_obj.txtStreet1);
	_obj.stepView2.add(_obj.borderViewS23);
	
	if(countryName[0] !== 'Australia' || countryName[0] !== 'United Arab Emirates')
	{
		try{
			_obj.stepView2.add(_obj.txtStreet2);
			_obj.stepView2.add(_obj.borderViewS24);
	
			_obj.stepView2.add(_obj.txtLocality);
			_obj.stepView2.add(_obj.borderViewS25);
			
			_obj.stepView2.add(_obj.txtSubLocality);
			_obj.stepView2.add(_obj.borderViewS26);
		}catch(e){}
	}
	
	if(countryName[0] === 'United Arab Emirates')
	{
		_obj.cityView.add(_obj.txtCity);
		_obj.cityView.add(_obj.imgCity);
		_obj.stepView2.add(_obj.cityView);
		_obj.stepView2.add(_obj.borderViewS27);
	}	
	else
	{
		_obj.stepView2.add(_obj.txtCity);
		_obj.stepView2.add(_obj.borderViewS27);
	}
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			_obj.stateView.add(_obj.lblState);
			_obj.stateView.add(_obj.imgState);
			_obj.stepView2.add(_obj.stateView);
			_obj.stepView2.add(_obj.borderViewS28);
        }
        else
        {
			_obj.stepView2.add(_obj.lblState);
			_obj.stepView2.add(_obj.borderViewS28);
        }
	}
	
	_obj.occupationView.add(_obj.lblOccupation);
	_obj.occupationView.add(_obj.imgOccupation);
	_obj.stepView2.add(_obj.occupationView);
	_obj.stepView2.add(_obj.borderViewS210);
	
	_obj.stepView2.add(_obj.txtOccupation);
	_obj.stepView2.add(_obj.borderViewS211);
	
	//Zip/Pin code
	_obj.stepView2.add(_obj.txtZip);
	_obj.stepView2.add(_obj.borderViewS29);
	
	//Note
	_obj.noteView.add(_obj.lblNote);
	_obj.stepView2.add(_obj.noteView);
	_obj.stepView2.add(_obj.borderViewS10);
	
	//Mobile
	_obj.mobileView.add(_obj.lblMobile);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.stepView2.add(_obj.mobileView);
	_obj.stepView2.add(_obj.borderViewS11);
	
	//Day time no
	_obj.dayView.add(_obj.lblDay);
	_obj.dayView.add(_obj.txtDayNo);
	_obj.stepView2.add(_obj.dayView);
	_obj.stepView2.add(_obj.borderViewS12);
	
	_obj.stepView2.add(_obj.btnStep2);
	
	// Occupation List
	
	_obj.occupationView.addEventListener('click',function(e){
		activityIndicator.showIndicator();
		var occupationOptions = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"ocupationlisting",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].ocupationlisting.length;i++)
				{
					occupationOptions.push(e.result.response[0].ocupationlisting[i]);
				}
				
				var optionDialogOccupation = require('/utils/OptionDialog').showOptionDialog('Occupation',occupationOptions,-1);
				optionDialogOccupation.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogOccupation.addEventListener('click',function(evt){
					if(optionDialogOccupation.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblOccupation.text = optionDialogOccupation.options[evt.index];
						optionDialogOccupation = null;
						
						if(_obj.lblOccupation.text === 'Others')
						{
							_obj.txtOccupation.setTop(15);
							_obj.txtOccupation.setHeight(35);
							_obj.borderViewS211.setHeight(1);
							_obj.txtOccupation.visible = true;
							_obj.borderViewS211.visible = true;
						}
						else
						{
							_obj.txtOccupation.setTop(0);
							_obj.txtOccupation.setHeight(0);
							_obj.borderViewS211.setHeight(0);
							_obj.txtOccupation.visible = false;
							_obj.borderViewS211.visible = false;
						}
					}
					else
					{
						optionDialogOccupation = null;
						_obj.lblOccupation.text = 'Select Occupation*';
						_obj.txtOccupation.setTop(0);
						_obj.txtOccupation.setHeight(0);
						_obj.borderViewS211.setHeight(0);
						_obj.txtOccupation.visible = false;
						_obj.borderViewS211.visible = false;
					}
				});	
			}
			else
			{
				require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.btnStep2.addEventListener('click',function(e){
		
		// Apartment/House No.
		_obj.flat = _obj.txtAptNo.value; 
		if(_obj.txtAptNo.value == '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your ' + _obj.txtAptNo.hintText,[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtAptNo.hintText + ' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('',_obj.txtAptNo.hintText + ' cannot contain  |  or  \'  or  ` or  \" or #',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtAptNo.value.charAt(i)+' entered for '+_obj.txtAptNo.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
			return;
		}
		else
		{
			
		}
		// Building No.
		if(countryName[0] !== 'Australia')
		{
			_obj.bldgNo = _obj.txtBldgNo.value;
			if(_obj.txtBldgNo.value.length > 0)
			{
				if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtBldgNo.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtBldgNo.hintText + ' field',[L('btn_ok')]).show();
		    		_obj.txtBldgNo.value = '';
		    		_obj.txtBldgNo.focus();
					return;
				}
				else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtBldgNo.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtBldgNo.value.charAt(i)+' entered for '+_obj.txtBldgNo.hintText+' field',[L('btn_ok')]).show();
		    		_obj.txtBldgNo.value = '';
		    		_obj.txtBldgNo.focus();
					return;
				}
			}
		}
		else
		{
			_obj.bldgNo = '';
		}
		
		//Street1
		
		_obj.street1 = _obj.txtStreet1.value;
		if(_obj.txtStreet1.value === "")
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Street Name',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street Name',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Street Name cannot contain | or \' or ` or \" or #',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else if(_obj.txtStreet1.value.search("[^a-zA-Z0-9 ]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphanumeric characters are allowed in Street Name field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtStreet1.value.charAt(i)+' entered for '+_obj.txtStreet1.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else
		{
			
		}
		
		if(countryName[0] !== 'Australia' || countryName[0] !== 'United Arab Emirates')
		{
			try{
				// Street 2
				_obj.street2 = _obj.txtStreet2.value;
				
				if(_obj.txtStreet2.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet2.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street 2 field',[L('btn_ok')]).show();
			    		_obj.txtStreet2.value = '';
			    		_obj.txtStreet2.focus();
						return;
					}
					else if(_obj.txtStreet2.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Street 2 field',[L('btn_ok')]).show();
			    		_obj.txtStreet2.value = '';
			    		_obj.txtStreet2.focus();
						return;
					}
					else
					{
						
					}
				}
				
				// Locality
				_obj.locality = _obj.txtLocality.value;
			
				if(_obj.txtLocality.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLocality.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of locality',[L('btn_ok')]).show();
			    		_obj.txtLocality.value = '';
			    		_obj.txtLocality.focus();
						return;
					}
					else if(_obj.txtLocality.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in locality field',[L('btn_ok')]).show();
			    		_obj.txtLocality.value = '';
			    		_obj.txtLocality.focus();
						return;
					}
					else
					{
					}
				}
			
				// Sub Locality
				_obj.sub_locality = _obj.txtSubLocality.value;
				
				if(_obj.txtSubLocality.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtSubLocality.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Sub Locality',[L('btn_ok')]).show();
						_obj.txtSubLocality.value = '';
			    		_obj.txtSubLocality.focus();
						return;
					}
					else if(_obj.txtSubLocality.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Sub Locality field',[L('btn_ok')]).show();
			    		_obj.txtSubLocality.value = '';
			    		_obj.txtSubLocality.focus();
						return;
					}
					else
					{
						
					}
				}
			}catch(e){}
		}
		
		// City
		
		if(countryName[0] === 'United Arab Emirates')
		{
			_obj.city = _obj.txtCity.text;
			if(_obj.txtCity.text === 'Select City*')
			{
				require('/utils/AlertDialog').showAlert('','Please select your city',[L('btn_ok')]).show();
	    		_obj.txtCity.text = 'Select City*';
				return;
			}
			else
			{
			}
		}
		else
		{
			_obj.city = _obj.txtCity.value;
			
			if(_obj.txtCity.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your city',[L('btn_ok')]).show();
	    		_obj.txtCity.focus();
	    		_obj.txtCity.value = '';
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtCity.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of city',[L('btn_ok')]).show();
	    		_obj.txtCity.focus();
	    		_obj.txtCity.value = '';
				return;
			}
			else if(_obj.txtCity.value.search("[^a-zA-Z ]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in city',[L('btn_ok')]).show();
	    		_obj.txtCity.focus();
	    		_obj.txtCity.value = '';
				return;
			}
			else
			{
			}	
		}
		
		//State
		if(countryName[0] !== 'Singapore')
		{
			if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
			{
				
				_obj.state = _obj.lblState.text;
				
				if(countryName[0] === 'United Arab Emirates')
				{
					if(_obj.lblState.text === 'Select Emirates*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your Emirate',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select Emirates*';
						return;
					}
					else
					{
					}	
				}
				else
				{
					if(_obj.lblState.text === 'Select State*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your state',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select State*';
						return;
					}
					else
					{
					}
				}
	
	        }
	        else
	        {
				_obj.state = _obj.lblState.value;
				
				if(_obj.lblState.value === '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your state',[L('btn_ok')]).show();
		    		_obj.lblState.focus();
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.lblState.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of state',[L('btn_ok')]).show();
		    		_obj.lblState.focus();
					return;
				}
				else if(_obj.lblState.value.search("[^a-zA-Z ]") >= 0)
				{
					require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in state',[L('btn_ok')]).show();
		    		_obj.lblState.focus();
					return;
				}
				else
				{
				}
				
	        }
		}
		
		// Occupation Arizona - US
	
		if((countryName[0] === 'United States') &&  (_obj.state === 'Arizona'))
		{
			if(_obj.lblOccupation.text === 'Select Occupation*')
			{
				require('/utils/AlertDialog').showAlert('','Please select your occupation',[L('btn_ok')]).show();
	    		_obj.lblOccupation.text = 'Select Occupation*';
				return;
			}
			else if(_obj.lblOccupation.text === 'Others')
			{
				if(_obj.txtOccupation.value === '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter occupation',[L('btn_ok')]).show();
		    		_obj.txtOccupation.focus();
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtOccupation.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of occupation',[L('btn_ok')]).show();
		    		_obj.txtOccupation.focus();
					return;
				}
				else if(_obj.txtOccupation.value.search("[^a-zA-Z]") >= 0)
				{
					require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in occupation',[L('btn_ok')]).show();
		    		_obj.txtOccupation.focus();
					return;
				}
				else
				{
				}
			}
		}
		
		// Zipcode
		
		if(_obj.txtZip.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your '+_obj.txtZip.hintText,[L('btn_ok')]).show();
    		_obj.txtZip.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtZip.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of '+_obj.txtZip.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtZip.focus();
			return;
		}
		
		if(_obj.txtZip.value.length > 0)
		{
	 		var len = _obj.txtZip.value.length;
			var validchars = '';	
			validchars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
			var currChar, prevChar, nextChar;
			var validAlph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var validNum = "0123456789";
			var validAlphFlag = false;
			var validNumFlag = false;
	
			for(i = 0; i <= len - 1; i++)
			{
				currChar = _obj.txtZip.value.charAt(i);
				prevChar = _obj.txtZip.value.charAt(i-1);
				nextChar = _obj.txtZip.value.charAt(i+1);
	
				if(!(validAlph.indexOf(currChar) == -1) && validAlphFlag == false)
				{
					validAlphFlag = true;
				}
	
				if(!(validNum.indexOf(currChar) == -1) && validNumFlag == false)
				{
					validNumFlag = true;
				}
							
				if(validchars.indexOf(currChar) == -1)
				{
					if(countryName[0] != 'Canada')
					{
						require('/utils/AlertDialog').showAlert('','Invalid character "' + currChar + '" entered in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
						return;
					}
				}
	
				if(currChar == ' ' && prevChar == ' ' || prevChar == ' ' && currChar == ' ' && nextChar == ' ' || prevChar == ' ' && currChar == ' ' && i == len-1 || currChar == ' ' && i == len-1)
				{
					if(countryName[0] == 'United Kingdom')
					{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText + '. Only one space is allowed in middle',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					else
					{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					break;
				}
			}
						
			if((countryName[0] != 'United Arab Emirates') && (countryName[0] != 'UAE') && (countryName[0] != 'Hong Kong')) //Country check for HK & UAE
			{		
				if((countryName[0] == 'Singapore') || (countryName[0] == 'Australia') || (countryName[0] == 'United States') || (countryName[0] == 'Portugal'))
				{
					if(countryName[0] != 'Portugal')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
	
					if((countryName[0] == 'Singapore') && (len != 6))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 6 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
						return;
					}
					else if((countryName[0] == 'Australia') && (len != 4))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
						return;
					}
					else if((countryName[0] == 'United States') && (len != 5))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
						return;
					}
					else if((countryName[0] == 'Portugal'))
					{			
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
						else if( len < 3 || len > 25)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 25 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
				}
				else
				{
					if(countryName[0] == 'Canada')
					{
						var pcode = _obj.txtZip.value;
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
						else if(pcode.indexOf(" ") != 3)
						{
							require('/utils/AlertDialog').showAlert('','There must be a space after 3rd character in '+_obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'United Kingdom')
					{			
						if( len < 5 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 5 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
						
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					else
					{
						if( len < 3 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
	
					if((countryName[0] == 'France') || (countryName[0] == 'Germany') || (countryName[0] == 'Italy') || (countryName[0] == 'Euroland') || (countryName[0] == 'New Zealand'))
					{
						var pncode = _obj.txtZip.value;
						if(pncode.split(" ").length > 2)
						{
							require('/utils/AlertDialog').showAlert('','Only one single space is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
							
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'Belgium')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
	
						if(len != 4)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'Spain')
					{
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'United States')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
						if(len != 5)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'Japan')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] != 'Spain')
					{ 
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.focus();
							return;
						}
					}
				}
			} 
		 }
		 
		 ////Call getuser details api
		 
		 function getProfileData()
	 {
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETUSERDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
				
				
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				_obj.referenceNo = e.result.referenceNo;
				
				/*_obj.txtFirstName.value = e.result.firstName;
				_obj.txtLastName.value = e.result.lastName;
				_obj.lblDOB.text = e.result.dob;
				_obj.txtAddress.value = e.result.address;
				_obj.txtCity.value = e.result.city;
				_obj.txtState.value = e.result.state;
				_obj.txtZip.value = e.result.pinCode;
				_obj.txtCountry.value = e.result.country;
				
				var splitMobile = e.result.mobileNo.split('-');
				
				_obj.lblMobile.text =  + splitMobile[0];
				_obj.txtMobile.value = splitMobile[1];
				
				var splitDayNo = e.result.resPhoneNo.split('-');
				
				for(var i=0; i<splitDayNo.length; i++)
				{
					_obj.txtDayNo.value = _obj.txtDayNo.value + splitDayNo[i];
				}
				
				_obj.txtEmail.value = e.result.emailId;
				
				if(e.result.alternateNo !== "")
				{
					var splitAlternate = e.result.alternateNo.split('-');
				
					_obj.lblAlternateNo.text = splitAlternate[0];
					_obj.txtAlternateNo.value = splitAlternate[1];
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_myprofile();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}*/
		}
     }
		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	//getProfileData();
	
		 
		 	////////// Go to Step 3 //////////
		//_obj.scrollableView.moveNext();
		///progressBar(50);
		
		//call update address API
		activityIndicator.showIndicator();
					
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"UPDATEUSERDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				
				/*'"loginId":"'+_obj.logId+'",'+ 
				'"ownerId":"'+_obj.ownId+'",'+
				'"sessionId":"'+_obj.sesId+'"'+
				'"country":"'+countryName[0]+'",'+
				'"dayTimePhone":"'+_obj.txtDayNo.value+'",'+
				'"mobileNumber":"'+_obj.lblMobile.text+_obj.txtMobile.value+'",'+*/
				
				'"referenceNo":" ",'+
				//'"referenceNo":"'+_obj.referenceNo+' ",'+
				//'"mobileNo":" ",'+ 
				'"resPhoneNo":"'+_obj.txtDayNo.value+'",'+ 
				//'"alternateNo":" ",'+
				//'"firstName":"",'+
				//'"lastName":"",'+
				//'"dob":"",'+
				//'"address":"",'+ 
			            
				'"state":"'+_obj.state.value+'",'+
				'"city":"'+_obj.txtCity.value+'",'+
				//'"zip":"'+_obj.txtZip.value+'",'+
				'"pinCode":"'+_obj.txtZip.value+'",'+
				//'"country":"",'+
				//'"emailId":"",'+
				//'"gender":"",'+
				//'"knowAboutUs":"",'+
				//'"nationality":"",'+
				//'"occupation":"",'+
				'"flat":"'+_obj.flat+'",'+
				'"building":"'+_obj.bldgNo+'",'+
				'"street1":"'+_obj.street1+'",'+
				'"street2":"'+_obj.street2+'",'+
				'"locality":"'+_obj.locality+'",'+
				'"sublocality":"'+_obj.sub_locality+'",'+
				//'","subLocality":"'+_obj.sub_locality+'"},'+

				'"fieldsToUpdate":{"mobileNoFlag":"Y","resPhoneNoFlag":"Y","alternateNoFlag":"N","firstNameFlag":"N","lastNameFlag":"N","emailIdFlag":"N","nationalityFlag":"N","occupationFlag":"N","stateFlag":"Y", "cityFlag":"Y","pinCodeFlag":"Y"}'+'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			
		function xhrSuccess(e1) {
			
			if(e1.result.responseFlag === "S")
			{
				activityIndicator.hideIndicator();
				
				TiGlobals.addressUpdate = true;
				
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e1.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e1.result.message);
				}
				 setTimeout(function(){ 
					 if(countryCode[0] === 'US'){
						
							require('/js/kyc/us/KYCModal').KYCModal();
							
							destroy_addressUpdate();
						}
						else{
						
							destroy_addressUpdate();
						}
				    },
						
				    2000); 
				 
			}
			else
			{
				if(e1.result.message === L('invalid_session') || e1.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addressUpdate();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e1.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e1) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
		
_obj.imgClose.addEventListener('click',function(e){
		destroy_addressUpdate();
	});
	
	//_obj.scrollableView.views = [_obj.stepView1,_obj.stepView2,_obj.stepView3,_obj.stepView4];
	_obj.scrollableView.views = [_obj.stepView2];
	_obj.mainView.add(_obj.scrollableView);
	_obj.globalView.add(_obj.mainView);
	_obj.winAddrUpdate.add(_obj.globalView);
	_obj.winAddrUpdate.open();
	
	_obj.winAddrUpdate.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_register();
				alertDialog = null;
			}
		});
	});
	
	function destroy_addressUpdate()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove registration start ##############');
			
			_obj.winAddrUpdate.close();
			require('/utils/RemoveViews').removeViews(_obj.winAddrUpdate);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_addressUpdate',destroy_addressUpdate);
			require('/utils/Console').info('############## Remove registration end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_addressUpdate', destroy_addressUpdate);
}; // addressUpdate()
