exports.AddBankConfirmationModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Bank Account Confirmation DEB');
	
	var _obj = {
		style : require('/styles/deb/AddBankConfirmation').AddBankConfirmation,
		winAddBankConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		
		addBankConfirmationFinalView : null,
		webView : null, 
		btnSendMoney : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winAddBankConfirmation = Titanium.UI.createWindow(_obj.style.winAddBankConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winAddBankConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Bank Account Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// Bank Account Confirmation ///////////////////
	
	_obj.addBankConfirmationFinalView = Ti.UI.createScrollView(_obj.style.addBankConfirmationFinalView);
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'bank_account_confirmation_mobile.php?acc_type=deb&email='+Ti.App.Properties.getString('sourceCountryInfoEmail')+'&phone='+Ti.App.Properties.getString('sourceCountryTollFree');
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.btnSendMoney = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSendMoney.bottom = 20;
	_obj.btnSendMoney.title = 'SEND MONEY NOW';
	
	_obj.globalView.add(_obj.mainView);
	_obj.winAddBankConfirmation.add(_obj.globalView);
	_obj.winAddBankConfirmation.open();
	
	function successConfirmation()
	{
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"SENDERBANKACCADD",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"countryCode":"'+countryCode[0]+'",'+
				'"currencyCode":"'+countryCode[1]+'",'+
				'"paymodeCode":"DEB",'+
				'"firstName":"'+params.firstName+'",'+
			    '"lastName":"'+params.lastName+'",'+
			    '"bankName":"'+params.bankName+'",'+
			    '"bankAccNickName":"'+params.nickName+'",'+
			    '"referenceNo":"'+params.referenceNo+'",'+
			    '"accountType":"'+params.accountType+'",'+
			    '"accountNumber":"'+params.accountNumber+'",'+
			    '"sortCode":"'+params.sortCode+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				_obj.addBankConfirmationFinalView.add(_obj.webView);
				_obj.addBankConfirmationFinalView.add(_obj.btnSendMoney);
				_obj.mainView.add(_obj.addBankConfirmationFinalView);
				
				_obj.btnSendMoney.addEventListener('click',function(e){
					if(Ti.App.Properties.getString('loginStatus') !== '0')
					{
						destroy_addbankconfirmation();
						require('/utils/RemoveViews').removeAllScrollableViews();
						require('/utils/PageController').pageController('transfer');
					}
					else
					{
						if(Ti.App.Properties.getString('mPIN') === '1')
						{
							require('/js/Passcode').Passcode();
						}
						else
						{
							require('/js/LoginModal').LoginModal();
						}
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addbankconfirmation();
				}
				else
				{
					var alertDialog = require('/utils/AlertDialog').showAlert('', e.result.message, [L('btn_ok')]);
					alertDialog.show();
			
					alertDialog.addEventListener('click', function(e) {
						alertDialog.hide();
						if (e.index === 0 || e.index === "0") {
							alertDialog = null;
							destroy_addbankconfirmation();
						}
					});
				}
			}
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	successConfirmation();
		
	_obj.imgClose.addEventListener('click',function(e){
		destroy_addbankconfirmation();
	});
	
	_obj.winAddBankConfirmation.addEventListener('androidback', function(){
		destroy_addbankconfirmation();
	});
	
	function destroy_addbankconfirmation()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove bank confirmation start ##############');
			
			_obj.winAddBankConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winAddBankConfirmation);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_addbankconfirmation',destroy_addbankconfirmation);
			require('/utils/Console').info('############## Remove bank confirmation end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_addbankconfirmation', destroy_addbankconfirmation);
}; // AddBankConfirmationModal()
