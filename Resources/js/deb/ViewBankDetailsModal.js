exports.ViewBankDetailsModal = function(params)
{
	require('/lib/analytics').GATrackScreen('View DEB Bank Account Details');
	
	var _obj = {
		style : require('/styles/ach/ViewBankDetails').ViewBankDetails,
		winViewBankDetails : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		bankAccountView : null,
		tblBankAccounts : null
	};
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winViewBankDetails = Titanium.UI.createWindow(_obj.style.winViewBankDetails);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winViewBankDetails);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'DEB Bank Account Details';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// Add Bank ///////////////////
	
	_obj.bankAccountView = Ti.UI.createScrollView(_obj.style.bankAccountView);
	
	_obj.tblBankAccounts = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblBankAccounts.height = 7*60;
	
	function populateBankAccountDetails() {
		for(var i=0; i<=6; i++)
		{
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				height : 60,
				backgroundColor : 'transparent',
				className : 'bank_account_details'
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var lblKey = Ti.UI.createLabel({
				top : 10,
				left : 20,
				height : 15,
				width:Ti.UI.SIZE,
				textAlign: 'left',
				font : TiFonts.FontStyle('lblNormal12'),
				color : TiFonts.FontStyle('greyFont')
			});
			
			var lblValue = Ti.UI.createLabel({
				top : 30,
				height : 20,
				left : 20,
				width:Ti.UI.SIZE,
				textAlign: 'left',
				font : TiFonts.FontStyle('lblNormal14'),
				color : TiFonts.FontStyle('blackFont')
			});
			
			switch(i)
			{
				case 0:
					lblKey.text = 'Account Holder Name';
					lblValue.text = params.accName;
				break;
				
				case 1:
					lblKey.text = 'Bank Name';
					lblValue.text = params.bankName;
				break;
				
				case 2:
					lblKey.text = 'Bank Account Nick Name';
					lblValue.text = '-';	
				break;
				
				case 3:
				
					var len = params.accNo.length;
					var padLength = len-4;
					var padChar = '';
					
					for(var j=0; j<padLength; j++)
					{
						padChar = padChar + '*';
					}
				
					lblKey.text = 'Account Number';
					lblValue.text = padChar+params.accNo.substr(params.accNo.length - 4);
				break;
				
				case 4:
					lblKey.text = 'Account Type';
					lblValue.text = 'Savings';
				break;
				
				case 5:
					lblKey.text = 'BLZ Code';
					lblValue.text = params.bankCode;
				break;
				
				case 6:
					lblKey.text = 'Country-Currency';
					lblValue.text = origSplit[0]+'-'+origSplit[1];
				break;
			}
			
			
			row.add(lblKey);
			row.add(lblValue);
			_obj.tblBankAccounts.appendRow(row);
		}
	}
	
	populateBankAccountDetails();
	
	_obj.mainView.add(_obj.bankAccountView);
	_obj.bankAccountView.add(_obj.tblBankAccounts);
	_obj.globalView.add(_obj.mainView);
	_obj.winViewBankDetails.add(_obj.globalView);
	_obj.winViewBankDetails.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_viewbankdetails();
	});
	
	function destroy_viewbankdetails()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove view bank details start ##############');
			
			_obj.winViewBankDetails.close();
			require('/utils/RemoveViews').removeViews(_obj.winViewBankDetails);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_viewbankdetails',destroy_viewbankdetails);
			require('/utils/Console').info('############## Remove view bank details end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_viewbankdetails', destroy_viewbankdetails);
}; // ViewBankDetailsModal()