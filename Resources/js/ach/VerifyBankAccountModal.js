exports.VerifyBankAccountModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Bank Account Verification');
	
	var _obj = {
		style : require('/styles/ach/VerifyBankAccount').VerifyBankAccount,
		winVerifyBankAccount : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		bankAccountView : null,
		lblTopNote : null,
		tblBankAccounts : null,
		lblNote : null,
		lblPt1 : null,
		lblPt2 : null,
		btnSubmit : null,
	};
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winVerifyBankAccount = Titanium.UI.createWindow(_obj.style.winVerifyBankAccount);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winVerifyBankAccount);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Bank Account Verification';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// Add Bank ///////////////////
	
	_obj.bankAccountView = Ti.UI.createScrollView(_obj.style.bankAccountView);
	
	_obj.lblTopNote = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.lblTopNote.height = Ti.UI.SIZE;
	_obj.lblTopNote.left = 20;
	_obj.lblTopNote.right = 20;
	_obj.lblTopNote.text = 'To verify your account, all you need to do is to check your bank account statement and look for a debit and credit that we have initiated to your bank account. Enter the amount you see on your bank statement in the Debit/Credit field below';
	
	_obj.tblBankAccounts = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblBankAccounts.top = 15;
	_obj.tblBankAccounts.height = 6*60;
	
	_obj.done = Ti.UI.createButton(_obj.style.done);
	_obj.done.addEventListener('click',function(){
		txtAmount.blur();
	});
	
	var txtAmount = Ti.UI.createTextField(_obj.style.txtAmount);
	
	txtAmount.keyboardType = Ti.UI.KEYBOARD_TYPE_DECIMAL_PAD;
	txtAmount.keyboardToolbar = [_obj.done];
	
	function populateBankAccountDetails() {
		try{
		for(var i=0; i<=5; i++)
		{
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				height : 60,
				backgroundColor : 'transparent',
				className : 'bank_account_details'
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var lblKey = Ti.UI.createLabel({
				top : 10,
				left : 20,
				height : 15,
				width:Ti.UI.SIZE,
				textAlign: 'left',
				font : TiFonts.FontStyle('lblNormal12'),
				color : TiFonts.FontStyle('blackFont')
			});
			
			var lblValue = Ti.UI.createLabel({
				top : 30,
				height : 20,
				left : 20,
				width:Ti.UI.SIZE,
				textAlign: 'left',
				font : TiFonts.FontStyle('lblNormal14'),
				color : TiFonts.FontStyle('blackFont')
			});
			
			txtAmount.hintText = 'Debit/Credit Amount*';
			var borderView = Ti.UI.createView(_obj.style.borderView);
			
			switch(i)
			{
				case 0:
					lblKey.text = 'Account Holder Name';
					lblValue.text = params.accName;
					
					row.add(lblKey);
					row.add(lblValue);
				break;
				
				case 1:
					lblKey.text = 'Bank Name';
					lblValue.text = params.bankName;
					
					row.add(lblKey);
					row.add(lblValue);
				break;
				
				case 2:
					lblKey.text = 'Routing Number (ABA)';
					lblValue.text = params.routNo;	
					
					row.add(lblKey);
					row.add(lblValue);
				break;
				
				case 3:
					lblKey.text = 'Applicable Currency';
					lblValue.text = origSplit[1];
					
					row.add(lblKey);
					row.add(lblValue);
				break;
				
				case 4:
					lblKey.text = 'Account Type';
					lblValue.text = params.accType;
					
					row.add(lblKey);
					row.add(lblValue);
				break;
				
				case 5:
					txtAmount.top = 15;
					borderView.top = 45;
					
					row.add(txtAmount);
					row.add(borderView);
				break;
			}
			
			_obj.tblBankAccounts.appendRow(row);
		}
		}catch(e){}
	}
	
	populateBankAccountDetails();
	
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.lblNote.text = 'Note';
	
	_obj.lblPt1 = Ti.UI.createLabel(_obj.style.lblPt);
	_obj.lblPt1.text = '\u00B7 You need to enter either of the debit or credit amounts from your Bank. (e.g. If your debit and credit amount in your Bank Statement is 33 cents each, you will need to enter 0.33 in the "Debit/Credit Amount" field provided.)';
	
	_obj.lblPt2 = Ti.UI.createLabel(_obj.style.lblPt);
	_obj.lblPt2.text = '\u00B7 Please ensure the amount is accurate to prevent locking of your account on Remit2India Home.';
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.mainView.add(_obj.bankAccountView);
	_obj.bankAccountView.add(_obj.lblTopNote);
	_obj.bankAccountView.add(_obj.tblBankAccounts);
	_obj.bankAccountView.add(_obj.lblNote);
	_obj.bankAccountView.add(_obj.lblPt1);
	_obj.bankAccountView.add(_obj.lblPt2);
	_obj.bankAccountView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winVerifyBankAccount.add(_obj.globalView);
	_obj.winVerifyBankAccount.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_verifybankaccount();
	});
	
	_obj.btnSubmit.addEventListener('click',function(e){
		if(txtAmount.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Debit/Credit amount',[L('btn_ok')]).show();
    		txtAmount.value = '';
    		txtAmount.focus();
			return;
		}
		else
		{
			activityIndicator.showIndicator();
		
			var xhr = require('/utils/XHR');
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"SENDERBANKACCVERIFICATION",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"achId":"'+params.accId+'",'+
					'"debitAmt":"'+txtAmount.value+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				try{
				activityIndicator.hideIndicator();
				if(e.result.responseFlag === "S")
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast(e.result.message);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast(e.result.message);
					}
					
					if(params.pg === 'bank')
					{
						Ti.App.fireEvent('populateBankAccounts');
						destroy_verifybankaccount();
					}
					else
					{
						destroy_verifybankaccount();
						require('/js/my_account/OverviewModal').OverviewModal();
					}
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_verifybankaccount();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				}catch(e){}
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
	});
	
	function destroy_verifybankaccount()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove verify bank account start ##############');
			
			_obj.winVerifyBankAccount.close();
			require('/utils/RemoveViews').removeViews(_obj.winVerifyBankAccount);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_verifybankaccount',destroy_verifybankaccount);
			require('/utils/Console').info('############## Remove verify bank account end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_verifybankaccount', destroy_verifybankaccount);
}; // VerifyBankAccountModal()
