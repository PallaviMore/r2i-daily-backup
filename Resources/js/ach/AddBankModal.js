exports.AddBankModal = function()
{
	require('/lib/analytics').GATrackScreen('Add Bank ACH');
	
	var _obj = {
		style : require('/styles/ach/AddBank').AddBank,
		winAddBank : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		addBankView : null,
		lblBankTransaction : null,
		txtFirstName : null,
		borderViewS11 : null,
		txtLastName : null,
		borderViewS12 : null,
		txtBankName : null,
		borderViewS13 : null,
		lblAccountType : null,
		accountView : null,
		imgSaving : null,
		lblSaving : null,
		imgChecking : null,
		lblChecking : null,
		doneRoutingNo : null,
		txtRoutingNo : null,
		borderViewS14 : null,
		doneAccountNo : null,
		txtAccountNo : null,
		borderViewS15 : null,
		termsView : null,
		imgTerms : null,
		lblTerms1 : null,
		btnSubmit : null,
		accountType : null,
		terms : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winAddBank = Titanium.UI.createWindow(_obj.style.winAddBank);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winAddBank);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Add Bank Details';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// Add Bank ///////////////////
	
	_obj.addBankView = Ti.UI.createScrollView(_obj.style.addBankView);
	
	_obj.lblBankTransaction = Ti.UI.createLabel(_obj.style.lblBankTransaction);
	_obj.lblBankTransaction.text = 'Your transaction will be processed only after the bank account from which the transaction is booked has been verified.';
	
	_obj.txtFirstName = Ti.UI.createTextField(_obj.style.txtFirstName);
	_obj.txtFirstName.hintText = 'First Name*';
	_obj.txtFirstName.maxLength = 25;
	_obj.txtFirstName.value = Ti.App.Properties.getString('firstName');
	_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtLastName = Ti.UI.createTextField(_obj.style.txtLastName);
	_obj.txtLastName.hintText = 'Last Name*';
	_obj.txtLastName.maxLength = 25;
	_obj.txtLastName.value = Ti.App.Properties.getString('lastName');
	_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtBankName = Ti.UI.createTextField(_obj.style.txtBankName);
	_obj.txtBankName.hintText = 'Bank Name*';
	_obj.txtBankName.maxLength = 25;
	_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.lblAccountType = Ti.UI.createLabel(_obj.style.lblAccountType);
	_obj.lblAccountType.text = 'Account Type';
	
	_obj.accountView = Ti.UI.createView(_obj.style.accountView);
	_obj.imgSaving = Ti.UI.createImageView(_obj.style.imgRadio);
	_obj.imgSaving.sel = 'saving';
	_obj.lblSaving = Ti.UI.createLabel(_obj.style.lblAccount);
	_obj.lblSaving.text = 'Saving';
	_obj.lblSaving.sel = 'saving';
	
	_obj.imgChecking = Ti.UI.createImageView(_obj.style.imgRadio);
	_obj.imgChecking.left = 10;
	_obj.imgChecking.sel = 'saving';
	_obj.lblChecking = Ti.UI.createLabel(_obj.style.lblAccount);
	_obj.lblChecking.text = 'Checking';
	_obj.lblChecking.sel = 'saving';
	
	_obj.doneRoutingNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneRoutingNo.addEventListener('click',function(){
		_obj.txtRoutingNo.blur();
	});
	
	_obj.txtRoutingNo = Ti.UI.createTextField(_obj.style.txtRoutingNo);
	_obj.txtRoutingNo.hintText = 'Routing Number*';
	_obj.txtRoutingNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtRoutingNo.keyboardToolbar = [_obj.doneRoutingNo];
	_obj.txtRoutingNo.maxLength = 9;
	_obj.borderViewS14 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneAccountNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneAccountNo.addEventListener('click',function(){
		_obj.txtAccountNo.blur();
	});
	
	_obj.txtAccountNo = Ti.UI.createTextField(_obj.style.txtAccountNo);
	_obj.txtAccountNo.hintText = 'Account Number*';
	//_obj.txtAccountNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtAccountNo.keyboardType =Titanium.UI.KEYBOARD_DECIMAL_PAD;
	_obj.txtAccountNo.keyboardToolbar = [_obj.doneAccountNo];
	_obj.txtAccountNo.maxLength = 17;
	_obj.borderViewS15 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.termsView = Ti.UI.createView(_obj.style.termsView);
	_obj.imgTerms = Ti.UI.createImageView(_obj.style.imgTerms);
	_obj.lblTerms = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblTerms1 = Ti.UI.createLabel(_obj.style.lblTerms1);
	_obj.lblTerms.text = 'I agree to the ';
	_obj.lblTerms1.text = 'Terms & Conditions';
	
	_obj.terms = 'N';
	_obj.imgTerms.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Y';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'N';
		}
	});
	
	_obj.lblTerms.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Y';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'N';
		}
	});
	
	_obj.lblTerms1.addEventListener('click',function(e){
		require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','about','terms-and-conditions');
	});
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SAVE';
	
	_obj.addBankView.add(_obj.lblBankTransaction);
	_obj.addBankView.add(_obj.txtFirstName);
	_obj.addBankView.add(_obj.borderViewS11);
	_obj.addBankView.add(_obj.txtLastName);
	_obj.addBankView.add(_obj.borderViewS12);
	_obj.addBankView.add(_obj.txtBankName);
	_obj.addBankView.add(_obj.borderViewS13);
	_obj.addBankView.add(_obj.lblAccountType);
	_obj.accountView.add(_obj.imgSaving);
	_obj.accountView.add(_obj.lblSaving);
	_obj.accountView.add(_obj.imgChecking);
	_obj.accountView.add(_obj.lblChecking);
	_obj.addBankView.add(_obj.accountView);
	_obj.addBankView.add(_obj.txtRoutingNo);
	_obj.addBankView.add(_obj.borderViewS14);
	_obj.addBankView.add(_obj.txtAccountNo);
	_obj.addBankView.add(_obj.borderViewS15);
	_obj.termsView.add(_obj.imgTerms);
	_obj.termsView.add(_obj.lblTerms);
	_obj.termsView.add(_obj.lblTerms1);
	_obj.addBankView.add(_obj.termsView);
	_obj.addBankView.add(_obj.btnSubmit);
	_obj.mainView.add(_obj.addBankView);
	_obj.globalView.add(_obj.mainView);
	_obj.winAddBank.add(_obj.globalView);
	_obj.winAddBank.open();
	
	_obj.imgSaving.addEventListener('click',function(e){
		_obj.imgSaving.image = '/images/radio_sel.png';
	    _obj.imgChecking.image = '/images/radio_unsel.png';
	    _obj.accountType = 'S';
	});
	
	_obj.imgChecking.addEventListener('click',function(e){
		_obj.imgChecking.image = '/images/radio_sel.png';
	    _obj.imgSaving.image = '/images/radio_unsel.png';
	    _obj.accountType = 'C';
	});
	
	_obj.lblSaving.addEventListener('click',function(e){
		_obj.imgSaving.image = '/images/radio_sel.png';
	    _obj.imgChecking.image = '/images/radio_unsel.png';
	    _obj.accountType = 'S';
	});
	
	_obj.lblChecking.addEventListener('click',function(e){
		_obj.imgChecking.image = '/images/radio_sel.png';
	    _obj.imgSaving.image = '/images/radio_unsel.png';
	    _obj.accountType = 'C';
	});
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		var fName = require('/lib/toml_validations').trim(_obj.txtFirstName.value);
		var lName = require('/lib/toml_validations').trim(_obj.txtLastName.value);
		
		////////////////////// First Name ////////////////////
    	if(_obj.txtFirstName.value === '' || _obj.txtFirstName.value === 'First Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
			return;
		}
		else if(_obj.txtFirstName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in First Name field',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter your First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(fName.length <= 1 && fName.length > 0)
		{	
			require('/utils/AlertDialog').showAlert('','First Name cannot be a single alphabet. Please enter your complete First Name',[L('btn_ok')]).show();
    		_obj.txtFirstName.value = '';
    		_obj.txtFirstName.focus();
    		return;
		}
		else
		{
			
		}
		////////////////////// Last Name ////////////////////
		if(_obj.txtLastName.value === '' || _obj.txtLastName.value === 'Last Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
    		return;
		}
		else if(_obj.txtLastName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Last Name field',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else if(lName.length <= 1 && lName.length > 0) // Last NAME SHOULD BE GREATER THEN 1
		{	
			require('/utils/AlertDialog').showAlert('','Last Name cannot be a single alphabet. Please enter your complete Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else
		{
			
		}
		
		//Bank Name
		if(_obj.txtBankName.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter Bank Name',[L('btn_ok')]).show();
    		_obj.txtBankName.value = '';
    		_obj.txtBankName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_special_char(_obj.txtBankName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid special character entered for Bank Name',[L('btn_ok')]).show();
    		_obj.txtBankName.value = '';
    		_obj.txtBankName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateAlphaNumAtleastOneChar(_obj.txtBankName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Bank Name',[L('btn_ok')]).show();
    		_obj.txtBankName.value = '';
    		_obj.txtBankName.focus();
			return;
		}
		else
		{
			
		}
		
		//Account Type
		if(_obj.accountType === null)
		{
			require('/utils/AlertDialog').showAlert('','Please select an Account Type',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		//Routing Number
		if(_obj.txtRoutingNo.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter Routing Number (ABA)',[L('btn_ok')]).show();
    		_obj.txtRoutingNo.value = '';
    		_obj.txtRoutingNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtRoutingNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Routing Number (ABA)',[L('btn_ok')]).show();
    		_obj.txtRoutingNo.value = '';
    		_obj.txtRoutingNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_special_char(_obj.txtRoutingNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Special characters are not allowed in Routing Number (ABA)',[L('btn_ok')]).show();
    		_obj.txtRoutingNo.value = '';
    		_obj.txtRoutingNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_integer_value(_obj.txtRoutingNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Decimal values are not allowed in Routing Number (ABA)',[L('btn_ok')]).show();
    		_obj.txtRoutingNo.value = '';
    		_obj.txtRoutingNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_ispositive(_obj.txtRoutingNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Routing Number (ABA)',[L('btn_ok')]).show();
    		_obj.txtRoutingNo.value = '';
    		_obj.txtRoutingNo.focus();
			return;
		}
		else if(_obj.txtRoutingNo.value.length != 9)
		{
			require('/utils/AlertDialog').showAlert('','Routing Number (ABA) should be a 9 digit numeric value',[L('btn_ok')]).show();
    		_obj.txtRoutingNo.value = '';
    		_obj.txtRoutingNo.focus();
			return;
		}
		else if(_obj.txtRoutingNo.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Routing Number (ABA) must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtRoutingNo.value = '';
    		_obj.txtRoutingNo.focus();
			return;
		}
		else // Only for US
		{
			var strRoutNo = _obj.txtRoutingNo.value;
			
			var checkSumRoutingNo = ((strRoutNo.charAt(0)*3) + (strRoutNo.charAt(1)*7) + (strRoutNo.charAt(2)*1) + 
									 (strRoutNo.charAt(3)*3) + (strRoutNo.charAt(4)*7) + (strRoutNo.charAt(5)*1) + 
									 (strRoutNo.charAt(6)*3) + (strRoutNo.charAt(7)*7) + (strRoutNo.charAt(8)*1) );
			if(checkSumRoutingNo % 10 != 0)
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Routing Number (ABA)',[L('btn_ok')]).show();
	    		_obj.txtRoutingNo.value = '';
	    		_obj.txtRoutingNo.focus();
				return;
			}
		}
		
		//Account Number
		if(_obj.txtAccountNo.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Account Number',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAccountNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Account Number',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(_obj.txtAccountNo.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Account Number must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_special_char(_obj.txtAccountNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Special characters are not allowed in Account Number',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_ispositive(_obj.txtAccountNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Account Number',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else
		{
			
		}
		
		//Terms and Condition
		if(_obj.terms === 'N')
		{
			require('/utils/AlertDialog').showAlert('','Please accept the terms and conditions',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		var params = {
			firstName:_obj.txtFirstName.value,
		    lastName:_obj.txtLastName.value,
		    accountType:_obj.accountType,
		    routingNumber:_obj.txtRoutingNo.value,
		    accountNumber:_obj.txtAccountNo.value,
		    bankName:_obj.txtBankName.value
		};
		
		require('/js/ach/AddBankConfirmationModal').AddBankConfirmationModal(params);
		destroy_addbank();
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_addbank();
	});
	
	_obj.winAddBank.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_addbank();
				alertDialog = null;
			}
		});
	});
	
	function destroy_addbank()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove add bank start ##############');
			
			_obj.winAddBank.close();
			require('/utils/RemoveViews').removeViews(_obj.winAddBank);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_addbank',destroy_addbank);
			require('/utils/Console').info('############## Remove add bank end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_addbank', destroy_addbank);
}; // AddBankModal()