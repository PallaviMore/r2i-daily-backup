exports.AddBankConfirmationModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Add Bank ACH Confirmation');
	
	var _obj = {
		style : require('/styles/ach/AddBankConfirmation').AddBankConfirmation,
		winAddBankConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lbl1 : null,
		lbl2 : null,
		lbl3 : null,
		borderViewS1 : null,
		tblDetails : null,
		btnSubmit : null,
		addBankConfirmationView : null,
		addBankConfirmationFinalView : null,
		webView : null, 
		btnSendMoney : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winAddBankConfirmation = Titanium.UI.createWindow(_obj.style.winAddBankConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winAddBankConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Bank Account Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// Bank Account Confirmation ///////////////////
	
	_obj.addBankConfirmationView = Ti.UI.createScrollView(_obj.style.addBankConfirmationView);
	
	_obj.lbl1 = Ti.UI.createLabel(_obj.style.lbl1);
	_obj.lbl1.text = '\u00B7 ACH Bank account will be applicable only for USD transfers.';
	
	_obj.lbl2 = Ti.UI.createLabel(_obj.style.lbl2);
	_obj.lbl2.text = '\u00B7 Please confirm your Bank Account details.';
	
	_obj.lbl3 = Ti.UI.createLabel(_obj.style.lbl3);
	_obj.lbl3.text = '\u00B7 You can book the transaction now but the debit instructions will be sent only after your bank account is verified and approved by our Compliance team.';
	
	_obj.borderViewS1 = Ti.UI.createView(_obj.style.borderView);
	_obj.borderViewS1.top = 10;
	_obj.tblDetails = Ti.UI.createTableView(_obj.style.tableView);
	
	for(var i=0; i<=5; i++)
	{
		var row = Ti.UI.createTableViewRow({
			top : 0,
			left : 0,
			right : 0,
			height : 56,
			backgroundColor : '#FFF'
		});
		
		if(TiGlobals.osname !== 'android')
		{
			row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
		}
		
		var lblBankDetails1 = Ti.UI.createLabel({
			height : 16,
			top : 10,
			left : 20,
			right : 20,
			textAlign : 'left',
			font : TiFonts.FontStyle('lblNormal14'),
			color : TiFonts.FontStyle('greyFont')
		});

		var lblBankDetails2 = Ti.UI.createLabel({
			height : 16,
			top : 30,
			bottom:10,
			left : 20,
			right : 20,
			textAlign : 'left',
			font : TiFonts.FontStyle('lblNormal14'),
			color : TiFonts.FontStyle('blackFont')
		});
		
		switch(i)
		{
			case 0:
				lblBankDetails1.text = 'First Name';
				lblBankDetails2.text = params.firstName;
			break;
			
			case 1:
				lblBankDetails1.text = 'Last Name';
				lblBankDetails2.text = params.lastName;
			break;
			
			case 2:
				lblBankDetails1.text = 'Bank Name';
				lblBankDetails2.text = params.bankName;
			break;
			
			case 3:
				lblBankDetails1.text = 'Account Type';
				
				if(params.accountType === 'S')
				{
					lblBankDetails2.text = 'Savings';	
				}
				else
				{
					lblBankDetails2.text = 'Current';
				}
			break;
			
			case 4:
				lblBankDetails1.text = 'Routing Number';
				lblBankDetails2.text = params.routingNumber;
			break;
			
			case 5:
				lblBankDetails1.text = 'Account Number';
				lblBankDetails2.text = params.accountNumber;
			break;
		}
		
		row.add(lblBankDetails1);
		row.add(lblBankDetails2);
		_obj.tblDetails.appendRow(row);
	}
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.addBankConfirmationView.add(_obj.lbl1);
	_obj.addBankConfirmationView.add(_obj.lbl2);
	_obj.addBankConfirmationView.add(_obj.lbl3);
	_obj.addBankConfirmationView.add(_obj.borderViewS1);
	_obj.addBankConfirmationView.add(_obj.tblDetails);
	_obj.addBankConfirmationView.add(_obj.btnSubmit);
	_obj.mainView.add(_obj.addBankConfirmationView);
	_obj.globalView.add(_obj.mainView);
	_obj.winAddBankConfirmation.add(_obj.globalView);
	_obj.winAddBankConfirmation.open();
	
	// Success Confirmation
	
	_obj.addBankConfirmationFinalView = Ti.UI.createScrollView(_obj.style.addBankConfirmationFinalView);
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'bank_account_confirmation_mobile.php?acc_type=ach&email='+Ti.App.Properties.getString('sourceCountryInfoEmail')+'&phone='+Ti.App.Properties.getString('sourceCountryTollFree');
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.btnSendMoney = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSendMoney.bottom = 20;
	_obj.btnSendMoney.title = 'SEND MONEY NOW';
	
	
	function successConfirmation()
	{
		_obj.mainView.remove(_obj.addBankConfirmationView);
		
		_obj.addBankConfirmationFinalView.add(_obj.webView);
		_obj.addBankConfirmationFinalView.add(_obj.btnSendMoney);
		_obj.mainView.add(_obj.addBankConfirmationFinalView);
		
		_obj.btnSendMoney.addEventListener('click',function(e){
			if(Ti.App.Properties.getString('loginStatus') !== '0')
			{
				destroy_addbankconfirmation();
				require('/utils/RemoveViews').removeAllScrollableViews();
				require('/utils/PageController').pageController('transfer');
			}
			else
			{
				if(Ti.App.Properties.getString('mPIN') === '1')
				{
					require('/js/Passcode').Passcode();
				}
				else
				{
					require('/js/LoginModal').LoginModal();
				}
			}
		});
	}
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		// xhr call
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETUSERDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			if(e.result.responseFlag === "S")
			{
				var xhr1 = require('/utils/XHR');
		
				xhr1.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"SENDERBANKACCADD",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
						'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
						'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
						'"countryCode":"'+countryCode[0]+'",'+
						'"currencyCode":"'+countryCode[1]+'",'+
						'"paymodeCode":"ACH",'+
						'"firstName":"'+params.firstName+'",'+
					    '"lastName":"'+params.lastName+'",'+
					    '"accountType":"'+params.accountType+'",'+
					    '"routingNumber":"'+params.routingNumber+'",'+
					    '"accountNumber":"'+params.accountNumber+'",'+
					    '"bankName":"'+params.bankName+'",'+
					    '"referenceNo":"'+e.result.referenceNo+'",'+
					    '"bankAccNickName":"",'+
					    '"sortCode":""'+
						'}',
					success : xhr1Success,
					error : xhr1Error,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhr1Success(e1) {
					activityIndicator.hideIndicator();
					if(e1.result.responseFlag === "S")
					{
						successConfirmation();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e1.result.message,[L('btn_ok')]).show();
					}
					xhr1 = null;
				}
		
				function xhr1Error(e1) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr1 = null;
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addbankconfirmation();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_addbankconfirmation();
	});
	
	_obj.winAddBankConfirmation.addEventListener('androidback', function(){
		destroy_addbankconfirmation();
	});
	
	function destroy_addbankconfirmation()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove bank confirmation start ##############');
			
			_obj.winAddBankConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winAddBankConfirmation);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_addbankconfirmation',destroy_addbankconfirmation);
			require('/utils/Console').info('############## Remove bank confirmation end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_addbankconfirmation', destroy_addbankconfirmation);
}; // AddBankConfirmationModal()
