exports.ReferEmailModal = function()
{
	require('/lib/analytics').GATrackScreen('Refer through email');
	
	var _obj = {
		style : require('/styles/mor/ReferEmail').ReferEmail, // style
		winReferEmail : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblSectionHead : null,
		sectionHeaderBorder : null,
		lblFriendCount : null,
		lblFriendTxt : null,
		friendView : null,
		txtEmail : null,
		borderView2 : null,
		imgTerms : null,
		lblTerms : null,
		lblTerms1 : null,
		termsView : null,
		btnSubmit : null,
		
		terms : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winReferEmail = Ti.UI.createWindow(_obj.style.winReferEmail);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winReferEmail);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Refer & Earn';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'Refer through email';
	
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder);
	
	_obj.friendView = Ti.UI.createView(_obj.style.headView);
	_obj.lblFriendCount = Ti.UI.createLabel(_obj.style.lblHeadCount);
	_obj.lblFriendCount.text = '1';
	_obj.lblFriendTxt = Ti.UI.createLabel(_obj.style.lblHeadTxt);
	_obj.lblFriendTxt.text = 'Enter your friends\' email Ids below separated by comma and without spaces';
	
	_obj.txtEmail = Ti.UI.createTextArea(_obj.style.txtEmail);
	_obj.txtEmail.value = "Email Address*";
	_obj.txtEmail.color = "#dddde0";
	_obj.borderView2 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtEmail.addEventListener('focus',function(e){
		try{
		if(e.source.value === 'Email Address*'){
	        e.source.value = '';
	        _obj.txtEmail.color = "#000";
	      }
	      }catch(e){Ti.API.info(e);}
	});
	
	_obj.txtEmail.addEventListener('blur',function(e){
		try{
		if(e.source.value === ''){
	        e.source.value = 'Email Address*';
	        _obj.txtEmail.color = "#dddde0";
	    }
	    }catch(e){}
	});
	
	_obj.termsView = Ti.UI.createView(_obj.style.termsView);
	_obj.imgTerms = Ti.UI.createImageView(_obj.style.imgTerms);
	_obj.lblTerms = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblTerms1 = Ti.UI.createLabel(_obj.style.lblTerms1);
	_obj.lblTerms.text = 'I agree to the ';
	_obj.lblTerms1.text = 'Terms & Conditions';
	
	_obj.terms = 'N';
	_obj.imgTerms.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Y';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'N';
		}
	});
	
	_obj.lblTerms.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Y';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'N';
		}
	});
	
	_obj.lblTerms1.addEventListener('click',function(e){
		require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','about','terms-and-conditions');
	});
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.btnSubmit.addEventListener('click',function(e){
		try{
		if(_obj.txtEmail.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Email Id\'s cannot be blank',[L('btn_ok')]).show();
			return;		
		}
		else if(_obj.txtEmail.value.length > 0)
		{
			if(require('/lib/toml_validations').splitEmail(_obj.txtEmail.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Email Address',[L('btn_ok')]).show();
	    		return;
			}
		}
		else
		{
			
		}
		
		//Terms and Condition
		if(_obj.terms === 'N')
		{
			require('/utils/AlertDialog').showAlert('','Please accept the terms and conditions',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		var friendDataArray = {
		    frdReferList: []
		};
		
		friendDataArray.frdReferList.push({ 
	        "emails" : _obj.txtEmail.value
	    });
		
		require('/utils/Console').info(JSON.stringify({"frdReferList":friendDataArray.frdReferList}));
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"MORREFERRAL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId').toUpperCase()+'",'+ 
				'"referType":"Refer through Email",'+
				'"frndReferArray":'+JSON.stringify({"frdReferList":friendDataArray.frdReferList})+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				setTimeout(function(){
					destroy_referemail();
				},500);
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_referemail();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
		}catch(e){}
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);
	_obj.friendView.add(_obj.lblFriendCount);
	_obj.friendView.add(_obj.lblFriendTxt);
	_obj.mainView.add(_obj.friendView);
	_obj.mainView.add(_obj.txtEmail);
	_obj.mainView.add(_obj.borderView2);
	_obj.termsView.add(_obj.imgTerms);
	_obj.termsView.add(_obj.lblTerms);
	_obj.termsView.add(_obj.lblTerms1);
	_obj.mainView.add(_obj.termsView);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winReferEmail.add(_obj.globalView);
	_obj.winReferEmail.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_referemail();
	});
	
	_obj.winReferEmail.addEventListener('androidback',function(e){
		destroy_referemail();
	});
	
	function destroy_referemail()
	{
		try{
			
			require('/utils/Console').info('############## Remove refer email start ##############');
			
			_obj.winReferEmail.close();
			require('/utils/RemoveViews').removeViews(_obj.winReferEmail);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_referemail',destroy_referemail);
			require('/utils/Console').info('############## Remove refer email end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_referemail', destroy_referemail);
}; // ReferEmailModal()
