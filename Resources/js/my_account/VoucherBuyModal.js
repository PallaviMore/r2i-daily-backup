exports.VoucherBuyModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Freeway Voucher Addition');
	
	var _obj = {
		style : require('/styles/my_account/VoucherBuy').VoucherBuy, // style
		winVoucherBuy : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblRTRNHeader : null,
		rtrnView : null,
		promoView : null,
		btnSubmit : null,
		
		//FW
		fwVoucherView : [],
		lblFW : [],
		imgFW : [],
		
		//FX
		lblFXHeader : null,
		fxVoucherRedeem : 'No',
		voucherMaxQty : null,
		
		fxfwVoucherSel : null,
		freewayMinAmt : null,
		voucherCount : 0, 
		voucherCode : '',
		fxVouchers : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winVoucherBuy = Ti.UI.createWindow(_obj.style.winVoucherBuy);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winVoucherBuy);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Freeway Voucher Addition';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.rtrnView = Ti.UI.createScrollView(_obj.style.rtrnView);
	
	_obj.lblRTRNHeader = Ti.UI.createLabel(_obj.style.lblRTRNHeader);
	_obj.lblRTRNHeader.text = 'RTRN No.: ' + params.rtrnDetails[0].rtrn;
	
	_obj.promoView = Ti.UI.createView(_obj.style.promoView);
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.rtrnView.add(_obj.lblRTRNHeader);
	_obj.mainView.add(_obj.rtrnView);
	_obj.mainView.add(_obj.promoView);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winVoucherBuy.add(_obj.globalView);
	_obj.winVoucherBuy.open();
	
	_obj.btnSubmit.addEventListener('click', function(e){
		
		// Voucher Fx/Fw
		if(_obj.fxfwVoucherSel === 1)
		{
			if((_obj.fxfwVoucherSel === 1) && (parseInt(_obj.voucherCount) > parseInt(_obj.voucherMaxQty)))
			{
				require('/utils/AlertDialog').showAlert('','You cannot subscribe to more than ' + _obj.voucherMaxQty + ' vouchers.',[L('btn_ok')]).show();
				return;
			}
			else
			{
				
			}
		}
		else
		{
			require('/utils/AlertDialog').showAlert('','Please select at least one voucher',[L('btn_ok')]).show();
			return;
		}
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"BUYVOUCHERPOSTTXN",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rtrn":"'+params.rtrnDetails[0].rtrn+'",'+ 
				'"custTxnFee":"'+params.rtrnDetails[0].custtxnFee+'",'+
				'"operatingmodeId":"INTL",'+
				'"fxVouchers":"'+_obj.fxVouchers+'",'+
				'"voucherQty":"'+_obj.voucherCount+'",'+
				'"voucherType":"'+_obj.voucherType+'",'+
				'"orgAmount":"'+params.rtrnDetails[0].sendOrgAmount+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(evt) {
			activityIndicator.hideIndicator();
			if(evt.result.status === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.successMessage);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.successMessage);
				}
				
				setTimeout(function(){
					destroy_voucherbuy();
				},1000);
			}
			else
			{
				if(evt.result.displayBankMessage === L('invalid_session') || evt.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_voucherbuy();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(evt) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	function voucherBuy()
	{
		// Get Voucher CONFIG
					
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"voucherSettingsExtra",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+origSplit[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(evt) {
			if(evt.result.status === "S")
			{
				if(evt.result.response[0].voucherSettings !== '' || evt.result.response[0].voucherSettings !== null)
				{
					_obj.voucherMaxQty = evt.result.response[0].voucherSettings.maxqty;
					_obj.voucherType = evt.result.response[0].voucherSettings.voucherType;
					
					if(_obj.voucherType !== null)
					{
						var xhr1 = require('/utils/XHR');
				
						xhr1.call({
							url : TiGlobals.appURLTOML,
							get : '',
							post : '{' +
								'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
								'"requestName":"GETACTIVEVOUCHERDETAILS",'+
								'"partnerId":"'+TiGlobals.partnerId+'",'+
								'"channelId":"'+TiGlobals.channelId+'",'+
								'"ipAddress":"'+TiGlobals.ipAddress+'",'+
								'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
								'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
								'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
								'"originatingCountry":"'+origSplit[0]+'",'+ 
								'"originatingCurrency":"'+origSplit[1]+'",'+
								'"destinationCountry":"'+destSplit[0]+'",'+
								'"destinationCurrency":"'+destSplit[1]+'"'+
								'}',
							success : xhr1Success,
							error : xhr1Error,
							contentType : 'application/json',
							timeout : TiGlobals.timer
						});
				
						function xhr1Success(evt1) {
							require('/utils/Console').info('Result ======== ' + evt1.result);
							activityIndicator.hideIndicator();
							
							if(evt1.result.responseFlag === "S")
							{
								if(_obj.voucherType === 'FW')
								{
									_obj.freewayMinAmt = evt.result.response[0].voucherSettings.freewayMinAmt;
									
									if(_obj.fwVoucherView.length === 0)
									{
										var sel=0;
										
										for(var i=0;i<evt1.result.voucherArray.length;i++)
										{
											// FW Voucher View
											
											_obj.fwVoucherView[i] = Ti.UI.createView(_obj.style.fwVoucherView);
											_obj.fwVoucherView[i].sel = evt1.result.voucherArray[i].voucherCode+'~'+evt1.result.voucherArray[i].voucherValue+'~'+evt1.result.voucherArray[i].voucherFee+'~'+evt1.result.voucherArray[i].voucherId+'~'+evt1.result.voucherArray[i].voucherDesc +'~'+ evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit;
											_obj.fwVoucherView[i].vid = i;
											_obj.lblFW[i] = Ti.UI.createLabel(_obj.style.lblFW);
											_obj.lblFW[i].sel = evt1.result.voucherArray[i].voucherCode+'~'+evt1.result.voucherArray[i].voucherValue+'~'+evt1.result.voucherArray[i].voucherFee+'~'+evt1.result.voucherArray[i].voucherId+'~'+evt1.result.voucherArray[i].voucherDesc +'~'+ evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit;
											_obj.lblFW[i].vid = i;
											_obj.lblFW[i].text = 'Subscribe to Remit2India\'s ' + evt1.result.voucherArray[i].voucherDesc + ' for only ' + origSplit[0] + ' ' + evt1.result.voucherArray[i].voucherFee + ' and pay zero transfer fees on all transactions upto ' + evt.result.response[0].voucherSettings.lastDate + '.';
											_obj.imgFW[i] = Ti.UI.createImageView(_obj.style.imgFW);
											_obj.imgFW[i].sel = evt1.result.voucherArray[i].voucherCode+'~'+evt1.result.voucherArray[i].voucherValue+'~'+evt1.result.voucherArray[i].voucherFee+'~'+evt1.result.voucherArray[i].voucherId+'~'+evt1.result.voucherArray[i].voucherDesc +'~'+ evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit;
											_obj.imgFW[i].vid = i;
											_obj.imgFW[i].voucherCode = evt1.result.voucherArray[i].voucherCode;
											
											_obj.fwVoucherView[i].add(_obj.lblFW[i]);
											_obj.fwVoucherView[i].add(_obj.imgFW[i]);
											_obj.promoView.add(_obj.fwVoucherView[i]);
											
											_obj.voucherCount = 0;
											
											_obj.fwVoucherView[i].addEventListener('click',function(efw){
												
												if(_obj.imgFW[efw.source.vid].image === '/images/checkbox_unsel.png')
												{
													for(var i=0;i<evt1.result.voucherArray.length;i++)
													{
														_obj.imgFW[i].image = '/images/checkbox_unsel.png';
														_obj.fxfwVoucherSel = 0;
														_obj.voucherCount = 0;
													}
													
													_obj.fxfwVoucherSel = 1;
													_obj.imgFW[efw.source.vid].image = '/images/checkbox_sel.png';
													_obj.voucherCount = _obj.voucherCount + 1;
													_obj.voucherCode = _obj.imgFW[efw.source.vid].voucherCode;
													_obj.fxVouchers = _obj.imgFW[efw.source.vid].sel;
												}
												else
												{
													if(_obj.voucherCount > 0)
													{
														_obj.fxfwVoucherSel = 0;
														_obj.imgFW[efw.source.vid].image = '/images/checkbox_unsel.png';
														_obj.voucherCount = _obj.voucherCount - 1;
														_obj.fxVouchers = '';
													}
													
													_obj.voucherCode = '';
												}
											});
										}
									}
								}
								
								if(_obj.voucherType === 'FX')
								{
									if(_obj.lblFXHeader === null)
									{
										var rowBody = []; 
										var lblB1 = []; 
										var lblB2 = []; 
										var lblB3 = []; 
										var lblB4 = []; 
										var viewB5 = []; 
										var txtB5 = [];
										var done = [];
										var borderB = [];
											
										_obj.freewayMinAmt = evt.result.response[0].voucherSettings.freewayMinAmt;
										
										_obj.lblFXHeader = Ti.UI.createLabel(_obj.style.lblFXHeader);
										_obj.lblFXHeader.text = 'Select the FXvoucher of your choice and the quantity to get extra paisa on all your transactions.';
										_obj.borderTopFXView = Ti.UI.createView(_obj.style.borderPromoView);
										_obj.borderTopFXView.top = 10;
										_obj.voucherView = Ti.UI.createView(_obj.style.voucherView);
										_obj.voucherView.height = (35*(evt1.result.voucherArray.length+1)+(evt1.result.voucherArray.length+1));
										
										var rowHeader = Ti.UI.createView({
											top : 0,
											left : 0,
											right : 0,
											height : 35,
											backgroundColor : '#c9c9c9',
											horizontalWrap:false,
											layout:'horizontal'
										});
										
										var lblH1 = Ti.UI.createLabel({
											text:'FXvoucher',
											top : 0,
											left : 0,
											width : '20%',
											height : 35,
											textAlign:'center',
											font:TiFonts.FontStyle('lblNormal10'),
											color:TiFonts.FontStyle('redFont'),
											backgroundColor : '#FFF'
										});
										
										var lblH2 = Ti.UI.createLabel({
											text:'Amount Cap',
											top : 0,
											left:TiGlobals.osname === 'android' ? 1 : 2,
											width : '20%',
											height : 35,
											textAlign:'center',
											font:TiFonts.FontStyle('lblNormal10'),
											color:TiFonts.FontStyle('redFont'),
											backgroundColor : '#FFF'
										});
										
										var lblH3 = Ti.UI.createLabel({
											text:'Fees',
											top : 0,
											left:TiGlobals.osname === 'android' ? 1 : 2,
											width : '20%',
											height : 35,
											textAlign:'center',
											font:TiFonts.FontStyle('lblNormal10'),
											color:TiFonts.FontStyle('redFont'),
											backgroundColor : '#FFF'
										});
										
										var lblH4 = Ti.UI.createLabel({
											text:'Extra Paisa',
											top : 0,
											left:TiGlobals.osname === 'android' ? 1 : 2,
											width : '20%',
											height : 35,
											textAlign:'center',
											font:TiFonts.FontStyle('lblNormal10'),
											color:TiFonts.FontStyle('redFont'),
											backgroundColor : '#FFF'
										});
										
										var lblH5 = Ti.UI.createLabel({
											text:'Quantity',
											top : 0,
											left:TiGlobals.osname === 'android' ? 1 : 2,
											width : '20%',
											height : 35,
											textAlign:'center',
											font:TiFonts.FontStyle('lblNormal10'),
											color:TiFonts.FontStyle('redFont'),
											backgroundColor : '#FFF'
										});
										
										var borderH = Ti.UI.createView(_obj.style.borderPromoView);
										
										rowHeader.add(lblH1);
										rowHeader.add(lblH2);
										rowHeader.add(lblH3);
										rowHeader.add(lblH4);
										rowHeader.add(lblH5);
										_obj.voucherView.add(rowHeader);
										_obj.voucherView.add(borderH);
										
										for(var i=0; i<evt1.result.voucherArray.length; i++)
										{
											rowBody[i] = Ti.UI.createView({
												top : 0,
												left : 0,
												right : 0,
												height : 35,
												backgroundColor : '#c9c9c9',
												horizontalWrap:false,
												layout:'horizontal'
											});
											
											if(i%2 === 0)
											{
												var bgColor = '#f5f5f5';
											}
											else
											{
												var bgColor = '#e5e5e5';
											}
											
											lblB1[i] = Ti.UI.createLabel({
												text:evt1.result.voucherArray[i].voucherDesc,
												top : 0,
												left : 0,
												width : '20%',
												height : 35,
												textAlign:'center',
												font:TiFonts.FontStyle('lblNormal10'),
												color:TiFonts.FontStyle('blackFont'),
												backgroundColor : bgColor
											});
											
											lblB2[i] = Ti.UI.createLabel({
												text:origSplit[1]+' '+evt1.result.voucherArray[i].voucherValue,
												top : 0,
												left:TiGlobals.osname === 'android' ? 1 : 2,
												width : '20%',
												height : 35,
												textAlign:'center',
												font:TiFonts.FontStyle('lblNormal10'),
												color:TiFonts.FontStyle('blackFont'),
												backgroundColor : bgColor
											});
											
											lblB3[i] = Ti.UI.createLabel({
												text:origSplit[1]+' '+evt1.result.voucherArray[i].voucherFee,
												top : 0,
												left:TiGlobals.osname === 'android' ? 1 : 2,
												width : '20%',
												height : 35,
												textAlign:'center',
												font:TiFonts.FontStyle('lblNormal10'),
												color:TiFonts.FontStyle('blackFont'),
												backgroundColor : bgColor
											});
											
											lblB4[i] = Ti.UI.createLabel({
												text:evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit,
												top : 0,
												left:TiGlobals.osname === 'android' ? 1 : 2,
												width : '20%',
												height : 35,
												textAlign:'center',
												font:TiFonts.FontStyle('lblNormal10'),
												color:TiFonts.FontStyle('blackFont'),
												backgroundColor : bgColor
											});
											
											viewB5[i] = Ti.UI.createView({
												top : 0,
												left:TiGlobals.osname === 'android' ? 1 : 2,
												width : '20%',
												height : 35,
												backgroundColor : bgColor
											});
											
											done[i] = Ti.UI.createButton(_obj.style.done);
											done[i].addEventListener('click',function(){
												for(var i=0; i<evt1.result.voucherArray.length; i++)
												{
													txtB5[i].blur();
												}
											});
											
											txtB5[i] = Ti.UI.createTextField({
												value:'0',
												width : '50%',
												height : 35,
												font:TiFonts.FontStyle('lblNormal10'),
												color:TiFonts.FontStyle('blackFont'),
												textAlign:'center',
												backgroundColor : 'transparent',
												borderColor:'#000',
												borderWidth:1,
												paddingLeft:5,
												paddingRight:5,
												autoComplete:false,
												autocorrect:false,
												autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
												keyboardType:Ti.UI.KEYBOARD_TYPE_NUMBER_PAD,
												keyboardToolbar:[done[i]],
												voucherCode:evt1.result.voucherArray[i].voucherCode,
												fxSel : evt1.result.voucherArray[i].voucherCode+'~'+evt1.result.voucherArray[i].voucherValue+'~'+evt1.result.voucherArray[i].voucherFee+'~'+evt1.result.voucherArray[i].voucherId+'~'+evt1.result.voucherArray[i].voucherDesc +'~'+ evt1.result.voucherArray[i].voucherSlab[0].voucherBenefit,
												sel:i,
												bubbleParent:false
											});
											
											txtB5[i].addEventListener('click',function(e){
												_obj.fxfwVoucherSel = 0;
												_obj.voucherCount = 0;
												_obj.voucherCode = '';
												for(var j=0; j<evt1.result.voucherArray.length; j++)
												{
													txtB5[j].value = '0';
												}
												
												setTimeout(function(){
													txtB5[e.source.sel].value = '';
													txtB5[e.source.sel].focus();
												},20);
											});
											
											txtB5[i].addEventListener('change',function(e){
												if(parseInt(e.source.value) > 0)
												{
													_obj.fxfwVoucherSel = 1;
													_obj.voucherCount = e.source.value;
													_obj.voucherCode = e.source.voucherCode;
													_obj.fxVouchers = e.source.fxSel;
												}
											});
											
											borderB[i] = Ti.UI.createView(_obj.style.borderPromoView);
											
											rowBody[i].add(lblB1[i]);
											rowBody[i].add(lblB2[i]);
											rowBody[i].add(lblB3[i]);
											rowBody[i].add(lblB4[i]);
											viewB5[i].add(txtB5[i]);
											rowBody[i].add(viewB5[i]);
											_obj.voucherView.add(rowBody[i]);
											_obj.voucherView.add(borderB[i]);
										}
										
										_obj.promoView.add(_obj.lblFXHeader);
										_obj.promoView.add(_obj.borderTopFXView);
										_obj.promoView.add(_obj.voucherView);
									}
								}
							}
							else
							{
								if(evt1.result.message === L('invalid_session') || evt1.result.message === 'Invalid Session')
								{
									require('/lib/session').session();
									destroy_voucherbuy();
								}
							}
							xhr1 = null;
						}
				
						function xhr1Error(evt1) {
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr1 = null;
						}
					}
				}
			}
			else
			{
				require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
			}
			xhr = null;
		}

		function xhrError(evt) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	voucherBuy();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_voucherbuy();
	});
	
	_obj.winVoucherBuy.addEventListener('androidback',function(e){
		destroy_voucherbuy();
	});
	
	function destroy_voucherbuy()
	{
		try{
			
			require('/utils/Console').info('############## Remove voucherbuy start ##############');
			
			_obj.winVoucherBuy.close();
			require('/utils/RemoveViews').removeViews(_obj.winVoucherBuy);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_voucherbuy',destroy_voucherbuy);
			require('/utils/Console').info('############## Remove voucherbuy end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_voucherbuy', destroy_voucherbuy);
}; // VoucherBuyModal()