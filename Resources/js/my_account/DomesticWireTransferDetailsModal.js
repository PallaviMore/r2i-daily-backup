exports.DomesticWireTransferDetailsModal = function(rtrn,nostroDetails)
{
	require('/lib/analytics').GATrackScreen('Domestic Wire Transfer Details');
	
	var _obj = {
		style : require('/styles/my_account/DomesticWireTransferDetails').DomesticWireTransferDetails,
		winDomesticWireTransferDetails : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblWireTxtHeader : null,
		lblWireTxt : null,
		btnDownload : null,
	};
	
	_obj.winDomesticWireTransferDetails = Ti.UI.createWindow(_obj.style.winDomesticWireTransferDetails);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winDomesticWireTransferDetails);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Domestic Wire Transfer Details';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);


	var correspondent =  nostroDetails.field1.split('|');
	var bacs_sort_code =  nostroDetails.field2.split('|');
	//var accountname =  nostroDetails.field3.split('|');     
	var bank_account_number =  nostroDetails.field4.split('|');
	//var fed_routing_number = nostroDetails.field5.split('|');  //newly added by sanjivani on 2 feb 2017
	
	
	_obj.lblWireTxtHeader = Ti.UI.createLabel(_obj.style.lblWireTxtHeader);
	_obj.lblWireTxtHeader.text = 'Tracking No.';
	
	
	if(nostroDetails.hasOwnProperty('field5')){
		var fed_routing_number = nostroDetails.field5.split('|');
		_obj.lblWireTxt = Ti.UI.createLabel(_obj.style.lblWireTxt);
	_obj.lblWireTxt.text = 	'- ' + rtrn + '\n\n' +
							'- ' + correspondent[0] + '\n\n' +
							'- ' + correspondent[1] + '\n\n' +
							'- ' + bacs_sort_code[0] + '\n\n' +
							'- ' + bacs_sort_code[1] + '\n\n' +
							'- ' + fed_routing_number[0] + '\n\n' +
							'- ' + fed_routing_number[1] + '\n\n' +
							'- ' + accountname[0] + '\n\n' +
							'- ' + accountname[1] + '\n\n' +
							'- ' + bank_account_number[0] + '\n\n' +
							'- ' + bank_account_number[1] + '\n\n' +
							'Important: You will need to print this instruction sheet and present it to your bank to ensure that your transaction is completed';
	}
	
	else{
		if(nostroDetails.hasOwnProperty('field3')){
	    var accountname =  nostroDetails.field3.split('|');	
	    _obj.lblWireTxt = Ti.UI.createLabel(_obj.style.lblWireTxt);
	   _obj.lblWireTxt.text = 	'- ' + rtrn + '\n\n' +
							'- ' + correspondent[0] + '\n\n' +
							'- ' + correspondent[1] + '\n\n' +
							'- ' + bacs_sort_code[0] + '\n\n' +
							'- ' + bacs_sort_code[1] + '\n\n' +
							'- ' + accountname[0] + '\n\n' +
							'- ' + accountname[1] + '\n\n' +
							'- ' + bank_account_number[0] + '\n\n' +
							'- ' + bank_account_number[1] + '\n\n' +
							'Important: You will need to print this instruction sheet and present it to your bank to ensure that your transaction is completed';
	}
			
		
		else{
		_obj.lblWireTxt = Ti.UI.createLabel(_obj.style.lblWireTxt);
	_obj.lblWireTxt.text = 	'- ' + rtrn + '\n\n' +
							'- ' + correspondent[0] + '\n\n' +
							'- ' + correspondent[1] + '\n\n' +
							'- ' + bacs_sort_code[0] + '\n\n' +
							'- ' + bacs_sort_code[1] + '\n\n' +
							//'- ' + accountname[0] + '\n\n' +
							//'- ' + accountname[1] + '\n\n' +
							'- ' + bank_account_number[0] + '\n\n' +
							'- ' + bank_account_number[1] + '\n\n' +
							'Important: You will need to print this instruction sheet and present it to your bank to ensure that your transaction is completed';
	}
	
}
	_obj.btnDownload = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnDownload.title = 'DOWNLOAD';
	
	_obj.btnDownload.addEventListener('click', function(e) {
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_ok')],
			message:'You can download the pdf reciept from the desktop website.'
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				alertDialog = null;
				destroy_dwtd();
			}
		});
	});
							
	_obj.mainView.add(_obj.lblWireTxtHeader);
	_obj.mainView.add(_obj.lblWireTxt);
	_obj.mainView.add(_obj.btnDownload);
	
	_obj.globalView.add(_obj.mainView);
	_obj.winDomesticWireTransferDetails.add(_obj.globalView);
	_obj.winDomesticWireTransferDetails.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_dwtd();
	});
	
	_obj.winDomesticWireTransferDetails.addEventListener('androidback',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_dwtd();
	});
	
	function destroy_dwtd()
	{
		try{
			
			require('/utils/Console').info('############## Remove DomesticWireTransferDetails start ##############');
			
			_obj.winDomesticWireTransferDetails.close();
			require('/utils/RemoveViews').removeViews(_obj.winDomesticWireTransferDetails);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_dwtd',destroy_dwtd);
			require('/utils/Console').info('############## Remove DomesticWireTransferDetails end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_dwtd', destroy_dwtd);
}; // DomesticWireTransferDetailsModal()