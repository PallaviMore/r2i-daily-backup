function Dashboard(_self) {
	Ti.App.Properties.setString('pg', 'dashboard');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen(Ti.App.Properties.getString('pg'));
	
	var _obj = {
		style : require('/styles/Dashboard').Dashboard, // style
		dashboardView : null,
		mainView : null,
		
		scrollableView : null,
		
		// Guaranteed Rate
		guaranteedView : null,
		guaranteedRateView : null,
		lblGuaranteedRate : null,
		lblGuaranteedPaymode : null,
		txtGuaranteedRateView : null,
		lblAmountGuaranteedTxt : null,
		txtGuaranteedRate : null,
		doneGuaranteedMobile : null,
		lblGuaranteedRateCountry : null,
		borderViewGuaranteed1 : null,
		lblAmountGuaranteedTxt1 : null,
		txtGuaranteedValueView : null,
		txtGuaranteedValue : null,
		lblGuaranteedValueCountry : null,
		borderViewGuaranteed2 : null,
		btnGuaranteedSendMoney : null,
		
		// Indicative Rate
		indicativeView : null,
		indicativeRateView : null,
		lblIndicativeRate : null,
		lblIndicativePaymode : null,
		lblAmountIndicativeTxt : null,
		txtIndicativeRateView : null,
		txtIndicativeRate : null,
		doneIndicativeMobile : null,
		lblIndicativeRateCountry : null,
		borderViewIndicative1 : null,
		lblAmountIndicativeTxt1 : null,
		txtIndicativeValueView : null,
		txtIndicativeValue : null,
		lblIndicativeValueCountry : null,
		borderViewIndicative2 : null,
		btnIndicativeSendMoney : null,
		
		// icon view
		iconView : null,
		tansferView : null,
		imgTransfer : null,
		imgLock : null,
		aboutView : null,
		imgAbout : null,
		
		// Variables
		tab : null,
		indicativePaymodeName : null,
		indicativePaymodeCode : null,
		guaranteedPaymodeName : null,
		guaranteedPaymodeCode : null,
		
		indicativeExchangeResult : null,
		guaranteedExchangeResult : null,
		guaranteedRateDefault : null,
		indicativeRateDefault : null,
		minGValue : null,
		maxGValue : null,
		minIValue : null,
		maxIValue : null,
		rate : null,
		feeDtls : [],
		netFee : 0,
		netConvAmt111: null
	};
	
	_obj.rate = '';
	_obj.tab = 'guaranteed';
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
		
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	var guaranteed_rate = '';
	var guaranteed_value = [];
	var indicative_rate = '';
	var indicative_value = [];
	
	// CreateUI
	
	_obj.dashboardView = Ti.UI.createView(_obj.style.dashboardView);
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	_obj.iconView = Ti.UI.createView(_obj.style.iconView);
	_obj.tansferView = Ti.UI.createView(_obj.style.tansferView);
	_obj.imgTransfer = Ti.UI.createImageView(_obj.style.imgTransfer);
	_obj.imgLock = Ti.UI.createImageView(_obj.style.imgLock);
	
	_obj.aboutView = Ti.UI.createView(_obj.style.aboutView);
	_obj.imgAbout = Ti.UI.createImageView(_obj.style.imgAbout);
	
	_obj.mainView.add(_obj.scrollableView);
	
	_obj.tansferView.addEventListener('click',function(e){
		if(Ti.App.Properties.getString('loginStatus') !== '0')
		{
			require('/utils/PageController').pageController('transfer');
		}
		else
		{
			if(Ti.App.Properties.getString('mPIN') === '1')
			{
				require('/js/Passcode').Passcode();
			}
			else
			{
				require('/js/LoginModal').LoginModal();
			}
		}
	});
	
	_obj.aboutView.addEventListener('click',function(e){
		require('/utils/PageController').pageController('about');
	});
   if(TiGlobals.osname === 'iphone' || TiGlobals.osname === 'ipad')
   {
    _obj.dashboardView.addEventListener('touchstart', require('/utils/activity-tracker').didActivity); //working//tested on iphone
    
    }

	_obj.tansferView.add(_obj.imgTransfer);
	if(Ti.App.Properties.getString('loginStatus') === '0') 
	{
		_obj.tansferView.add(_obj.imgLock);
	}
	_obj.iconView.add(_obj.tansferView);
	
	_obj.aboutView.add(_obj.imgAbout);
	_obj.iconView.add(_obj.aboutView);
	
	_obj.mainView.add(_obj.iconView);
	_obj.dashboardView.add(_obj.mainView);
	
	function createGuaranteedUI()
	{	
		// Guaranteed Rate
		
		// Post Login
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{		
			if(_obj.guaranteedPaymodeName !== null)
			{
				_obj.guaranteedView = Ti.UI.createView(_obj.style.guaranteedView);
				_obj.guaranteedRateView = Ti.UI.createView(_obj.style.rateView);
				_obj.lblGuaranteedRate = Ti.UI.createLabel(_obj.style.lblRate);
				_obj.lblGuaranteedPaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
				_obj.txtGuaranteedRateView = Ti.UI.createView(_obj.style.txtRateView);
				_obj.lblAmountGuaranteedTxt = Ti.UI.createLabel(_obj.style.lblAmountTxt);
				_obj.lblAmountGuaranteedTxt.text = 'Sending Amount';
				_obj.txtGuaranteedRate = Ti.UI.createTextField(_obj.style.txtRate);
				_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1];
				//_obj.txtGuaranteedRate.keyboardType = Ti.UI.KEYBOARD_NUMBER_PAD;
				_obj.txtGuaranteedRate.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
				_obj.doneGuaranteedMobile = Ti.UI.createButton(_obj.style.done);
				_obj.txtGuaranteedRate.keyboardToolbar = [_obj.doneGuaranteedMobile];
				_obj.lblGuaranteedRateCountry = Ti.UI.createLabel(_obj.style.lblRateCountry);
				_obj.lblGuaranteedRateCountry.text = origSplit[1];
				_obj.borderViewGuaranteed1 = Ti.UI.createView(_obj.style.borderView);
				_obj.lblAmountGuaranteedTxt1 = Ti.UI.createLabel(_obj.style.lblAmountTxt);
				_obj.lblAmountGuaranteedTxt1.text = 'Guaranteed Receiving Amount';
				_obj.txtGuaranteedValueView = Ti.UI.createView(_obj.style.txtValueView);
				_obj.txtGuaranteedValue = Ti.UI.createTextField(_obj.style.txtValue);
				_obj.txtGuaranteedValue.hintText = 'Receiving amount';
				_obj.txtGuaranteedValue.editable = false;
				_obj.lblGuaranteedValueCountry = Ti.UI.createLabel(_obj.style.lblValueCountry);
				_obj.lblGuaranteedValueCountry.text = destSplit[1];
				_obj.borderViewGuaranteed2 = Ti.UI.createView(_obj.style.borderView);
				_obj.btnGuaranteedSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
				_obj.btnGuaranteedSendMoney.title = 'SEND MONEY NOW';
			}
			
			_obj.btnGuaranteedSendMoney.addEventListener('click',function(e){
				if(Ti.App.Properties.getString('loginStatus') !== '0')
				{
					require('/utils/PageController').pageController('transfer');
				}
				else
				{
					if(Ti.App.Properties.getString('mPIN') === '1')
					{
						require('/js/Passcode').Passcode();
					}
					else
					{
						require('/js/LoginModal').LoginModal();
					}
				}
			});
			//change 2
			_obj.txtGuaranteedRate.addEventListener('change',function(e){
				try{
				if(_obj.txtGuaranteedRate.value.charAt(0) == '0')
				{
					_obj.txtGuaranteedRate.value = _obj.minGValue;
					return;
				}
				}catch(e){}
				//to comment if needed
				if((parseInt(_obj.txtGuaranteedRate.value) < _obj.minGValue))
				{
					_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' (Min. ' + _obj.minGValue + ')';
					_obj.txtGuaranteedValue.value = '0.00';
				}
				else if((parseInt(_obj.txtGuaranteedRate.value) > _obj.maxGValue))
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Value cannot be greater than '+_obj.maxGValue);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Value cannot be greater than '+_obj.maxGValue);
					}
					
					_obj.txtGuaranteedRate.value = _obj.maxGValue;
				}
				else
				{
					//getExchangeRate('guaranteed');
					getConvExchangeRate('guaranteed');	
				}
			});
		}
		else
		{
			// Pre login
			
			if(_obj.guaranteedPaymodeName !== null)
			{
				_obj.guaranteedView = Ti.UI.createView(_obj.style.guaranteedView);
				_obj.guaranteedRateView = Ti.UI.createView(_obj.style.rateView);
				_obj.lblGuaranteedRate = Ti.UI.createLabel(_obj.style.lblRate);
				_obj.lblGuaranteedPaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
				_obj.txtGuaranteedRateView = Ti.UI.createView(_obj.style.txtRateView);
				_obj.lblAmountGuaranteedTxt = Ti.UI.createLabel(_obj.style.lblAmountTxt);
				_obj.lblAmountGuaranteedTxt.text = 'Sending Amount';
				_obj.txtGuaranteedRate = Ti.UI.createTextField(_obj.style.txtRate);
				_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1];
				//_obj.txtGuaranteedRate.keyboardType = Ti.UI.KEYBOARD_NUMBER_PAD;
	            _obj.txtGuaranteedRate.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
				_obj.doneGuaranteedMobile = Ti.UI.createButton(_obj.style.done);
				_obj.txtGuaranteedRate.keyboardToolbar = [_obj.doneGuaranteedMobile];
				_obj.lblGuaranteedRateCountry = Ti.UI.createLabel(_obj.style.lblRateCountry);
				_obj.lblGuaranteedRateCountry.text = origSplit[1];
				_obj.borderViewGuaranteed1 = Ti.UI.createView(_obj.style.borderView);
				_obj.lblAmountGuaranteedTxt1 = Ti.UI.createLabel(_obj.style.lblAmountTxt);
				_obj.lblAmountGuaranteedTxt1.text = 'Guaranteed Receiving Amount';
				_obj.txtGuaranteedValueView = Ti.UI.createView(_obj.style.txtValueView);
				_obj.txtGuaranteedValue = Ti.UI.createTextField(_obj.style.txtValue);
				_obj.txtGuaranteedValue.hintText = 'Receiving amount';
				_obj.txtGuaranteedValue.editable = false;
				_obj.lblGuaranteedValueCountry = Ti.UI.createLabel(_obj.style.lblValueCountry);
				_obj.lblGuaranteedValueCountry.text = destSplit[1];
				_obj.borderViewGuaranteed2 = Ti.UI.createView(_obj.style.borderView);
				_obj.btnGuaranteedSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
				_obj.btnGuaranteedSendMoney.title = 'SEND MONEY NOW';
			}
			
			_obj.btnGuaranteedSendMoney.addEventListener('click',function(e){
				if(Ti.App.Properties.getString('loginStatus') !== '0')
				{
					if(TiGlobals.addressUpdate == false){
						require('/js/updateuserdetails').addressUpdate();
					}
					else{
						require('/utils/PageController').pageController('transfer');
					}
					
				}
				else
				{
					if(Ti.App.Properties.getString('mPIN') === '1')
					{
						require('/js/Passcode').Passcode();
					}
					else
					{
						require('/js/LoginModal').LoginModal();
					}
				}
			});
			
			_obj.txtGuaranteedRate.addEventListener('change',function(e){
				if((parseInt(_obj.txtGuaranteedRate.value) < _obj.minGValue))
				{
					_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' (Min. ' + _obj.minGValue + ')';
					_obj.txtGuaranteedValue.value = '0.00';
				}
				else if((parseInt(_obj.txtGuaranteedRate.value) > _obj.maxGValue))
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Value cannot be greater than '+_obj.maxGValue);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Value cannot be greater than '+_obj.maxGValue);
					}
					
					_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' (Min. ' + _obj.minGValue + ')';
					_obj.txtGuaranteedRate.value = _obj.maxGValue;
				}
				else
				{
					getConvExchangeRate('guaranteed');
				}
			});
		}
			
		_obj.guaranteedRateView.add(_obj.lblGuaranteedRate);
		_obj.guaranteedRateView.add(_obj.lblGuaranteedPaymode);
		_obj.guaranteedView.add(_obj.guaranteedRateView);
		_obj.guaranteedView.add(_obj.lblAmountGuaranteedTxt);
		_obj.txtGuaranteedRateView.add(_obj.txtGuaranteedRate);
		_obj.txtGuaranteedRateView.add(_obj.lblGuaranteedRateCountry);
		_obj.guaranteedView.add(_obj.txtGuaranteedRateView);
		_obj.guaranteedView.add(_obj.borderViewGuaranteed1);
		_obj.guaranteedView.add(_obj.lblAmountGuaranteedTxt1);
		_obj.txtGuaranteedValueView.add(_obj.txtGuaranteedValue);
		_obj.txtGuaranteedValueView.add(_obj.lblGuaranteedValueCountry);
		_obj.guaranteedView.add(_obj.txtGuaranteedValueView);
		_obj.guaranteedView.add(_obj.borderViewGuaranteed2);
		_obj.guaranteedView.add(_obj.btnGuaranteedSendMoney);
		
		_obj.doneGuaranteedMobile.addEventListener('click',function(){
			_obj.txtGuaranteedRate.blur();
		});
		
		if(_obj.guaranteedPaymodeName !== null && _obj.guaranteedPaymodeCode !== null){
			_obj.scrollableView.addView(_obj.guaranteedView);
		}
	}
		
	// Indicative Rate
	
	function createIndicativeUI()
	{
		// Post Login
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{		
			if(_obj.indicativePaymodeName !== null)
			{
				_obj.indicativeView = Ti.UI.createView(_obj.style.indicativeView);
				_obj.indicativeRateView = Ti.UI.createView(_obj.style.rateView);
				_obj.lblIndicativeRate = Ti.UI.createLabel(_obj.style.lblRate);
				_obj.lblIndicativePaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
				_obj.lblAmountIndicativeTxt = Ti.UI.createLabel(_obj.style.lblAmountTxt);
				_obj.lblAmountIndicativeTxt.text = 'Sending Amount';
				_obj.txtIndicativeRateView = Ti.UI.createView(_obj.style.txtRateView);
				_obj.txtIndicativeRate = Ti.UI.createTextField(_obj.style.txtRate);
				_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1];
				_obj.txtIndicativeRate.keyboardType = Ti.UI.KEYBOARD_NUMBER_PAD;
				_obj.doneIndicativeMobile = Ti.UI.createButton(_obj.style.done);
				_obj.txtIndicativeRate.keyboardToolbar = [_obj.doneIndicativeMobile];
				_obj.lblIndicativeRateCountry = Ti.UI.createLabel(_obj.style.lblRateCountry);
				_obj.lblIndicativeRateCountry.text = origSplit[1];
				_obj.borderViewIndicative1 = Ti.UI.createView(_obj.style.borderView);
				_obj.lblAmountIndicativeTxt1 = Ti.UI.createLabel(_obj.style.lblAmountTxt);
				_obj.lblAmountIndicativeTxt1.text = 'Indicative Receiving Amount';
				_obj.txtIndicativeValueView = Ti.UI.createView(_obj.style.txtValueView);
				_obj.txtIndicativeValue = Ti.UI.createTextField(_obj.style.txtValue);
				_obj.txtIndicativeValue.hintText = 'Receiving amount';
				_obj.txtIndicativeValue.editable = false;
				_obj.lblIndicativeValueCountry = Ti.UI.createLabel(_obj.style.lblValueCountry);
				_obj.lblIndicativeValueCountry.text = destSplit[1];
				_obj.borderViewIndicative2 = Ti.UI.createView(_obj.style.borderView);
				_obj.btnIndicativeSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
				_obj.btnIndicativeSendMoney.title = 'SEND MONEY NOW';
			}
			
			_obj.btnIndicativeSendMoney.addEventListener('click',function(e){
				if(Ti.App.Properties.getString('loginStatus') !== '0')
				{
					require('/utils/PageController').pageController('transfer');
				}
				else
				{
					if(Ti.App.Properties.getString('mPIN') === '1')
					{
						require('/js/Passcode').Passcode();
					}
					else
					{
						require('/js/LoginModal').LoginModal();
					}
				}
			});
			
			_obj.txtIndicativeRate.addEventListener('change',function(e){
				try{
				if(_obj.txtIndicativeRate.value.charAt(0) == '0')
				{
					_obj.txtIndicativeRate.value = _obj.minIValue;
					return;
				}
			}catch(e){
				Ti.API.info(e);
			}
			
				if((parseInt(_obj.txtIndicativeRate.value) < _obj.minIValue))
				{
					Ti.API.info("Indicative value11111",+_obj.txtIndicativeRate.value);
					Ti.API.info("Min value111",+_obj.minIValue);
					
					_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' (Min. ' + _obj.minIValue + ')';
					//by sanjivani
					//_obj.txtIndicativeRate.value = _obj.minIValue;
					_obj.txtIndicativeValue.value = '0.00';
				}
				else if((parseInt(_obj.txtIndicativeRate.value) > _obj.maxIValue))
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Value cannot be greater than '+_obj.maxIValue);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Value cannot be greater than '+_obj.maxIValue);
					}
					
					_obj.txtIndicativeRate.value = _obj.maxIValue;
				}
				else
				{
					//getExchangeRate('indicative');	
					getConvExchangeRate('indicative');
				}
			});
			
		}
		else   //PreLogin
		{
			if(_obj.indicativePaymodeName !== null)
			{
				_obj.indicativeView = Ti.UI.createView(_obj.style.indicativeView);
				_obj.indicativeRateView = Ti.UI.createView(_obj.style.rateView);
				_obj.lblIndicativeRate = Ti.UI.createLabel(_obj.style.lblRate);
				_obj.lblIndicativePaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
				_obj.txtIndicativeRateView = Ti.UI.createView(_obj.style.txtRateView);
				_obj.lblAmountIndicativeTxt = Ti.UI.createLabel(_obj.style.lblAmountTxt);
				_obj.lblAmountIndicativeTxt.text = 'Sending Amount';
				_obj.txtIndicativeRate = Ti.UI.createTextField(_obj.style.txtRate);
				_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1];
				_obj.txtIndicativeRate.keyboardType = Ti.UI.KEYBOARD_NUMBER_PAD;
				_obj.doneIndicativeMobile = Ti.UI.createButton(_obj.style.done);
				_obj.txtIndicativeRate.keyboardToolbar = [_obj.doneIndicativeMobile];
				_obj.lblIndicativeRateCountry = Ti.UI.createLabel(_obj.style.lblRateCountry);
				_obj.lblIndicativeRateCountry.text = origSplit[1];
				_obj.borderViewIndicative1 = Ti.UI.createView(_obj.style.borderView);
				_obj.lblAmountIndicativeTxt1 = Ti.UI.createLabel(_obj.style.lblAmountTxt);
				_obj.lblAmountIndicativeTxt1.text = 'Indicative Receiving Amount';
				_obj.txtIndicativeValueView = Ti.UI.createView(_obj.style.txtValueView);
				_obj.txtIndicativeValue = Ti.UI.createTextField(_obj.style.txtValue);
				_obj.txtIndicativeValue.hintText = 'Receiving amount';
				_obj.txtIndicativeValue.editable = false;
				_obj.lblIndicativeValueCountry = Ti.UI.createLabel(_obj.style.lblValueCountry);
				_obj.lblIndicativeValueCountry.text = destSplit[1];
				_obj.borderViewIndicative2 = Ti.UI.createView(_obj.style.borderView);
				_obj.btnIndicativeSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
				_obj.btnIndicativeSendMoney.title = 'SEND MONEY NOW';
			}
			
			_obj.btnIndicativeSendMoney.addEventListener('click',function(e){
				if(Ti.App.Properties.getString('loginStatus') !== '0')
				{
					require('/utils/PageController').pageController('transfer');
				}
				else
				{
					if(Ti.App.Properties.getString('mPIN') === '1')
					{
						require('/js/Passcode').Passcode();
					}
					else
					{
						require('/js/LoginModal').LoginModal();
					}
				}
			});
			//change 1
			///
			///
			_obj.txtIndicativeRate.addEventListener('change',function(e){
			
				if(_obj.txtIndicativeRate.value.charAt(0)  == '0')
				{
					_obj.txtIndicativeRate.value = _obj.minIValue;
					return;
				}
				
				if((parseInt(_obj.txtIndicativeRate.value) < _obj.minIValue))
				{
					_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' (Min. ' + _obj.minIValue + ')';
					//by sanjivani
					//_obj.txtIndicativeRate.value = _obj.minIValue;
					_obj.txtIndicativeValue.value = '0.00';
					
				}
				else if((parseInt(_obj.txtIndicativeRate.value) > _obj.maxIValue))
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast('Value cannot be greater than '+_obj.maxIValue);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast('Value cannot be greater than '+_obj.maxIValue);
					}
					//_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' (Min. ' + _obj.minIValue + ')';
					_obj.txtIndicativeRate.value = _obj.maxIValue;
				}
				else
				{
					getConvExchangeRate('indicative');	
				}
			});
		}	
		_obj.indicativeRateView.add(_obj.lblIndicativeRate);
		_obj.indicativeRateView.add(_obj.lblIndicativePaymode);
		_obj.indicativeView.add(_obj.indicativeRateView);
		_obj.indicativeView.add(_obj.lblAmountIndicativeTxt);
		_obj.txtIndicativeRateView.add(_obj.txtIndicativeRate);
		_obj.txtIndicativeRateView.add(_obj.lblIndicativeRateCountry);
		_obj.indicativeView.add(_obj.txtIndicativeRateView);
		_obj.indicativeView.add(_obj.borderViewIndicative1);
		_obj.indicativeView.add(_obj.lblAmountIndicativeTxt1);
		_obj.txtIndicativeValueView.add(_obj.txtIndicativeValue);
		_obj.txtIndicativeValueView.add(_obj.lblIndicativeValueCountry);
		_obj.indicativeView.add(_obj.txtIndicativeValueView);
		_obj.indicativeView.add(_obj.borderViewIndicative2);
		_obj.indicativeView.add(_obj.btnIndicativeSendMoney);
		
		_obj.doneIndicativeMobile.addEventListener('click',function(){
			_obj.txtIndicativeRate.blur();
		});
		
		if(_obj.indicativePaymodeName !== null && _obj.indicativePaymodeCode !== null){
			_obj.scrollableView.addView(_obj.indicativeView);
		}
	}
	
	
	// Call UI
	if(Ti.App.Properties.getString('indicativePaymodeName') !== ''&& Ti.App.Properties.getString('guaranteedPaymodeName') == '')
	{
		_obj.indicativePaymodeName = Ti.App.Properties.getString('indicativePaymodeName');
		_obj.indicativePaymodeCode = Ti.App.Properties.getString('indicativePaymodeCode');	        	
		createIndicativeUI();
	}
	else if(Ti.App.Properties.getString('guaranteedPaymodeName') !== ''&&Ti.App.Properties.getString('indicativePaymodeName') == '')
	{
		_obj.guaranteedPaymodeName = Ti.App.Properties.getString('guaranteedPaymodeName');
		_obj.guaranteedPaymodeCode = Ti.App.Properties.getString('guaranteedPaymodeCode');
		createGuaranteedUI();
	}
	else if(Ti.App.Properties.getString('guaranteedPaymodeName') !== '' && Ti.App.Properties.getString('indicativePaymodeName') !== '')
	{
		_obj.indicativePaymodeName = Ti.App.Properties.getString('indicativePaymodeName');
		_obj.indicativePaymodeCode = Ti.App.Properties.getString('indicativePaymodeCode');
		_obj.guaranteedPaymodeName = Ti.App.Properties.getString('guaranteedPaymodeName');
		_obj.guaranteedPaymodeCode = Ti.App.Properties.getString('guaranteedPaymodeCode');
		
		// If both present show only guaranteed
		
		_obj.indicativePaymodeName = null;
		_obj.indicativePaymodeCode = null;
		
		/*createIndicativeUI();
		setInterval(function(){
			
			//setInterval(function(){
				createGuaranteedUI();
				//},3000);
			},3000);*/
		
		
		//sanju code-- 10 dec
		/*
		 * if(Ti.App.Properties.getString('guaranteedPaymodeName') !== ''){
		
		}
		
		setTimeout(function(){ 
			
			
	    },
			
	    2000); */
		createGuaranteedUI();
	}
	/*else if(Ti.App.Properties.getString('guaranteedPaymodeName') !== '')
	{
		_obj.guaranteedPaymodeName = Ti.App.Properties.getString('guaranteedPaymodeName');
		_obj.guaranteedPaymodeCode = Ti.App.Properties.getString('guaranteedPaymodeCode');
		createGuaranteedUI();
	}
	else if(Ti.App.Properties.getString('indicativePaymodeName') !== '')
	{
		_obj.indicativePaymodeName = Ti.App.Properties.getString('indicativePaymodeName');
		_obj.indicativePaymodeCode = Ti.App.Properties.getString('indicativePaymodeCode');	        	
		createIndicativeUI();
	}*/
	else
	{
		
	}
		
	// Call Corridor
	
	function corridorPaymode() {
				
		// For Post Login
					
		if(Ti.App.Properties.getString('loginStatus') !== '0') 
		{		
			if(_obj.indicativePaymodeName !== null)
			{
				try{
					activityIndicator.showIndicator();
					var xhr2 = require('/utils/XHR');

					xhr2.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"originatingCountryCode":"'+origSplit[0]+'",'+ 
							'"originatingCurrencyCode":"'+origSplit[1]+'",'+
							'"destinationCountryCode":"'+destSplit[0]+'",'+
							'"destinationCurrencyCode":"'+destSplit[1]+'",'+
							'"payModeCode":"'+_obj.indicativePaymodeCode+'",'+
							//'"payModeCode":"CCARD",'+
							'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
							'}',
						success : xhr2Success,
						error : xhr2Error,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
	
					function xhr2Success(evt) {
						try{
							require('/utils/Console').info('Result ======== ' + evt.result);
							
							activityIndicator.hideIndicator();
							if(evt.result.responseFlag === "S")
							{
								
								_obj.indicativeExchangeResult = evt.result;
								_obj.lblIndicativePaymode.text = 'For ' + _obj.indicativePaymodeName;
								
								if(_obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit !== '' && _obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit !== null)
								{
									_obj.indicative_rate = (Math.floor((parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate)+parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit)) * 100)/100);						
								}
								else
								{
									_obj.indicative_rate = ((Math.floor(parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate)) * 100)/100);
								}
								
								indicative_value = [];
								
								try{
							     	for(var gr = 0; gr < _obj.indicativeExchangeResult.exchangeRateDtls.length; gr++){
							     		indicative_value.push(_obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + (Math.floor((parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[gr].netConversionRate)+parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[gr].fxBenefit)) * 100)/100));
							     	}
							     	
							     	for(var fd = 0; fd < _obj.indicativeExchangeResult.feeDtls.length; fd++)
						     		{
						     			_obj.feeDtls.push(_obj.indicativeExchangeResult.feeDtls[fd].rangeFrom + '-' + _obj.indicativeExchangeResult.feeDtls[fd].rangeTo + '-' + _obj.indicativeExchangeResult.feeDtls[fd].netFee);
						     		}
							     	
							    }catch(e){
							    	for(var gr = 0; gr < _obj.indicativeExchangeResult.exchangeRateDtls.length; gr++){
							     		indicative_value.push(_obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].netConversionRate + '~' + '0');
							     	}
							    }
							    var minExchange = [];
		     					var maxExchange = [];
						     	
						     	for(var s=0; s<_obj.indicativeExchangeResult.exchangeRateDtls.length;s++)
		     					{
		     						minExchange.push(parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[s].rangeFrom));
		     						maxExchange.push(parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[s].rangeTo));
		     					}
		     					
		     					minExchange.sort(function(a,b){return a-b;});
		     					maxExchange.sort(function(a,b){return b-a;});
		     					
		     					_obj.minIValue = minExchange[0];
		     					_obj.maxIValue = maxExchange[0];
						     	
						     	Ti.API.info(_obj.minIValue +'-------'+_obj.maxIValue);
						     	
						     	_obj.txtIndicativeRate.value = _obj.minIValue;
						     	
								//_obj.minIValue = parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[0].rangeFrom);
						     	//_obj.maxIValue = parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[_obj.indicativeExchangeResult.exchangeRateDtls.length-1].rangeTo);
						     	
						     	
						     	//_obj.rate = _obj.rate + '#' +  _obj.indicative_rate + '-' + destSplit[1] + '-' + 'Indicative';
						     	//Ti.API.info("my Indiacative rate is1--",+_obj.rate);
								
								
								_obj.txtIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];
								
								_obj.indicativeRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];	
								Ti.API.info('post--_obj.indicative_rate 3',+_obj.indicative_rate );
								_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minIValue + ')';
								Ti.API.info('post _obj.indicative_rate 4',+_obj.indicative_rate );
								try{
									var strFeeDtl = [];
									
									if(_obj.feeDtls.length > 0)
									{
										for(var fd = 0; fd < _obj.feeDtls.length; fd++)
							     		{
							     			strFeeDtl = _obj.feeDtls[fd].split('-');
							     			
							     			if(parseInt(_obj.minIValue) >= parseInt(strFeeDtl[0]) && parseInt(_obj.minIValue) <= parseInt(strFeeDtl[1])){	
												require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
												_obj.netFee = (Math.floor(parseFloat(strFeeDtl[2]))*100/100);
												break;
											}
							     		}
							     	}
						     		
						     		//var amt = ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate));
									//var amt = ((parseFloat(_obj.minIValue))*(_obj.indicative_rate));
						     		
						     		/*setTimeout(function(){
										//Ti.API.info('IF ' + ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate)));
						     			Ti.API.info('IF ' +  ((parseFloat(_obj.minIValue))*(_obj.indicative_rate)));
							     		_obj.txtIndicativeValue.value = amt.toString();	
									},200);*/
						     		
							     }catch(e){
							     	/*setTimeout(function(){
										//Ti.API.info('ELSE ' + ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate)));
										Ti.API.info('ELSE ' +  ((parseFloat(_obj.minIValue))*(_obj.indicative_rate)));
							     		_obj.txtIndicativeValue.value = (_obj.minIValue*parseFloat(_obj.indicative_rate));	
									},200);*/
							     }
							     //Ti.API.info("my Indiacative rate is2--",+_obj.rate);
							     //Ti.App.fireEvent('displayRates',{data:_obj.rate});
							     getConvExchangeRate('indicative');	
							}
							xhr2 = null;
						}catch(e){Ti.API.info(e);}
					}
	
					function xhr2Error(evt) {
						try{
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr2 = null;
						}catch(e){}
					}
				}catch(e){}
			}
			//
			if(_obj.guaranteedPaymodeName !== null)
			{
				activityIndicator.showIndicator();
				var xhr1 = require('/utils/XHR');
				
				xhr1.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"originatingCountryCode":"'+origSplit[0]+'",'+ 
						'"originatingCurrencyCode":"'+origSplit[1]+'",'+
						'"destinationCountryCode":"'+destSplit[0]+'",'+
						'"destinationCurrencyCode":"'+destSplit[1]+'",'+
						'"payModeCode":"'+_obj.guaranteedPaymodeCode+'",'+
						'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
						'}',
					success : xhr1Success,
					error : xhr1Error,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhr1Success(evt) {
					try{
						require('/utils/Console').info('Result ======== ' + evt.result);
						
						activityIndicator.hideIndicator();
						
						if(evt.result.responseFlag === "S")
						{
							_obj.guaranteedExchangeResult = evt.result;
							_obj.lblGuaranteedPaymode.text = 'For ' + _obj.guaranteedPaymodeName;
							guaranteed_rate = parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate);
							
							if(_obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit !== '' && _obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit !== null)
							{
								_obj.guaranteed_rate = (Math.floor((parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate)+parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit)) * 100)/100);						
							}
							else
							{
								_obj.guaranteed_rate = ((Math.floor(parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate)) * 100)/100);
							}
							
							guaranteed_value = [];
							
							try{
						     	for(var gr = 0; gr < _obj.guaranteedExchangeResult.exchangeRateDtls.length; gr++){
						     		guaranteed_value.push(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + (Math.floor((parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].netConversionRate)+parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].fxBenefit)) * 100)/100));
						     	}
						     	
						     	for(var fd = 0; fd < _obj.guaranteedExchangeResult.feeDtls.length; fd++)
					     		{
					     			_obj.feeDtls.push(_obj.guaranteedExchangeResult.feeDtls[fd].rangeFrom + '-' + _obj.guaranteedExchangeResult.feeDtls[fd].rangeTo + '-' + _obj.guaranteedExchangeResult.feeDtls[fd].netFee);
					     		}
						     	
						    }catch(e){
						    	for(var gr = 0; gr < _obj.guaranteedExchangeResult.exchangeRateDtls.length; gr++){
						     		guaranteed_value.push(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].netConversionRate + '~' + '0');
						     	}
						    }
							
							var minExchange = [];
	     					var maxExchange = [];
					     	
					     	for(var s=0; s<_obj.guaranteedExchangeResult.exchangeRateDtls.length;s++)
	     					{
	     						minExchange.push(parseInt(_obj.guaranteedExchangeResult.exchangeRateDtls[s].rangeFrom));
	     						maxExchange.push(parseInt(_obj.guaranteedExchangeResult.exchangeRateDtls[s].rangeTo));
	     					}
	     					
	     					minExchange.sort(function(a,b){return a-b;});
	     					maxExchange.sort(function(a,b){return b-a;});
	     					
	     					_obj.minGValue = minExchange[0];
	     					_obj.maxGValue = maxExchange[0];
					     	
					     	Ti.API.info(_obj.minGValue+'-------'+_obj.maxGValue);
					     	//check 1
					     	//_obj.rate = _obj.rate + '#' +  _obj.guaranteed_rate + '-' + destSplit[1] + '-' + 'Guaranteed';
							_obj.txtGuaranteedRate.value = _obj.minGValue;
							
							_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + _obj.guaranteed_rate + ' ' + destSplit[1];
							_obj.guaranteedRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.guaranteed_rate + ' ' + destSplit[1];	
							
							
							_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
							try{
								var strFeeDtl = [];
								
								if(_obj.feeDtls.length > 0)
								{
									for(var fd = 0; fd < _obj.feeDtls.length; fd++)
						     		{
						     			strFeeDtl = _obj.feeDtls[fd].split('-');
						     			
						     			if(parseInt(_obj.minGValue) >= parseInt(strFeeDtl[0]) && parseInt(_obj.minGValue) <= parseInt(strFeeDtl[1])){	
											require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
											_obj.netFee = (Math.floor(parseFloat(strFeeDtl[2]))*100/100);
											break;
										}
						     		}
						     	}
					     		
					     		//var amt = ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate));
					     		
					     		/*setTimeout(function(){
									Ti.API.info('IF ' + ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate)));
						     		_obj.txtGuaranteedValue.value = amt.toString();	
								},200);*/
					     		
						     }catch(e){
						     	/*setTimeout(function(){
									Ti.API.info('ELSE ' + ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate)));
						     		_obj.txtGuaranteedValue.value = (_obj.minGValue*parseFloat(_obj.guaranteed_rate));	
								},200);*/
						     }
						     
						     //Ti.App.fireEvent('displayRates',{data:_obj.rate});
						     getConvExchangeRate('indicative');	
						}
						xhr1 = null;
					}catch(e){}
				}

				function xhr1Error(evt) {
					try{
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr1 = null;
					}catch(e){}
				}
				
			}
			

		}
		else
		{
			// Pre-login	
			
			if(_obj.guaranteedPaymodeName !== null)
			{
				activityIndicator.showIndicator();
				var xhr1 = require('/utils/XHR');
				
				xhr1.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"originatingCountryCode":"'+origSplit[0]+'",'+ 
						'"originatingCurrencyCode":"'+origSplit[1]+'",'+
						'"destinationCountryCode":"'+destSplit[0]+'",'+
						'"destinationCurrencyCode":"'+destSplit[1]+'",'+
						'"payModeCode":"'+_obj.guaranteedPaymodeCode+'",'+
						'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
						'}',
					success : xhr1Success,
					error : xhr1Error,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});

				function xhr1Success(evt) {
					try{
						require('/utils/Console').info('Result ======== ' + evt.result);
						
						activityIndicator.hideIndicator();
						
						if(evt.result.responseFlag === "S")
						{
							_obj.guaranteedExchangeResult = evt.result;
							_obj.lblGuaranteedPaymode.text = 'For ' + _obj.guaranteedPaymodeName;
							guaranteed_rate = parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate);
							
							if(_obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit !== '' && _obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit !== null)
							{
								_obj.guaranteed_rate = (Math.floor((parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate)+parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].fxBenefit)) * 100)/100);						
							}
							else
							{
								_obj.guaranteed_rate = ((Math.floor(parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[0].netConversionRate)) * 100)/100);
							}
							
							guaranteed_value = [];
							
							try{
						     	for(var gr = 0; gr < _obj.guaranteedExchangeResult.exchangeRateDtls.length; gr++){
						     		guaranteed_value.push(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + (Math.floor((parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].netConversionRate)+parseFloat(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].fxBenefit)) * 100)/100));
						     	}
						     	
						     	for(var fd = 0; fd < _obj.guaranteedExchangeResult.feeDtls.length; fd++)
					     		{
					     			_obj.feeDtls.push(_obj.guaranteedExchangeResult.feeDtls[fd].rangeFrom + '-' + _obj.guaranteedExchangeResult.feeDtls[fd].rangeTo + '-' + _obj.guaranteedExchangeResult.feeDtls[fd].netFee);
					     		}
						     	
						    }catch(e){
						    	for(var gr = 0; gr < _obj.guaranteedExchangeResult.exchangeRateDtls.length; gr++){
						     		guaranteed_value.push(_obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + _obj.guaranteedExchangeResult.exchangeRateDtls[gr].netConversionRate + '~' + '0');
						     	}
						    }
							//
							var minExchange = [];
	     					var maxExchange = [];
					     	
					     	for(var s=0; s<_obj.guaranteedExchangeResult.exchangeRateDtls.length;s++)
	     					{
	     						minExchange.push(parseInt(_obj.guaranteedExchangeResult.exchangeRateDtls[s].rangeFrom));
	     						maxExchange.push(parseInt(_obj.guaranteedExchangeResult.exchangeRateDtls[s].rangeTo));
	     					}
	     					
	     					minExchange.sort(function(a,b){return a-b;});
	     					maxExchange.sort(function(a,b){return b-a;});
	     					
	     					_obj.minGValue = minExchange[0];
	     					_obj.maxGValue = maxExchange[0];
					     	
					     	Ti.API.info(_obj.minGValue+'-------'+_obj.maxGValue);
					     	///
					     	//_obj.rate = _obj.rate + '#' +  _obj.guaranteed_rate + '-' + destSplit[1] + '-' + 'Guaranteed';
							_obj.txtGuaranteedRate.value = _obj.minGValue;
							
							_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + _obj.guaranteed_rate + ' ' + destSplit[1];
							_obj.guaranteedRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.guaranteed_rate + ' ' + destSplit[1];	
							
							_obj.txtGuaranteedRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minGValue + ')';
							try{
								var strFeeDtl = [];
								
								if(_obj.feeDtls.length > 0)
								{
									for(var fd = 0; fd < _obj.feeDtls.length; fd++)
						     		{
						     			strFeeDtl = _obj.feeDtls[fd].split('-');
						     			
						     			if(parseInt(_obj.minGValue) >= parseInt(strFeeDtl[0]) && parseInt(_obj.minGValue) <= parseInt(strFeeDtl[1])){	
											require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
											_obj.netFee = (Math.floor(parseFloat(strFeeDtl[2]))*100/100);
											break;
										}
						     		}
						     	}
					     		
					     		//var amt = ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate));
					     		
					     		/*setTimeout(function(){
									Ti.API.info('IF ' + ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate)));
						     		_obj.txtGuaranteedValue.value = amt.toString();	
								},200);*/
					     		
						     }catch(e){
						     	/*setTimeout(function(){
									Ti.API.info('ELSE ' + ((parseFloat(_obj.minGValue)-parseFloat(_obj.netFee))*(_obj.guaranteed_rate)));
						     		_obj.txtGuaranteedValue.value = (_obj.minGValue*parseFloat(_obj.guaranteed_rate));	
								},200);*/
						     }
						     
						     //Ti.App.fireEvent('displayRates',{data:_obj.rate});
						     getConvExchangeRate('guaranteed');	
						}
						xhr1 = null;
					}catch(e){}
				}

				function xhr1Error(evt) {
					try{
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr1 = null;
					}catch(e){}
				}
				
			}
			
			if(_obj.indicativePaymodeName !== null)
			{
				try{
					activityIndicator.showIndicator();
					var xhr2 = require('/utils/XHR');

					xhr2.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"GETEXCHANGERATEFORCOUNTRY",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"originatingCountryCode":"'+origSplit[0]+'",'+ 
							'"originatingCurrencyCode":"'+origSplit[1]+'",'+
							'"destinationCountryCode":"'+destSplit[0]+'",'+
							'"destinationCurrencyCode":"'+destSplit[1]+'",'+
							'"payModeCode":"'+_obj.indicativePaymodeCode+'",'+
							//'"payModeCode":"CCARD",'+
							'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
							'}',
						success : xhr2Success,
						error : xhr2Error,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
	
					function xhr2Success(evt) {
						try{
							require('/utils/Console').info('Result ======== ' + evt.result);
							
							activityIndicator.hideIndicator();
							if(evt.result.responseFlag === "S")
							{
								
								_obj.indicativeExchangeResult = evt.result;
								_obj.lblIndicativePaymode.text = 'For ' + _obj.indicativePaymodeName;
								_obj.indicative_rate = parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate).toFixed(2);
								Ti.API.info("Test indicative rate with toFixed(2)",+_obj.indicative_rate);
								if(_obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit !== '' && _obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit !== null)
								{
									_obj.indicative_rate = (Math.floor((parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate)+parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].fxBenefit)) * 100)/100);						
								}
								else
								{
									_obj.indicative_rate = ((Math.floor(parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[0].netConversionRate)) * 100)/100);
								}
								
								indicative_value = [];
								
								try{
							     	for(var gr = 0; gr < _obj.indicativeExchangeResult.exchangeRateDtls.length; gr++){
							     		indicative_value.push(_obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + (Math.floor((parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[gr].netConversionRate)+parseFloat(_obj.indicativeExchangeResult.exchangeRateDtls[gr].fxBenefit)) * 100)/100));
							     	}
							     	
							     	for(var fd = 0; fd < _obj.indicativeExchangeResult.feeDtls.length; fd++)
						     		{
						     			_obj.feeDtls.push(_obj.indicativeExchangeResult.feeDtls[fd].rangeFrom + '-' + _obj.indicativeExchangeResult.feeDtls[fd].rangeTo + '-' + _obj.indicativeExchangeResult.feeDtls[fd].netFee);
						     		}
							     	
							    }catch(e){
							    	for(var gr = 0; gr < _obj.indicativeExchangeResult.exchangeRateDtls.length; gr++){
							     		indicative_value.push(_obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeFrom + '-' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].rangeTo + '~' + _obj.indicativeExchangeResult.exchangeRateDtls[gr].netConversionRate + '~' + '0');
							     	}
							    }
								//sanjivani code for bug.1235
							    var minExchange = [];
		     					var maxExchange = [];
						     	
						     	for(var s=0; s<_obj.indicativeExchangeResult.exchangeRateDtls.length;s++)
		     					{
		     						minExchange.push(parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[s].rangeFrom));
		     						maxExchange.push(parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[s].rangeTo));
		     					}
		     					
		     					minExchange.sort(function(a,b){return a-b;});
		     					maxExchange.sort(function(a,b){return b-a;});
		     					
		     					_obj.minIValue = minExchange[0];
		     					_obj.maxIValue = maxExchange[0];
						     	
						     	Ti.API.info(_obj.minIValue +'-------'+_obj.maxIValue);
						     	
						     	_obj.txtIndicativeRate.value = _obj.minIValue;
						     	
								//_obj.minIValue = parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[0].rangeFrom);
						     	//_obj.maxIValue = parseInt(_obj.indicativeExchangeResult.exchangeRateDtls[_obj.indicativeExchangeResult.exchangeRateDtls.length-1].rangeTo);
						     	
						     	//_obj.rate = _obj.rate + '#' +  _obj.indicative_rate + '-' + destSplit[1] + '-' + 'Indicative';
									
								_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];
								
								_obj.indicativeRateDefault = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];
								Ti.API.info('_obj.indicative_rate 1',+_obj.indicative_rate );
								_obj.txtIndicativeRate.hintText = 'Enter ' + origSplit[1] + ' value (Min. ' + _obj.minIValue + ')';
								Ti.API.info('_obj.indicative_rate 2',+_obj.indicative_rate );
								try{
									var strFeeDtl = [];
									
									if(_obj.feeDtls.length > 0)
									{
										for(var fd = 0; fd < _obj.feeDtls.length; fd++)
							     		{
							     			strFeeDtl = _obj.feeDtls[fd].split('-');
							     			
							     			if(parseInt(_obj.minIValue) >= parseInt(strFeeDtl[0]) && parseInt(_obj.minIValue) <= parseInt(strFeeDtl[1])){	
												require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
												_obj.netFee = (Math.floor(parseFloat(strFeeDtl[2]))*100/100);
												break;
											}
							     		}
							     	}
						     		
						     		//var amt = ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate));
									//var amt = ((parseFloat(_obj.minIValue))*(_obj.indicative_rate));
						     		
						     		/*setTimeout(function(){
										//Ti.API.info('IF ' + ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate)));
						     			Ti.API.info('IF ' +  ((parseFloat(_obj.minIValue))*(_obj.indicative_rate)));
							     		_obj.txtIndicativeValue.value = amt.toString();	
									},200);*/
						     		
							     }catch(e){
							     	/*setTimeout(function(){
										//Ti.API.info('ELSE ' + ((parseFloat(_obj.minIValue)-parseFloat(_obj.netFee))*(_obj.indicative_rate)));
										Ti.API.info('ELSE ' +  ((parseFloat(_obj.minIValue))*(_obj.indicative_rate)));
							     		_obj.txtIndicativeValue.value = (_obj.minIValue*parseFloat(_obj.indicative_rate));	
									},200);*/
							     }
							     
							     //Ti.App.fireEvent('displayRates',{data:_obj.rate});
							     //getExchangeRate('indicative');
							     getConvExchangeRate('indicative');	
							}
							xhr2 = null;
						}catch(e){}
					}
	
					function xhr2Error(evt) {
						try{
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr2 = null;
						}catch(e){}
					}
				}catch(e){}
			}	
		}
	}
	
	corridorPaymode();
	
	// Pre login Calculations
	function getConvExchangeRate(type)
	{	
		var convAmt = null;
		var netFee = 0;
		var netConvRate = null;
		var strVal = [];
		var range = [];	
		var strFeeDtl = [];
		var amount = null;
		
		if(type == 'indicative')
		{
			amount = parseInt(_obj.txtIndicativeRate.value);
			Ti.API.info("In Indicative Amount is", +amount);
			for(var i=0; i<indicative_value.length; i++)
			{
				Ti.API.info('indicative_value[i] ==== ' + indicative_value[i]);  //target value for header
				//indicative_value[i] ==== 10001-200000~83.17
				strVal = indicative_value[i].split('~');	//10001-200000,83.17
				_obj.rate= strVal[1].split('-'); //83.17
				range = strVal[0].split('-'); //first part for range
				//myIndVal = strVal[1].split('-'); //my ind value for header
				_obj.netConvAmt111 = _obj.rate;
				Ti.API.info("In indicative",+_obj.rate);
				_obj.rate = "Indicative-"+_obj.rate;
				 Ti.App.fireEvent('displayRates',{data:_obj.rate});
				if(amount >= parseInt(range[0]) && amount <= parseInt(range[1])){
					
					require('/utils/Console').info('Range = ' + range[0] + " - " + range[1]);
					netConvRate = parseFloat(strVal[1]);
					
					break;
				}
				
			}
		
			//var myStr = _obj.rate.split('-');
			//var myStr1 = myStr[1];
			//_obj.netConvAmt111 =myStr1 ;
		
			Ti.API.info("_obj.netConvAmt111",+_obj.netConvAmt111);
			if(_obj.feeDtls.length > 0)
			{
				for(var fd = 0; fd < _obj.feeDtls.length; fd++)
		 		{
		 			strFeeDtl = _obj.feeDtls[fd].split('-');
		 			
		 			if(amount >= parseInt(strFeeDtl[0]) && amount <= parseInt(strFeeDtl[1])){	
						require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
						netFee = parseFloat(strFeeDtl[2]);
						break;
					}
		 		}
		 	}
		}
		else if(type == 'guaranteed')
		{
			amount = parseInt(_obj.txtGuaranteedRate.value);
			Ti.API.info("In Guaranteed Amount is", +amount);
			for(var i=0; i<guaranteed_value.length; i++)
			{
				Ti.API.info('guaranteed_value[i] ==== ' + guaranteed_value[i]); 
				strVal = guaranteed_value[i].split('~');
				_obj.rate= strVal[1].split('-'); 
				range = strVal[0].split('-');
				Ti.API.info("In Guaranteed",+_obj.rate);
				_obj.rate = "Guaranteed-"+_obj.rate;
				 Ti.App.fireEvent('displayRates',{data:_obj.rate});
				if(amount >= parseInt(range[0]) && amount <= parseInt(range[1])){
					
					require('/utils/Console').info('Range = ' + range[0] + " - " + range[1]);
					netConvRate = parseFloat(strVal[1]);
					Ti.API.info("Net Converted amount is", +netConvRate);
					break;
				}
				
			}
			
			if(_obj.feeDtls.length > 0)
			{
				for(var fd = 0; fd < _obj.feeDtls.length; fd++)
		 		{
		 			strFeeDtl = _obj.feeDtls[fd].split('-');
		 			
		 			if(amount >= parseInt(strFeeDtl[0]) && amount <= parseInt(strFeeDtl[1])){	
						require('/utils/Console').info('Range = ' + strFeeDtl[0] + " - " + strFeeDtl[1]);
						netFee = parseFloat(strFeeDtl[2]);
						break;
					}
		 		}
		 	}
		}
		
		Ti.API.info('netFee = '+netFee);
		
		if(isNaN(netFee))
		{
			netFee = 0;
		}
		
		//convAmt=(amount-netFee)*netConvRate;
		convAmt=amount*netConvRate;
		convAmt = convAmt.toFixed(2);
		Ti.API.info("Conv Amount is", +convAmt);
		if(convAmt < 0){
			convAmt = '';
		}
		else if(amount == 0){
			convAmt = '';
		}
		
		 if(type == 'guaranteed')
		{
			if(_obj.txtGuaranteedRate.value !== '')
			{
				_obj.txtGuaranteedValue.value = convAmt;
				Ti.API.info("Conv Guaranteed Amount is", +_obj.txtGuaranteedValue.value);
			}
			
			if(netConvRate === null || netConvRate === '')
			{
				_obj.lblGuaranteedRate.text = _obj.guaranteedRateDefault;
			}
			else
			{
				_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + netConvRate + ' ' + destSplit[1];
			}
		}	
		 else if(type == 'indicative'){
				if(_obj.txtIndicativeRate.value !== '')
				{
					_obj.txtIndicativeValue.value = convAmt;
					Ti.API.info("Conv Indicative Amount is", +_obj.txtIndicativeValue.value);
					
				}
				
				if(netConvRate === null || netConvRate === '')
				{
					//_obj.lblIndicativeRate.text = _obj.indicativeRateDefault;
					//_obj.lblIndicativeRate.text = '22.22';
					//_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.indicative_rate + ' ' + destSplit[1];
					_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + _obj.netConvAmt111 + ' ' + destSplit[1];
				}
				else
				{
					_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + netConvRate + ' ' + destSplit[1];
					//_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = 11.11 ' + destSplit[1];
				}
			
				
			}
		 else{
			 
		 }
	}
	
	function getExchangeRate(type)
	{
		var amount = '';
		
		if(type == "indicative"){
			amount = parseInt(_obj.txtIndicativeRate.value);
			Ti.API.info("*****************amount:",amount);
		}
		else if(type == "guaranteed"){
			amount = parseInt(_obj.txtGuaranteedRate.value);
		}
		
		activityIndicator.showIndicator();
		
		var xhrConvert = require('/utils/XHR');
		
		if(type == 'indicative')
		{
			var pm = _obj.indicativePaymodeCode;
		}
		
		if(type == 'guaranteed')
		{
			var pm = _obj.guaranteedPaymodeCode;
		}

		xhrConvert.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETEXCHANGERATE",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'",'+
				'"paymodeCode":"'+pm+'",'+
				'"amount":"'+amount+'",'+
				'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
				'}',
			success : xhrConvertSuccess,
			error : xhrConvertError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrConvertSuccess(evt) {
			try{
				require('/utils/Console').info('Result ======== ' + evt.result);
				
				activityIndicator.hideIndicator();
				
				if(evt.result.responseFlag === "S")
				{
					Ti.API.info("********IN SUCCESS*****************");
					if(type == 'indicative')
					{
						if(_obj.txtIndicativeRate.value !== '')
						{
							_obj.txtIndicativeValue.value = parseFloat(evt.result.indicativeDestAmount).toFixed(2);
						}
						else
						{
							_obj.txtIndicativeValue.value = '';
						}
						
						_obj.lblIndicativeRate.text = '1 ' + origSplit[1] + ' = ' + evt.result.exchangeRate + ' ' + destSplit[1];
					}
					
					if(type == 'guaranteed')
					{
						if(_obj.txtGuaranteedRate.value !== '')
						{
							_obj.txtGuaranteedValue.value = parseFloat(evt.result.indicativeDestAmount).toFixed(2);
							Ti.API.info("_obj.txtGuaranteedValue.value",+_obj.txtGuaranteedValue.value);
						}
						else
						{
							_obj.txtGuaranteedValue.value = '';
						}
						
						_obj.lblGuaranteedRate.text = '1 ' + origSplit[1] + ' = ' + evt.result.exchangeRate + ' ' + destSplit[1];
					}
				}
				xhrConvert = null;
			}catch(e){}
		}

		function xhrConvertError(evt) {
			try{
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhrConvert = null;
			}catch(e){}
		}
	}
		
	function destroy_dashboard() {
		try {
			if (_obj.dashboardView === null) {
				return;
			}

			require('/utils/Console').info('############## Remove dashboard start ##############');

			require('/utils/RemoveViews').removeViews(_obj.dashboardView);
			_obj = null;
			
			origSplit = null;
			origCC = null;
			destSplit = null;
			destCC = null;
			
			guaranteed_rate = null;
			guaranteed_value = null;
			indicative_rate = null;
			indicative_value = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_dashboard', destroy_dashboard);
			require('/utils/Console').info('############## Remove dashboard end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}

	Ti.App.addEventListener('destroy_dashboard', destroy_dashboard);

	return _obj.dashboardView;

};// Dashboard()

module.exports = Dashboard;
