exports.ForgotMPINModal = function()
{
	require('/lib/analytics').GATrackScreen('Forgot Password');
	
	var _obj = {
		style : require('/styles/ForgotPassword').ForgotPassword,
		winForgotMPIN : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		txtUserName : null,
		borderView1 : null,
		lblDOB : null,
		imgDOB : null,
		dobView : null,
		borderView2 : null,
		btnSubmit : null,
		
		dob : null,
	};
	
	_obj.winForgotMPIN = Ti.UI.createWindow(_obj.style.winForgotPassword);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winForgotMPIN);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Forgot MPIN';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.txtUserName = Ti.UI.createTextField(_obj.style.txtUsername);
	_obj.txtUserName.hintText = 'Username*';
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.dobView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblDOB = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDOB.text = 'Date of Birth*';
	_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderView2 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.dobView.addEventListener('click',function(e){
		require('/utils/DatePicker').DatePicker(_obj.lblDOB,'dob18');
	});
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.txtUserName);
	_obj.mainView.add(_obj.borderView1);
	_obj.dobView.add(_obj.lblDOB);
	_obj.dobView.add(_obj.imgDOB);
	_obj.mainView.add(_obj.dobView);
	_obj.mainView.add(_obj.borderView2);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winForgotMPIN.add(_obj.globalView);
	_obj.winForgotMPIN.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_forgotmpin();
	});
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		if(_obj.txtUserName.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter username',[L('btn_ok')]).show();
    		_obj.txtUserName.value = '';
    		_obj.txtUserName.focus();
			return;
		}
		else if((require('/lib/toml_validations').isEmail(_obj.txtUserName.value)) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid username',[L('btn_ok')]).show();
    		_obj.txtUserName.value = '';
    		_obj.txtUserName.focus();
			return;
		}
  		else if(_obj.lblDOB.text === '' || _obj.lblDOB.text === 'Date of Birth*')
		{
			require('/utils/AlertDialog').showAlert('','Please Enter your Date of Birth',[L('btn_ok')]).show();
    		_obj.lblDOB.text = 'Date of Birth*';
			return;
		}
  		else
		{
			activityIndicator.showIndicator();
			
			var dobSplit = _obj.lblDOB.text.split('/');
			var dob = dobSplit[0] + '-' + dobSplit[1] + '-' + dobSplit[2];
			
			var xhr = require('/utils/XHR');
			
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"FORGOTMPIN",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+_obj.txtUserName.value+'",'+ 
					'"dob":"'+dob+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});

			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.responseFlag === "S")
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast(e.result.message);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast(e.result.message);
					}
					
					destroy_forgotmpin();
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_forgotmpin();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
			}

			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
	});
	
	function destroy_forgotmpin()
	{
		try{
			
			require('/utils/Console').info('############## Remove login start ##############');
			
			_obj.winForgotMPIN.close();
			require('/utils/RemoveViews').removeViews(_obj.winForgotMPIN);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_forgotmpin',destroy_forgotmpin);
			require('/utils/Console').info('############## Remove login end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_forgotmpin', destroy_forgotmpin);
}; // Login()