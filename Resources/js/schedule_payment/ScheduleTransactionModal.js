exports.ScheduleTransactionModal = function()
{
	require('/lib/analytics').GATrackScreen('Schedule Transaction');
	
	var _obj = {
		style : require('/styles/schedule_payment/ScheduleTransaction').ScheduleTransaction,
		winScheduleTransaction : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		btnSchedule : null,
		tblTransaction : null,
		stepView : null,
		
		start : 0,
		last : 0,
		page : 1,
		limit : 10,
		totalPages : 1,
		totalRecords : 0,
		lastDistance : 0,
		updating : false
	};
	
	var countryName = Ti.App.Properties.getString('destinationCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	//alert(countryName[0]);
	
	_obj.winScheduleTransaction = Titanium.UI.createWindow(_obj.style.winScheduleTransaction);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winScheduleTransaction);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Schedule Payments';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.stepView = Ti.UI.createView(_obj.style.step);
	_obj.stepView.top = 40;
	
	_obj.btnSchedule = Ti.UI.createButton(_obj.style.btnSchedule);
	_obj.btnSchedule.title = 'SCHEDULE NEW PAYMENTS';
	
	//Gettransactioninfo call///
	function MygetTransactionInfo()
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETTRANSACTIONINFO",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"payModeCode":"",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S"){
				if(e.result.hasOwnProperty('achData')){
					Ti.API.info("In easy emi IF");	
					require('/js/schedule_payment/EasyEMIPaymentModal').EasyEMIPaymentModal();
					destroy_scheduletransaction();
				
				}
				else{
					Ti.API.info("In easy emi Else");
					var alertDialog = require('/utils/AlertDialog').showAlert('Transaction Error','Your ACH Bank Account has not been verified by you. Kindly verify your account by clicking on the link in the Bank Accounts section.', [L('btn_ok')]);
					alertDialog.show();
					/*alertDialog.addEventListener('click', function() {
						Ti.API.info("Hellooo");
					});*/
					alertDialog.addEventListener('click', function() {
						require('/utils/PageController').pageController('transfer');
					});
				}
				
				
			}
			else 
		{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	////
	_obj.btnSchedule.addEventListener('click',function(e){

		MygetTransactionInfo();
			
	});
	
	_obj.tblTransaction = Ti.UI.createTableView(_obj.style.tableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.stepView.add(_obj.btnSchedule);
	_obj.stepView.add(_obj.tblTransaction);
	_obj.mainView.add(_obj.stepView);
	_obj.globalView.add(_obj.mainView);
	_obj.winScheduleTransaction.add(_obj.globalView);
	_obj.winScheduleTransaction.open();
	
	_obj.tblTransaction.addEventListener('scroll',function(e){
		
		require('/utils/Console').info(_obj.page +' < '+ _obj.totalPages);
		if(_obj.page < _obj.totalPages)
		{
			if (TiGlobals.osname === 'android')
			{
		        if((e.firstVisibleItem + e.visibleItemCount) === (_obj.page * _obj.limit))
				{
					_obj.page = _obj.page + 1;
		            populateScheduleTxnPaginate();
			    }
		    }
		    else
		    {
		        var offset = e.contentOffset.y;
		        var height = e.size.height;
		        var total = offset + height;
		        var theEnd = e.contentSize.height;
		        var distance = theEnd - total;
		         
		        // going down is the only time we dynamically load,
		        // going up we can safely ignore -- note here that
		        // the values will be negative so we do the opposite
		        if (distance < _obj.lastDistance)
		        {
		            // adjust the % of rows scrolled before we decide to start fetching
		            var nearEnd = theEnd;
		 
		            if (!_obj.updating && (total >= nearEnd))
		            {
		               _obj.page = _obj.page + 1;
		               populateScheduleTxnPaginate();
		            }
		        }
		        _obj.lastDistance = distance;
		    }    
		}
	});	
	
	_obj.tblTransaction.addEventListener('click', function(e) {
		try{
			if(e.row.rw === 1)
			{
				if(e.source.sel === 'view')
				{
					 var params = {
						startDate : e.row.startDate,
						amount : e.row.amount,
						noOfPayments : e.row.noOfPayments,
						periodicity : e.row.periodicity,
						status : e.row.status,
						beneficiaryName : e.row.beneficiaryName,
						destCountry : e.row.destCountry,
						bankName : e.row.bankName,
						siType : e.row.siType,
						siNumber : e.row.siNumber
					};
					
					require('/js/schedule_payment/ScheduleTxnDetailsModal').ScheduleTxnDetailsModal(params);
				}
				
				if(e.source.sel === 'cancel')
				{
					var alertDialog = require('/utils/AlertDialog').showAlert('', 'Once you click on OK, the transaction will be deleted.', [L('btn_ok'), L('btn_cancel')]);
					alertDialog.show();
			
					alertDialog.addEventListener('click', function(evt) {
						alertDialog.hide();
						if (evt.index === 0 || evt.index === "0") {
							activityIndicator.showIndicator();
				
							var xhr = require('/utils/XHR');
							
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"SICANCELLATION",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"siType":"'+e.row.siType+'",'+
									'"siNumber":"'+e.row.siNumber+'"'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
							
							function xhrSuccess(e) {
								require('/utils/Console').info('Result ======== ' + e.result);
								activityIndicator.hideIndicator();
								
								if(e.result.responseFlag === "S")
								{
									if(TiGlobals.osname === 'android')
									{
										require('/utils/AlertDialog').toast(e.result.message);
									}
									else
									{
										require('/utils/AlertDialog').iOSToast(e.result.message);
									}
									
									populateScheduleTxn();
								}
								else
								{
									if(e.result.displayBankMessage === L('invalid_session') || e.result.message === 'Invalid Session' || e.result.message === 'Invalid Session.')
									{
										require('/lib/session').session();
										destroy_scheduletransaction();
									}
									else
									{
										require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
									}
								}
								
								xhr = null;
							}
					
							function xhrError(e) {
								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhr = null;
							}
						}
					});
				}
				
				if(e.source.sel === 'reschedule')
				{
					require('/js/my_account/KYCRescheduleModal').KYCRescheduleModal(e.row.rtrn);
				}
			}
		}catch(e){}
	});
	
	function populateScheduleTxn()
	{
		_obj.start = 1;
		_obj.last = _obj.limit;
		
		_obj.tblTransaction.setData([]);
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETSISTATEMENTDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rangeFrom":"'+_obj.start+'",'+
				'"rangeTo":"'+_obj.last+'",'+
				'"operatingmodeId":"INTL"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			_obj.totalRecords = parseInt(e.result.totalCount);	
			
			_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
			
			if(e.result.responseFlag === "S")
			{
				if(_obj.totalRecords === 0)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'schedule_txn',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : '',
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblTransaction.appendRow(row);
				}
				else
				{
					for(var i=0; i<e.result.siDetailsData.length; i++)
					{
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 106,
							backgroundColor : 'transparent',
							className : 'schedule_txn',
							startDate : e.result.siDetailsData[i].startDate,
							amount : e.result.siDetailsData[i].amount,
							noOfPayments : e.result.siDetailsData[i].noOfPayments,
							periodicity : e.result.siDetailsData[i].periodicity,
							status : e.result.siDetailsData[i].status,
							beneficiaryName : e.result.siDetailsData[i].beneficiaryName,
							destCountry : e.result.siDetailsData[i].destCountry,
							bankName : e.result.siDetailsData[i].bankName,
							siType : e.result.siDetailsData[i].type,
							siNumber : e.result.siDetailsData[i].sid,
							rw : 1
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var topView = Ti.UI.createView({
							top : 0,
							left : 0,
							right : 0,
							height : 50,
							backgroundColor : '#6f6f6f',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailTxnView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 50
						});
						
						var txnView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var dt = e.result.siDetailsData[i].startDate.split('-');
						
						var lblDate = Ti.UI.createLabel({
							text : 'Date ' + dt[0]+'/'+require('/utils/Date').monthNo(dt[1])+'/'+dt[2],
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblAmount = Ti.UI.createLabel({
							text : e.result.siDetailsData[i].amount,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						var borderView = Ti.UI.createView({
							height : 50,
							top : 0,
							left : 0,
							width:1,
							backgroundColor:'#898989'
						});
						
						var detailSendView = Ti.UI.createView({
							left : 0,
							right : 0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblSendTxt = Ti.UI.createLabel({
							text:'No. of Payments', 
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblNoPayment = Ti.UI.createLabel({
							text:e.result.siDetailsData[i].noOfPayments, 
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						var bottomView = Ti.UI.createView({
							top : 50,
							left : 0,
							right : 0,
							height : 56,
							backgroundColor : '#fff',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailBenView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var statusCheckView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblStatusHead = Ti.UI.createLabel({
							text : 'Status',
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						var lblStatus = Ti.UI.createLabel({
							text : e.result.siDetailsData[i].status,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var statusView = Ti.UI.createView({
							top : 0,
							left : 20,
							width:'50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var lblAccountStatus = Ti.UI.createLabel({
							text : 'Action:',
							height : 16,
							top : 10,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont'),
							sel:e.result.siDetailsData[i].accountStatus
						});
						
						var actionView = Ti.UI.createView({
							height : 16,
							top : 30,
							left : 0,
							width:Ti.UI.SIZE,
							layout:'horizontal',
							horizontalWrap:true
						});
						
						var lblView = Ti.UI.createLabel({
							text : 'View Details ',
							height : 20,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							sel:'view'
							
						});
						
						var lblCancel = Ti.UI.createLabel({
							text : '| Cancel EMI',
							height : 20,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							sel:'cancel'
						});
						
						txnView.add(lblDate);
						txnView.add(lblAmount);
						detailTxnView.add(txnView);
						topView.add(detailTxnView);
						topView.add(borderView);
						detailSendView.add(lblSendTxt);
						detailSendView.add(lblNoPayment);
						topView.add(detailSendView);
						statusCheckView.add(lblStatusHead);
						statusCheckView.add(lblStatus);
						detailBenView.add(statusCheckView);
						bottomView.add(detailBenView);
						statusView.add(lblAccountStatus);
						if((e.result.siDetailsData[i].action.indexOf('DETAILS,CANCEL')) > -1)
						{
							lblView.text = 'View Details ';
							lblCancel.text = '| Cancel EMI';
							actionView.add(lblView);
							actionView.add(lblCancel);	
						}
						else if((e.result.siDetailsData[i].action.indexOf('DETAILS')) > -1)
						{
							lblView.text = 'View Details ';
							actionView.add(lblView);
						}
						else if((e.result.siDetailsData[i].action.indexOf('CANCEL')) > -1)
						{
							lblCancel.text = 'Cancel EMI';
							actionView.add(lblCancel);
						}
						else
						{
							
						}
						statusView.add(actionView);
						bottomView.add(statusView);
						row.add(topView);
						row.add(bottomView);
						_obj.tblTransaction.appendRow(row);
					}
				}
				activityIndicator.hideIndicator();
			}
			else
			{
				activityIndicator.hideIndicator();
				
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session' || e.result.message === 'Invalid Session.')
				{
					require('/lib/session').session();
					destroy_scheduletransaction();
				}
				else
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'schedule_txn',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblTransaction.appendRow(row);
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
		}
	}
	
	populateScheduleTxn();
	
	function populateScheduleTxnPaginate()
	{
		_obj.updating = true;
		
		_obj.start = (((_obj.page * _obj.limit) + 1) - _obj.limit); 
		_obj.last = (_obj.page * _obj.limit);
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETSISTATEMENTDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rangeFrom":"'+_obj.start+'",'+
				'"rangeTo":"'+_obj.last+'",'+
				'"operatingmodeId":"INTL"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			_obj.totalRecords = parseInt(e.result.totalCount);	
			
			_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
			
			if(e.result.responseFlag === "S")
			{
				for(var i=0; i<e.result.siDetailsData.length; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 106,
						backgroundColor : 'transparent',
						className : 'schedule_txn',
						startDate : e.result.siDetailsData[i].startDate,
						amount : e.result.siDetailsData[i].amount,
						noOfPayments : e.result.siDetailsData[i].noOfPayments,
						periodicity : e.result.siDetailsData[i].periodicity,
						status : e.result.siDetailsData[i].status,
						beneficiaryName : e.result.siDetailsData[i].beneficiaryName,
						destCountry : e.result.siDetailsData[i].destCountry,
						bankName : e.result.siDetailsData[i].bankName,
						siType : e.result.siDetailsData[i].type,
						siNumber : e.result.siDetailsData[i].sid,
						rw : 1
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var topView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : '#6f6f6f',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailTxnView = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '50%',
						height : 50
					});
					
					var txnView = Ti.UI.createView({
						left : 0,
						right:0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var dt = e.result.siDetailsData[i].startDate.split('-');
					
					var lblDate = Ti.UI.createLabel({
						text : 'Date ' + dt[0]+'/'+require('/utils/Date').monthNo(dt[1])+'/'+dt[2],
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblAmount = Ti.UI.createLabel({
						text : e.result.siDetailsData[i].amount,
						top : 2,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var borderView = Ti.UI.createView({
						height : 50,
						top : 0,
						left : 0,
						width:1,
						backgroundColor:'#898989'
					});
					
					var detailSendView = Ti.UI.createView({
						left : 0,
						right : 0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblSendTxt = Ti.UI.createLabel({
						text:'No. of Payments', 
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblNoPayment = Ti.UI.createLabel({
						text:e.result.siDetailsData[i].noOfPayments, 
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var bottomView = Ti.UI.createView({
						top : 50,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#fff',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailBenView = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var statusCheckView = Ti.UI.createView({
						left : 0,
						right:0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblStatusHead = Ti.UI.createLabel({
						text : 'Status',
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblStatus = Ti.UI.createLabel({
						text : e.result.siDetailsData[i].status,
						top : 2,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var statusView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var lblAccountStatus = Ti.UI.createLabel({
						text : 'Action:',
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont'),
						sel:e.result.siDetailsData[i].accountStatus
					});
					
					var actionView = Ti.UI.createView({
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:true
					});
					
					var lblView = Ti.UI.createLabel({
						text : 'View Details ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						sel:'view'
						
					});
					
					var lblCancel = Ti.UI.createLabel({
						text : '| Cancel EMI',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						sel:'cancel'
					});
					
					txnView.add(lblDate);
					txnView.add(lblAmount);
					detailTxnView.add(txnView);
					topView.add(detailTxnView);
					topView.add(borderView);
					detailSendView.add(lblSendTxt);
					detailSendView.add(lblNoPayment);
					topView.add(detailSendView);
					statusCheckView.add(lblStatusHead);
					statusCheckView.add(lblStatus);
					detailBenView.add(statusCheckView);
					bottomView.add(detailBenView);
					statusView.add(lblAccountStatus);
					if((e.result.siDetailsData[i].action.indexOf('DETAILS,CANCEL')) > -1)
					{
						lblView.text = 'View Details ';
						lblCancel.text = '| Cancel EMI';
						actionView.add(lblView);
						actionView.add(lblCancel);	
					}
					else if((e.result.siDetailsData[i].action.indexOf('DETAILS')) > -1)
					{
						lblView.text = 'View Details ';
						actionView.add(lblView);
					}
					else if((e.result.siDetailsData[i].action.indexOf('CANCEL')) > -1)
					{
						lblCancel.text = 'Cancel EMI';
						actionView.add(lblCancel);
					}
					else
					{
						
					}
					statusView.add(actionView);
					bottomView.add(statusView);
					row.add(topView);
					row.add(bottomView);
					_obj.tblTransaction.appendRow(row);
					_obj.updating = false;
				}
				activityIndicator.hideIndicator();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session' || e.result.message === 'Invalid Session.')
				{
					require('/lib/session').session();
					destroy_scheduletransaction();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_scheduletransaction();
	});
	
	_obj.winScheduleTransaction.addEventListener('androidback', function(){
		destroy_scheduletransaction();
	});
	
	function destroy_scheduletransaction()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove schedule transaction start ##############');
			
			_obj.winScheduleTransaction.close();
			require('/utils/RemoveViews').removeViews(_obj.winScheduleTransaction);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_scheduletransaction',destroy_scheduletransaction);
			require('/utils/Console').info('############## Remove schedule transaction end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_scheduletransaction', destroy_scheduletransaction);
}; // ScheduleTransactionModal()
