function ReferEarn(_self) {
	Ti.App.Properties.setString('pg', 'refer_earn');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen(Ti.App.Properties.getString('pg'));

	var _obj = {
		style : require('/styles/ReferEarn').ReferEarn, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		tblRefer : null,
	};
	
	Ti.App.fireEvent('showLabel', {
		data : L('dashboard')
	});
	Ti.App.fireEvent('menuColor', {
		data : 'dashboard'
	});
	// Menu select
	Ti.App.fireEvent('showHeader');
	Ti.App.fireEvent('showMenu');
	Ti.App.fireEvent('showSearch');
	
	if(TiGlobals.osname !== 'android')
	{
		Ti.App.fireEvent('hideBack');
	}
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	//////////////////////////// Create UI ////////////////////////////
		
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);

	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Refer & Earn';
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	
	_obj.imgBack.addEventListener('click',function(){
		popView();
	});
	
	_obj.tblRefer = Ti.UI.createTableView(_obj.style.tableView);
	
	var ht = TiGlobals.osname === 'android' ? ((parseInt(TiGlobals.platformHeight) - (59+40+59))/3) : ((parseInt(TiGlobals.platformHeight) - (79+40+59))/3);
	require('/utils/Console').info(Math.ceil(ht));
	function referEarn()
	{
		for(var i=0; i<3; i++)
		{
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				height : ht,
				backgroundColor : i%2 === 0 ? '#efeff0' : '#e3e3e3',
				className : 'refer',
				rw:i
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var imgView = Ti.UI.createImageView({
				width : 175,
				height : 75
			});
			
			switch (i)
			{
				case 0:
					imgView.image = '/images/earn_manual.png';
				break;
				
				case 1:
					imgView.image = '/images/earn_email.png';
				break;
				
				case 2:
					imgView.image = '/images/earn_social.png';
				break;
				
			}

			var imgArrow = Ti.UI.createImageView({
				image : '/images/link_arrow_red.png',
				right : 20,
				width : 10,
				height : 15
			});
			
			row.add(imgView);
			row.add(imgArrow);
			_obj.tblRefer.appendRow(row);
		}
	}
	
	referEarn();
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.tblRefer);
	_obj.globalView.add(_obj.mainView);
	
	_obj.tblRefer.addEventListener('click', function(event){
		switch (event.row.rw)
		{
			case 0:
				// Manual Refer
				if(Titanium.Network.online)
				{
					require('/js/mor/ManualModal').ManualModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 1:
				// Email Refer
				if(Titanium.Network.online)
				{
					require('/js/mor/ReferEmailModal').ReferEmailModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 2:
				// Social Refer
				if(Titanium.Network.online)
				{
					require('/js/mor/ReferSocialModal').ReferSocialModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
		}
	});

		
	function destroy_refer_earn() {
		try {
			if (_obj.globalView === null) {
				return;
			}

			require('/utils/Console').info('############## Remove ReferEarn start ##############');

			require('/utils/RemoveViews').removeViews(_obj.globalView);
			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_refer_earn', destroy_refer_earn);
			require('/utils/Console').info('############## Remove ReferEarn end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}


	Ti.App.addEventListener('destroy_refer_earn', destroy_refer_earn);

	return _obj.globalView;

};// ReferEarn()

module.exports = ReferEarn;