exports.RegisterModal = function()
{
	require('/lib/analytics').GATrackScreen('Registration');
	
	var _obj = {
		style : require('/styles/Register').Register,
		winRegister : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		progressView : null,
		progressMainView : null,
		lblPercent : null,
		lblComplete : null,
		imgProgress : null,
		scrollableView : null,
		
		//Step 1
		stepView1 : null,
		txtFirstName : null,
		borderViewS11 : null,
		txtLastName : null,
		borderViewS12 : null,
		txtEmail : null,
		borderViewS13 : null,
		doneMobile : null,
		mobileView : null,
		lblMobile : null,
		txtMobile : null,
		borderViewS14 : null,
		dobView : null,
		lblDOB : null,
		imgDOB : null,
		borderViewS15 : null,
		doneDay : null,
		txtDayNo : null,
		borderViewS16 : null,
		genderView : null,
		lblGender : null,
		lblMale : null,
		lblFemale : null,
		citizenshipView : null,
		lblCitizenship : null,
		imgCitizenship : null,
		borderViewS18 : null,
		btnStep1 : null,
		
		//Step 2
		stepView2 : null,
		txtAptNo : null,
		borderViewS21 : null,
		txtBldgNo : null,
		borderViewS22 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		cityView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,
		stateView : null,
		lblState : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,
		occupationView : null,
		lblOccupation : null,
		imgOccupation : null,
		borderViewS210 : null,
		btnStep2 : null,
		
		// Step 3
		stepView3 : null,
		lblPasswordSetup : null,
		lblUsername : null,
		txtPassword : null,
		borderViewS31 : null,
		txtConfirmPassword : null,
		borderViewS32 : null,
		lblPasswordValidate : null,
		btnStep3 : null,
		
		//Step 4
		stepView4 : null,
		lblHow : null,
		HDYView : null,
		lblHDY : null,
		imgHDY : null,
		borderViewS41 : null,
		txtHDY : null,
		borderViewS42 : null,
		assuranceView : null,
		imgAssurance : null,
		lblAssurance : null,
		termsView : null,
		lblTerms : null,
		lblAgreement : null,
		lblAnd : null,
		lblPrivacy : null,
		btnRegister : null,
	
		gender : null,
		
		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
		assurance : null,
		dob : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winRegister = Titanium.UI.createWindow(_obj.style.winRegister);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winRegister);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Register';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.progressView = Ti.UI.createView(_obj.style.progressView);
	_obj.progressMainView = Ti.UI.createView(_obj.style.progressMainView);
	_obj.lblPercent = Ti.UI.createLabel(_obj.style.lblPercent);
	_obj.lblPercent.text = '0%';
	_obj.lblComplete = Ti.UI.createLabel(_obj.style.lblComplete);
	_obj.lblComplete.text = 'Profile Completed';
	_obj.imgProgress = Ti.UI.createImageView(_obj.style.imgProgress);
	_obj.imgProgress.image = '/images/progress_0.png';
	
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	_obj.progressMainView.add(_obj.lblPercent);
	_obj.progressMainView.add(_obj.lblComplete);
	_obj.progressMainView.add(_obj.imgProgress);
	_obj.progressView.add(_obj.progressMainView);
	//_obj.mainView.add(_obj.progressView);
	
	function progressBar(val){
		_obj.lblPercent.text = val+'%';
		_obj.imgProgress.image = '/images/progress_'+val+'.png';
	}
	
	/////////////////// Step 1 ///////////////////
	//First Name
	_obj.stepView1 = Ti.UI.createScrollView(_obj.style.stepView1);
	_obj.txtFirstName = Ti.UI.createTextField(_obj.style.txtFirstName);
	_obj.txtFirstName.hintText = '*First Name';
	_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);
	
	//Last NAme
	_obj.txtLastName = Ti.UI.createTextField(_obj.style.txtLastName);
	_obj.txtLastName.hintText = '*Last Name';
	_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
	
	//Email
	_obj.txtEmail = Ti.UI.createTextField(_obj.style.txtEmail);
	//_obj.txtEmail.hintText = '*Your Email ID (Username)';
	_obj.txtEmail.hintText = '*Your Email ID (will be your login id)';
	_obj.txtEmail.keyboardType = Ti.UI.KEYBOARD_TYPE_EMAIL;
	_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtEmail.addEventListener('blur',function(){
		
		if(_obj.txtEmail.value === '' || _obj.txtEmail.value === 'Email')
		{
			return;
		}
		else
		{
			
		}
		//check user availability
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"CHECKUSERAVAILABILITY",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+_obj.txtEmail.value+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "F")
			{  try{
				if(TiGlobals.osname === 'android')
				{
					Ti.API.info("+++++");
					require('/utils/AlertDialog').toast(e.result.Message);
					Ti.API.info("-----");
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.Message);
				}
				
				_obj.txtEmail.value = '';
			}catch(e){}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	//Password
	_obj.txtPassword = Ti.UI.createTextField(_obj.style.txtPassword);
	_obj.txtPassword.hintText = '*Password';
	//_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderViewS31);
	_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderView);
	_obj.txtConfirmPassword = Ti.UI.createTextField(_obj.style.txtConfirmPassword);
	_obj.txtConfirmPassword.hintText = '*Re-type Password';
	//_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderViewS32);
	_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);
	_obj.lblPasswordValidate = Ti.UI.createLabel(_obj.style.lblPasswordValidate);
	_obj.lblPasswordValidate.text = 'Password must be a minimum of 8 characters with at least one letter and one number';
	
	//DOB
	_obj.dobView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblDOB = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDOB.text = '*Date of Birth';
	_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS14 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneDay = Ti.UI.createButton(_obj.style.done);
	_obj.doneDay.addEventListener('click',function(){
		_obj.txtDayNo.blur();
	});
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobile.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	
	//Mobile
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobile = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblMobile.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = '*Mobile Number';
	//_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
    _obj.txtMobile.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
   
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobile];
	_obj.borderViewS15 = Ti.UI.createView(_obj.style.borderView);
	
	//Day time no.
	_obj.dayView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblDay = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblDay.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtDayNo = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtDayNo.hintText = 'Day Time Number*';
	//_obj.txtDayNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDayNo.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
	
	_obj.txtDayNo.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS16 = Ti.UI.createView(_obj.style.borderView);
	
	//Gender
	_obj.genderView = Ti.UI.createView(_obj.style.genderView);
	_obj.lblGender = Ti.UI.createLabel(_obj.style.lblGender);
	_obj.lblGender.text = '*Gender';
	_obj.lblMale = Ti.UI.createLabel(_obj.style.lblMale);
	_obj.lblFemale = Ti.UI.createLabel(_obj.style.lblFemale);
	
	_obj.lblMale.addEventListener('click',function(e){
		e.source.backgroundImage = '/images/male_sel.png';
	    _obj.lblFemale.backgroundImage = '/images/female_unsel.png';
	    _obj.gender = 'M';
	});
	
	_obj.lblFemale.addEventListener('click',function(e){
		e.source.backgroundImage = '/images/female_sel.png';
	    _obj.lblMale.backgroundImage = '/images/male_unsel.png';
	    _obj.gender = 'F';
	});
	
	//CitizenShip
	_obj.citizenshipView = Ti.UI.createView(_obj.style.citizenshipView);
	_obj.lblCitizenship = Ti.UI.createLabel(_obj.style.lblCitizenship);
	_obj.lblCitizenship.text = '*Country of Citizenship';
	_obj.imgCitizenship = Ti.UI.createImageView(_obj.style.imgCitizenship);
	_obj.borderViewS18 = Ti.UI.createView(_obj.style.borderView);
	
	//_obj.btnStep1 = Ti.UI.createButton(_obj.style.btnStep1);
	//_obj.btnStep1.title = 'CONTINUE';
	
	
	_obj.txtPromoCode = Ti.UI.createTextField(_obj.style.txtFirstName);
	_obj.txtPromoCode .hintText = 'Do you have promo code?';
	_obj.borderViewS33= Ti.UI.createView(_obj.style.borderView);
	
	//Adding components in step 1
	//First Name
	_obj.stepView1.add(_obj.txtFirstName);
	_obj.stepView1.add(_obj.borderViewS11);
	//Last Name
	_obj.stepView1.add(_obj.txtLastName);
	_obj.stepView1.add(_obj.borderViewS12);
	//Email ID
	_obj.stepView1.add(_obj.txtEmail);
	_obj.stepView1.add(_obj.borderViewS13);
	//Mobile no
	_obj.mobileView.add(_obj.lblMobile);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.stepView1.add(_obj.mobileView);
	_obj.stepView1.add(_obj.borderViewS15);
	
	//Password
	//_obj.stepView1.add(_obj.lblUsername);
	//_obj.stepView1.add(_obj.txtPassword);
	_obj.stepView1.add(_obj.txtPassword);
	_obj.stepView1.add(_obj.borderViewS31);
	_obj.stepView1.add(_obj.txtConfirmPassword);
	_obj.stepView1.add(_obj.borderViewS32);
	//_obj.stepView1.add(_obj.lblPasswordValidate);
	
	//Country of citizenship
	_obj.citizenshipView.add(_obj.lblCitizenship);
	_obj.citizenshipView.add(_obj.imgCitizenship);
	_obj.stepView1.add(_obj.citizenshipView);
	_obj.stepView1.add(_obj.borderViewS18);
	
	
	/*Day time no.
	 * _obj.dayView.add(_obj.lblDay);
	_obj.dayView.add(_obj.txtDayNo);
	_obj.stepView1.add(_obj.dayView);
	_obj.stepView1.add(_obj.borderViewS16);*/
	
	//Gender
	_obj.genderView.add(_obj.lblGender);
	_obj.genderView.add(_obj.lblMale);
	_obj.genderView.add(_obj.lblFemale);
	_obj.stepView1.add(_obj.genderView);
	
	//DOB
	_obj.dobView.add(_obj.lblDOB);
	_obj.dobView.add(_obj.imgDOB);
	_obj.stepView1.add(_obj.dobView);
	_obj.stepView1.add(_obj.borderViewS14);
	
	//Promo code --new field
	_obj.stepView1.add(_obj.txtPromoCode);
	_obj.stepView1.add(_obj.borderViewS33);
	
	//How did you hear about us
    
	_obj.HDYView = Ti.UI.createView(_obj.style.HDYView);
	_obj.lblHDY = Ti.UI.createLabel(_obj.style.lblHDY);
	_obj.lblHDY.text = 'How did you hear about us?';
	_obj.imgHDY = Ti.UI.createImageView(_obj.style.imgHDY);
	_obj.borderViewS41 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtHDY = Ti.UI.createTextField(_obj.style.txtHDY);
	_obj.txtHDY.hintText = '*Enter Source Name';
	_obj.borderViewS42 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.termsView = Ti.UI.createView(_obj.style.termsView);
	_obj.lblTerms = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblTerms.text = 'By clicking on "I Agree" you are accepting Remit2India'+'s ';
	
	_obj.lblAgreement = Ti.UI.createLabel(_obj.style.lblAgreement);
	_obj.lblAgreement.text = 'Terms and Conditions ';
	
	_obj.lblAnd = Ti.UI.createLabel(_obj.style.lblAnd);
	_obj.lblAnd.text = '& ';
	
	_obj.lblPrivacy = Ti.UI.createLabel(_obj.style.lblPrivacy);
	_obj.lblPrivacy.text = 'Privacy Policy';
	
	_obj.btnRegister = Ti.UI.createButton(_obj.style.btnRegister);
	_obj.btnRegister.title = 'I Agree, Create My Account';
	
	_obj.stepView1.add(_obj.lblHow);
	_obj.HDYView.add(_obj.lblHDY);
	_obj.HDYView.add(_obj.imgHDY);
	_obj.stepView1.add(_obj.HDYView);
	_obj.stepView1.add(_obj.borderViewS41);
	_obj.stepView1.add(_obj.txtHDY);
	_obj.stepView1.add(_obj.borderViewS42);
	
	_obj.termsView.add(_obj.lblTerms);
	_obj.termsView.add(_obj.lblAgreement);
	_obj.termsView.add(_obj.lblAnd);
	_obj.termsView.add(_obj.lblPrivacy);
	_obj.stepView1.add(_obj.termsView);
	_obj.stepView1.add(_obj.btnRegister);
	
	_obj.lblAgreement.addEventListener('click',function(e){
		require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','about','terms-and-conditions');
	});
	
	_obj.lblPrivacy.addEventListener('click',function(e){
		//require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','about','terms-and-conditions');
		require('/js/StaticPagesModal').StaticPagesModal('Privacy Policy','about','privacy-policy');
	
	});
	
	
	
	//_obj.stepView1.add(_obj.btnStep1);
	
	_obj.btnRegister.addEventListener('click',function(e){
		
		var fName = require('/lib/toml_validations').trim(_obj.txtFirstName.value);
		var lName = require('/lib/toml_validations').trim(_obj.txtLastName.value);
		
		////////////////////// First Name ////////////////////
    	if(_obj.txtFirstName.value === '' || _obj.txtFirstName.value === 'First Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.focus();
			return;
		}
		else if(_obj.txtFirstName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in First Name field',[L('btn_ok')]).show();
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter your First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(fName.length <= 1 && fName.length > 0)
		{	
			require('/utils/AlertDialog').showAlert('','First Name cannot be a single alphabet. Please enter your complete First Name',[L('btn_ok')]).show();
    		_obj.txtFirstName.focus();
    		return;
		}
		else
		{
			
		}
		////////////////////// Last Name ////////////////////
		if(_obj.txtLastName.value === '' || _obj.txtLastName.value === 'Last Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
    		return;
		}
		else if(_obj.txtLastName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Last Name field',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
			return;
		}
		else if(lName.length <= 1 && lName.length > 0) // Last NAME SHOULD BE GREATER THEN 1
		{	
			require('/utils/AlertDialog').showAlert('','Last Name cannot be a single alphabet. Please enter your complete Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.focus();
			return;
		}
		else
		{
			
		}//Pallavi code:defect no.116
		/*if(_obj.txtLastName.value === _obj.txtFirstName.value)
		{
			require('/utils/AlertDialog').showAlert('','First Name and Last Name cannot be the same',[L('btn_ok')]).show();
    		_obj.txtDayNo.focus();
			return;
		}*/
		////////////////////// Email ////////////////////
		if(_obj.txtEmail.value === '' || _obj.txtEmail.value === 'Email')
		{
			require('/utils/AlertDialog').showAlert('','Please enter Email Address',[L('btn_ok')]).show();
    		_obj.txtEmail.focus();
			return;
		}
		else if((require('/lib/toml_validations').isEmail(_obj.txtEmail.value)) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Email Address',[L('btn_ok')]).show();
    		_obj.txtEmail.focus();
			return;
		}
		else
		{
			
		}
		
		////////////////////// DOB ////////////////////
		if(_obj.lblDOB.text === '' || _obj.lblDOB.text === 'Date of Birth*')
		{
			require('/utils/AlertDialog').showAlert('','Please Enter your Date of Birth',[L('btn_ok')]).show();
    		_obj.lblDOB.text = 'Date of Birth*';
			return;
		}
		else
		{
			
		}
		
		////////////////////// Mobile No ////////////////////
		if(_obj.txtMobile.value === '' || _obj.txtMobile.value === 'Mobile Number*')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.focus();
			return;
		}
		else if(countryName[0] === 'United Kingdom')
		{ 						
			if(_obj.txtMobile.value.charAt(0) === 0 && _obj.txtMobile.value != '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Mobile Number without 0 in the beginning',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
			else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
		|| countryName[0] === 'Finland' || countryName[0] === 'France' 
		|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
		|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
		|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
		 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal' || countryName[0] === 'Belgium' || countryName[0] === 'Netherlands')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(countryName[0] === 'Germany')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 11))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Cyprus')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.focus();
			return;
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 11) && countryName[0] === 'New Zealand')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.focus();
			return;
		}
		else if((_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Spain')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.focus();
			return;
		}
		else if((_obj.txtMobile.value.length < 10 || _obj.txtMobile.value.length > 11) && countryName[0] === 'Austria')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.focus();
			return;
		}
		else if((_obj.txtMobile.value.length < 7 || _obj.txtMobile.value.length > 9) && ((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')))
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.focus();
			return;
		}
		else
		{
			
		}
		
		/*
		////////////////////// Daytime No ////////////////////
		//if UAE than it is not cumpusory
		if(countryName[0] != 'United Arab Emirates' && countryName[0] != 'UAE') 
		{		
			if(_obj.txtDayNo.value === '' || _obj.txtDayNo.value === 'Day Time Number*')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDayNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDayNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.focus();
				return;
			}
			else if((_obj.txtDayNo.value.length < 9 || _obj.txtDayNo.value.length > 10) && countryName[0] === 'Spain')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.focus();
				return;
			}
			else if(_obj.txtDayNo.value.length != 0 && _obj.txtDayNo.value.length < 7 && countryName[0] === 'New Zealand')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.focus();
				return;
			}
			else if((_obj.txtDayNo.value.length < 3 || _obj.txtDayNo.value.length > 9) && countryName[0] === 'Germany')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.focus();
				return;
			}
			else if(countryName[0] === 'United Kingdom')
			{
				if(_obj.txtDayNo.value.charAt(0) === 0 && _obj.txtDayNo.value != '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your Day Time Number without 0 in the beginning',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
				else if(_obj.txtDayNo.value.length != 0 && (_obj.txtDayNo.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
			}
			else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
			|| countryName[0] === 'Finland' || countryName[0] === 'France' 
			|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
			|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
			|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
			 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
			{ 									
				if(_obj.txtDayNo.value.length != 0 && (_obj.txtDayNo.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				} //Pallavi code
				else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDayNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.focus();
				return;
			}
			}
			else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal')
			{ 									
				if(_obj.txtDayNo.value.length != 0 && (_obj.txtDayNo.value.length < 9))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
			}
			else if(countryName[0] === 'Belgium' || countryName[0] === 'Netherlands' || countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
			{ 									
				if(_obj.txtDayNo.value.length != 0 && (_obj.txtDayNo.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
			}
		}
		if(((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')) && _obj.txtDayNo.value.length > 0)
		{
			if(_obj.txtDayNo.value.length < 7 || _obj.txtDayNo.value.length > 9)
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.focus();
				return;
			}
		}
		
		if(_obj.txtMobile.value === _obj.txtDayNo.value)
		{
			require('/utils/AlertDialog').showAlert('','Day Time No. and Mobile No. cannot be the same',[L('btn_ok')]).show();
    		_obj.txtDayNo.focus();
			return;
		}*/
		////////////////////// Gender ////////////////////
		if(_obj.gender === null)
		{
			require('/utils/AlertDialog').showAlert('','Please select Gender',[L('btn_ok')]).show();
    		return;
		}
		//Citizenship Validation Start Here
		else if(_obj.lblCitizenship.text === '' || _obj.lblCitizenship.text === 'Citizenship*')
		{
			require('/utils/AlertDialog').showAlert('','Please select Citizenship',[L('btn_ok')]).show();
    		_obj.lblCitizenship.text = 'Citizenship*';
			return;	
		}
		else
		{
			// Do nothing
		}
		
		
		//Password and confirm password validation
		
		if(_obj.txtPassword.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter password',[L('btn_ok')]).show();
    		_obj.txtPassword.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtPassword.value) == false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of password',[L('btn_ok')]).show();
    		_obj.txtPassword.focus();
			return;
		}
		else if(_obj.txtConfirmPassword.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter Re-type password',[L('btn_ok')]).show();
    		_obj.txtConfirmPassword.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtConfirmPassword.value) == false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Re-type password',[L('btn_ok')]).show();
    		_obj.txtConfirmPassword.focus();
			return;
		}
		else if(_obj.txtPassword.value.length != 0 && _obj.txtConfirmPassword.value.length != 0)
		{
			if(!((/\d/.test(_obj.txtPassword.value)) && (/[a-z]/i.test(_obj.txtPassword.value))))
			{
				require('/utils/AlertDialog').showAlert('','Password should be alpha-numeric',[L('btn_ok')]).show();
	    		_obj.txtPassword.focus();
	    		_obj.txtConfirmPassword.value = '';
				return;
			}
			else if(_obj.txtPassword.value.length < 8 || _obj.txtPassword.value.length > 20)
			{
				require('/utils/AlertDialog').showAlert('','Password should be of minimum 8 and maximum 20 characters',[L('btn_ok')]).show();
	    		_obj.txtPassword.focus();
	    		_obj.txtConfirmPassword.value = '';
				return;
			}
			else if(_obj.txtPassword.value != _obj.txtConfirmPassword.value)
			{
				require('/utils/AlertDialog').showAlert('','Password mismatched please retype your password',[L('btn_ok')]).show();
	    		_obj.txtConfirmPassword.focus();
				return;
			}
			else
			{
				
			}
		}
		
		// Call CHECKUSERAVAILABILITY
		
		/*activityIndicator.showIndicator();
		
		var xhrCheck = require('/utils/XHR');
		
		xhrCheck.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"CHECKUSERAVAILABILITY",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+_obj.txtEmail.value+'"'+
				'}',
			success : xhrCheckSuccess,
			error : xhrCheckError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrCheckSuccess(evt) {
			if(evt.result.responseFlag === "F")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(evt.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(evt.result.Message);
				}
				
				activityIndicator.hideIndicator();
			}
			else
			{
				// Call LEADREQ
						
				var xhrLead = require('/utils/XHR');

				xhrLead.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"LEADREQ",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"name":"'+_obj.txtFirstName.value + ' ' + _obj.txtLastName.value+'",'+ 
						'"remitEmailId":"'+_obj.txtEmail.value+'",'+
						//'"phoneNumber":"'+_obj.txtDayNo.value+'",'+
						'"phoneNumber":"'+_obj.txtMobile.value+'",'+ //changed on 1/2/17
						'"country":"'+_obj.lblDOB.text+'"'+
						'}',
					success : xhrLeadSuccess,
					error : xhrLeadError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhrLeadSuccess(e) {
					activityIndicator.hideIndicator();
					_obj.lblUsername.text = _obj.txtEmail.value;
					_obj.scrollableView.moveNext();
					progressBar(25);
					xhrLead = null;
				}
		
				function xhrLeadError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhrLead = null;
				}
			}
				
			xhrCheck = null;
		}

		function xhrCheckError(evt) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhrCheck = null;
		}*/
		
		
		///Call remit registration api///
		
		activityIndicator.showIndicator();
		var dobSplit = _obj.lblDOB.text.split('/');
		var dob = dobSplit[0] + '-' + dobSplit[1] + '-' + dobSplit[2];
			
		var xhr = require('/utils/XHR');
		
		/*var occupation = _obj.lblOccupation.text;
		
		// if Arizona US then update user details
		if(countryName[0] === 'United States' && _obj.state === 'Arizona')
		{
			if(occupation === 'Others')
			{
				occupation = _obj.txtOccupation.value;
			}
		}
		else
		{
			occupation = '';
		}*/
	
		xhr.call({
			
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"REMITREGISTRATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"firstName":"'+_obj.txtFirstName.value+'",'+
				'"middleName":" ",'+
				'"lastName":"'+_obj.txtLastName.value+'",'+
				'"country":"'+countryName[0]+'",'+
				//'"dayTimePhone":"'+_obj.txtDayNo.value+'",'+
				'"dayTimePhone":" ",'+
				'"mobileNumber":"'+_obj.lblMobile.text+_obj.txtMobile.value+'",'+
				'"remitEmailId":"'+_obj.txtEmail.value+'",'+
				'"dob":"'+dob+'",'+
				'"gender":"'+_obj.gender+'",'+
				'"knowAboutUs":"'+_obj.txtHDY.value+'",'+
				'"address":{"flat":" ","building":" ","street1":" ","street2":" ","Locality":" ","subLocality":" "},'+
			
				'"state":" ",'+
				'"city":" ",'+
				'"zip":" ",'+
				'"password":"'+require('/lib/Crypt/Base64').encode(_obj.txtPassword.value)+'",'+
				'"nationality":"'+_obj.lblCitizenship.text+'",'+
				'"occupation":" ",'+
				'"assureTransfer":" ",'+
				'}',
								
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

function xhrSuccess(e) {
		
			
			if(e.result.responseFlag === "S")
			{
				
				var logId = e.result.loginId;
				var sesId = e.result.sessionId;
				var ownId = e.result.ownerId;
				    Ti.App.Properties.setString('ownerId',e.result.ownerId);
					Ti.App.Properties.setString('loginId',e.result.loginId);
					Ti.App.Properties.setString('sessionId',e.result.sessionId);
				activityIndicator.hideIndicator();
				
				var name = _obj.txtFirstName.value;
				/*setTimeout(function(){ 
					 if(countryCode[0] === 'US'){
						
							require('/js/kyc/us/KYCModal').KYCModal();
							
							destroy_register();
						}
						else{
						
							destroy_register();
						}
				    },
						
				    2000); */
				require('/js/RegisterModalSuccess').RegisterSuccess(name);
				destroy_register();
				/*if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(L('register_success'));
				}
				else
				{
			
					require('/utils/AlertDialog').iOSToast(L('register_success'));
				}*/
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	
	});
	// Set HintText for mobile and day nos
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'United Kingdom' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;	*/	
			break;
		}
		case 'Australia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			/*_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;*/
			break;
		 }
		case 'Singapore' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			/*_obj.txtDayNo.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDayNo.maxLength = 8;*/
			break;
		 }
		 case 'Germany' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			/*_obj.txtDayNo.hintText = 'Day Time Number*';
			_obj.txtDayNo.maxLength = 14;*/
			break;
		 }			 
		 case 'Canada' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Belgium' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			/*_obj.txtDayNo.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDayNo.maxLength = 8;*/
			break;
		}		
		case 'Cyprus' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}			
		case 'Finland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}			
		case 'France' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Greece' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Ireland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Italy' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Japan' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Malta' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			/*_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;*/
			break;
		}
		case 'Portugal' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			/*_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;*/
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		
		case 'Austria' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		
		case 'New Zealand' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			/*_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;*/
			break;
		}
		
		case 'Spain' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			/*_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;*/
			break;
		}
		
		case 'United Arab Emirates' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			/*_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;*/
			break;
		}
		
		default:
		{
			break;
		}
	}
	
	/////////////////// Step 2 ///////////////////
	//All code moved in updateuserdetails.js
	
	
	/////////////////// Step 3 ///////////////////
	
	
	/////////////////// Step 4 ///////////////////
	
	
	_obj.HDYView.addEventListener('click',function(e){
		activityIndicator.showIndicator();
		var howOptions = [];
		var howOptionsTxt = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"howdidyouhereaboutus",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].howdidyouhereaboutus.length;i++)
				{
					howOptions.push(e.result.response[0].howdidyouhereaboutus[i].title);
					howOptionsTxt.push(e.result.response[0].howdidyouhereaboutus[i].asso_text);
				}
				
				var optionDialogHow = require('/utils/OptionDialog').showOptionDialog('How did you hear about us?',howOptions,-1);
				optionDialogHow.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogHow.addEventListener('click',function(evt){
					if(optionDialogHow.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblHDY.text = optionDialogHow.options[evt.index];
						_obj.txtHDY.hintText = howOptionsTxt[evt.index];
						optionDialogHow = null;
						
						// MOR validate
						
						_obj.doneHDY = Ti.UI.createButton(_obj.style.done);
						_obj.doneHDY.addEventListener('click',function(){
							_obj.txtHDY.blur();
						});
						
						if(_obj.lblHDY.text === 'Money on Referral (MOR)')
						{
							_obj.txtHDY.value = '';
							_obj.txtHDY.maxLength = 7;
							_obj.txtHDY.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
							_obj.txtHDY.keyboardToolbar = [_obj.doneHDY];
						}
						else
						{
							_obj.txtHDY.value = '';
							_obj.txtHDY.maxLength = 500;
							_obj.txtHDY.keyboardType = Ti.UI.KEYBOARD_DEFAULT;
							_obj.txtHDY.keyboardToolbar = [];
						}
						optionDialogHow = null;
					}
					else
					{
						optionDialogHow = null;
						_obj.lblHDY.text = 'Select Source*';
						_obj.txtHDY.hintText = 'Enter Source Name*';
					}
				});	
			}
			else
			{
				require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	
	_obj.dobView.addEventListener('click',function(e){
		require('/utils/DatePicker').DatePicker(_obj.lblDOB,'dob18');
	});
	
	function citizenship()
	{
		activityIndicator.showIndicator();
		var citizenshipOptions = [];
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+Ti.App.Properties.getString('destinationCountryCurName')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
				
				for(var i=0;i<splitArr.length;i++)
				{
					citizenshipOptions.push(splitArr[i]);
				}
				
				var optionDialogCitizenship = require('/utils/OptionDialog').showOptionDialog('Citizenship',citizenshipOptions,-1);
				optionDialogCitizenship.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogCitizenship.addEventListener('click',function(evt){
					if(optionDialogCitizenship.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCitizenship.text = optionDialogCitizenship.options[evt.index];
						optionDialogCitizenship = null;
					}
					else
					{
						optionDialogCitizenship = null;
						_obj.lblCitizenship.text = 'Citizenship*';
					}
				});	
			}
			else
			{
				require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.citizenshipView.addEventListener('click',function(){
		citizenship();
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_register();
	});
	
	//_obj.scrollableView.views = [_obj.stepView1,_obj.stepView2,_obj.stepView3,_obj.stepView4];
	_obj.scrollableView.views = [_obj.stepView1];
	_obj.mainView.add(_obj.scrollableView);
	_obj.globalView.add(_obj.mainView);
	_obj.winRegister.add(_obj.globalView);
	_obj.winRegister.open();
	
	_obj.winRegister.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_register();
				alertDialog = null;
			}
		});
	});
	
	function destroy_register()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove registration start ##############');
			
			_obj.winRegister.close();
			require('/utils/RemoveViews').removeViews(_obj.winRegister);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_register',destroy_register);
			require('/utils/Console').info('############## Remove registration end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_register', destroy_register);
}; // RegisterModal()
