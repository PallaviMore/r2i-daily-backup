exports.CreateMPINModal = function(){
	
	require('/lib/analytics').GATrackScreen('Create mPIN');
	
	var _obj = {
		style : require('/styles/CreateMPIN').CreateMPIN,
		winCreateMPIN : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		done : [],
		txtMPIN : [],
		doneConfirm : [],
		txtMPINConfirm : [],
		btnSubmit : null,
		
	};
	
	_obj.winCreateMPIN = Ti.UI.createWindow(_obj.style.winCreateMPIN);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winCreateMPIN);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Create mPIN';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	// mPIN View
	
	_obj.lblEnterMPIN = Ti.UI.createLabel(_obj.style.lblMPIN);
	_obj.lblEnterMPIN.text = 'Enter mPIN';
	
	_obj.mPINView = Ti.UI.createView(_obj.style.mPINView);
	
	for(var i=0; i<6; i++)
	{
		_obj.done[i] = Ti.UI.createButton(_obj.style.done);
	
		_obj.done[i].addEventListener('click',function(){
			
			for(var j=0; j<6; j++)
			{
				_obj.txtMPIN[j].blur();
			}
		});
		
		_obj.txtMPIN[i] = Ti.UI.createTextField(_obj.style.txtMPIN);
		_obj.txtMPIN[i].left = 0;
		_obj.txtMPIN[i].sel = i;
		_obj.txtMPIN[i].editable = true;
		
		if(i>0)
		{
			_obj.txtMPIN[i].left = '4%';
			_obj.txtMPIN[i].editable = false;
		}
		
		_obj.txtMPIN[i].width = '12%';
		_obj.txtMPIN[i].maxLength = 1;
		_obj.txtMPIN[i].passwordMask = true;
		_obj.txtMPIN[i].keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtMPIN[i].keyboardToolbar = [_obj.done[i]];
		
		_obj.mPINView.add(_obj.txtMPIN[i]);
		
		_obj.txtMPIN[i].addEventListener('change',function(){
			if(this.value !== '')
			{
				if(this.sel<5)
				{
					_obj.txtMPIN[this.sel+1].editable = true;
					_obj.txtMPIN[this.sel+1].focus();
				}
			}
		});
	}
	
	// Confirm
	
	_obj.lblConfirmMPIN = Ti.UI.createLabel(_obj.style.lblMPIN);
	_obj.lblConfirmMPIN.text = 'Confirm mPIN';
	
	_obj.mPINConfirmView = Ti.UI.createView(_obj.style.mPINView);
	
	for(var i=0; i<6; i++)
	{
		_obj.doneConfirm[i] = Ti.UI.createButton(_obj.style.done);
	
		_obj.doneConfirm[i].addEventListener('click',function(){
			
			for(var j=0; j<6; j++)
			{
				_obj.txtMPINConfirm[j].blur();
			}
		});
		
		_obj.txtMPINConfirm[i] = Ti.UI.createTextField(_obj.style.txtMPIN);
		_obj.txtMPINConfirm[i].left = 0;
		_obj.txtMPINConfirm[i].sel = i;
		_obj.txtMPINConfirm[i].editable = true;
		
		if(i>0)
		{
			_obj.txtMPINConfirm[i].left = '4%';
			_obj.txtMPINConfirm[i].editable = false;
		}
		
		_obj.txtMPINConfirm[i].width = '12%';
		_obj.txtMPINConfirm[i].maxLength = 1;
		_obj.txtMPINConfirm[i].passwordMask = true;
		_obj.txtMPINConfirm[i].keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtMPINConfirm[i].keyboardToolbar = [_obj.doneConfirm[i]];
		
		_obj.mPINConfirmView.add(_obj.txtMPINConfirm[i]);
		
		_obj.txtMPINConfirm[i].addEventListener('change',function(){
			if(this.value !== '')
			{
				if(this.sel<5)
				{
					_obj.txtMPINConfirm[this.sel+1].editable = true;
					_obj.txtMPINConfirm[this.sel+1].focus();
				}
			}
		});
	}
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblEnterMPIN);
	_obj.mainView.add(_obj.mPINView);
	_obj.mainView.add(_obj.lblConfirmMPIN);
	_obj.mainView.add(_obj.mPINConfirmView);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winCreateMPIN.add(_obj.globalView);
	_obj.winCreateMPIN.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		Ti.App.Properties.setString('mPIN','0');
		destroy_mpin();
	});
	
	_obj.winCreateMPIN.addEventListener('androidback',function(e){
		Ti.App.Properties.setString('mPIN','0');
		destroy_mpin();
	});
	
	_obj.btnSubmit.addEventListener('click',function(e){
		if(!(require('/utils/Debounce').debounce(e.source))){return;}
		
		var mpin = '';
		var mpin_confirm = '';
		
		for(var i=0; i<6; i++)
		{
			mpin = mpin + _obj.txtMPIN[i].value;
			mpin_confirm = mpin_confirm + _obj.txtMPINConfirm[i].value;
		}
		
		if(mpin.length<6)   //check if mpin is less than 6 digit otherwise gives alerts
  		{
  			require('/utils/AlertDialog').showAlert('',L('validateMPIN'),[L('btn_ok')]).show();
			return;
  		}
  		else if(mpin !== mpin_confirm)
  		{
  			require('/utils/AlertDialog').showAlert('',L('confirmMPIN'),[L('btn_ok')]).show();
  			
  			for(var i=0; i<6; i++)
			{
				_obj.txtMPIN[i].value = '';
				_obj.txtMPINConfirm[i].value = '';
			}
  			
			return;
  		}
  		else
		{
			var cipher = 'rijndael-128';
			var mode = 'ecb';
			var iv = null;
			var base64 = require('/lib/Crypt/Base64');
			
			require('/lib/Crypt/AES').Crypt(null,null,null, key, cipher, mode);
			var ciphertext = require('/lib/Crypt/AES').Encrypt(true, mpin, iv, key, cipher, mode);
			ciphertext = base64.encode(ciphertext);
			
			activityIndicator.showIndicator();
			
			var xhr = require('/utils/XHR');
			var base64 = require('/lib/Crypt/Base64');
			
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"CREATEMPIN",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"mPin":"'+ciphertext+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});

			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.responseFlag === "S")
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast(e.result.message);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast(e.result.message);
					}
					
					Ti.App.Properties.setString('mPIN','1');
					destroy_mpin();
				}
				/*else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_mpin();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}*/
			}

			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xmlhttp = null;
			}
		}	
	});
	
	function destroy_mpin()
	{
		try{
			
			require('/utils/Console').info('############## Remove create mpin start ##############');
			
			_obj.winCreateMPIN.close();
			require('/utils/RemoveViews').removeViews(_obj.winCreateMPIN);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_mpin',destroy_mpin);
			require('/utils/Console').info('############## Remove create mpin end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_mpin', destroy_mpin);

};// CreateMPINModal()