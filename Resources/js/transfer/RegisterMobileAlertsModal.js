exports.RegisterMobileAlertsModal = function()
{
	require('/lib/analytics').GATrackScreen('Mobile Alerts Registration');
	
	var _obj = {
		style : require('/styles/transfer/RegisterMobileAlerts').RegisterMobileAlerts,
		winRegisterMobileAlertsMobileAlerts : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		imgMobileAlerts : null,
		lblMobileAlertsTxt : null,
		mobileAlertsView : null,
		borderViewM1 : null,
		lblCountry : null,
		imgCountry : null,
		countryView : null,
		borderViewM2 : null,
		lblMobileNo : null,
		txtMobile : null,
		mobileView : null,
		borderViewM3 : null,
		borderViewM4 : null,
		imgReceiverAlerts : null,
		lblReceiverAlertsTxt : null,
		receiverAlertsView : null,
		borderViewM5 : null,
		imgTerms : null,
		lblTerm1 : null,
		lblTerm2 : null,
		termsView : null,
		btnSubmit : null,
		
		countryISD : null,
		countryOptions : null,
		mobileAlert : null,
		receiverAlert : null,
		terms : null, 
	};
	
	_obj.mobileAlert = 'No';
	_obj.receiverAlert = 'No';
	_obj.terms = 'No';
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var countryName = Ti.App.Properties.getString('destinationCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	_obj.winRegisterMobileAlertsMobileAlerts = Ti.UI.createWindow(_obj.style.winRegisterMobileAlertsMobileAlerts);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winRegisterMobileAlertsMobileAlerts);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Registration for Mobile Alerts';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mobileAlertsView = Ti.UI.createView(_obj.style.mobileAlertsView);
	_obj.imgMobileAlerts = Ti.UI.createImageView(_obj.style.imgMobileAlerts);
	_obj.lblMobileAlertsTxt = Ti.UI.createLabel(_obj.style.lblMobileAlertsTxt);
	_obj.lblMobileAlertsTxt.text =  'I would like to receive Mobile Alerts';
	_obj.borderViewM1 = Ti.UI.createView(_obj.style.borderFullView);
	
	_obj.mobileAlertsView.addEventListener('click',function(e){
		if(_obj.imgMobileAlerts.image === '/images/checkbox_unsel.png')
		{
			_obj.imgMobileAlerts.image = '/images/checkbox_sel.png';
			_obj.mobileAlert = 'Yes';
		}
		else
		{
			_obj.imgMobileAlerts.image = '/images/checkbox_unsel.png';
			_obj.mobileAlert = 'No';
		}
	});
	
	_obj.countryView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblCountry.text = 'Select Country*';
	_obj.imgCountry = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewM2 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneMobileNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobileNo.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobileNo = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblMobileNo.text = 'ISD';
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = 'Mobile Number*';
	_obj.txtMobile.maxLength = 15;
	_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobileNo];
	_obj.borderViewM3 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.borderViewM4 = Ti.UI.createView(_obj.style.borderFullView);
	_obj.borderViewM4.top = 30;
	
	_obj.receiverAlertsView = Ti.UI.createView(_obj.style.mobileAlertsView);
	_obj.imgReceiverAlerts = Ti.UI.createImageView(_obj.style.imgMobileAlerts);
	_obj.lblReceiverAlertsTxt = Ti.UI.createLabel(_obj.style.lblMobileAlertsTxt);
	_obj.lblReceiverAlertsTxt.text =  'I would like my Receiver/Receivers also to receive mobile alerts';
	_obj.borderViewM5 = Ti.UI.createView(_obj.style.borderFullView);
	
	_obj.receiverAlertsView.addEventListener('click',function(e){
		if(_obj.imgReceiverAlerts.image === '/images/checkbox_unsel.png')
		{
			_obj.imgReceiverAlerts.image = '/images/checkbox_sel.png';
			_obj.receiverAlert = 'Yes';
		}
		else
		{
			_obj.imgReceiverAlerts.image = '/images/checkbox_unsel.png';
			_obj.receiverAlert = 'No';
		}
	});
	
	_obj.termsView = Ti.UI.createView(_obj.style.termsView);
	_obj.imgTerms = Ti.UI.createImageView(_obj.style.imgTerms);
	_obj.lblTerm1 = Ti.UI.createLabel(_obj.style.lblTerm1);
	_obj.lblTerm1.text = 'I agree to the ';
	_obj.lblTerm2 = Ti.UI.createLabel(_obj.style.lblTerm2);
	_obj.lblTerm2.text = 'Terms & Conditions';
	
	_obj.termsView.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Yes';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'No';
		}
	});
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		if(_obj.lblCountry.text === 'Select Country*')
		{
			require('/utils/AlertDialog').showAlert('','Please select your Country',[L('btn_ok')]).show();
    		_obj.lblCountry.text = 'Select Country*';
			return;	
		}
		else
		{
			// Do nothing
		}
		
		if(_obj.txtMobile.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(_obj.txtMobile.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Mobile Number must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(_obj.txtMobile.value.length < 7)
		{
			require('/utils/AlertDialog').showAlert('','Mobile Number cannot be less than 7 digits',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(_obj.txtMobile.value.length > 15)
		{
			require('/utils/AlertDialog').showAlert('','Mobile Number cannot be greater than 15 digits',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else
		{
			
		}
		
		if(_obj.mobileAlert === 'No' && _obj.receiverAlert === 'No')
		{
			require('/utils/AlertDialog').showAlert('','Either Self Alert or Receiver Alert should be checked',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		if(_obj.terms === 'No')
		{
			require('/utils/AlertDialog').showAlert('','Please accept the terms and conditions',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		var alerts = '';
		
		if(_obj.mobileAlert === 'Yes')
		{
			alerts = alerts + 'S';
		}
		
		if(_obj.receiverAlert === 'No')
		{
			alerts = alerts + 'B';
		}
		
		activityIndicator.showIndicator();
			
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"SMSREGISTRATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"mobileNo":"'+_obj.lblMobileNo.text+''+_obj.txtMobile.value+'",'+
				'"smsFlag":"'+alerts+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				destroy_registermobilealerts();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_registermobilealerts();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
		
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.mobileAlertsView.add(_obj.imgMobileAlerts);
	_obj.mobileAlertsView.add(_obj.lblMobileAlertsTxt);
	_obj.mainView.add(_obj.mobileAlertsView);
	_obj.mainView.add(_obj.borderViewM1);
	_obj.countryView.add(_obj.lblCountry);
	_obj.countryView.add(_obj.imgCountry);
	_obj.mainView.add(_obj.countryView);
	_obj.mainView.add(_obj.borderViewM2);
	_obj.mobileView.add(_obj.lblMobileNo);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.mainView.add(_obj.mobileView);
	_obj.mainView.add(_obj.borderViewM3);
	_obj.mainView.add(_obj.borderViewM4);
	_obj.receiverAlertsView.add(_obj.imgReceiverAlerts);
	_obj.receiverAlertsView.add(_obj.lblReceiverAlertsTxt);
	_obj.mainView.add(_obj.receiverAlertsView);
	_obj.mainView.add(_obj.borderViewM5);
	_obj.termsView.add(_obj.imgTerms);
	_obj.termsView.add(_obj.lblTerm1);
	_obj.termsView.add(_obj.lblTerm2);
	_obj.mainView.add(_obj.termsView);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winRegisterMobileAlertsMobileAlerts.add(_obj.globalView);
	_obj.winRegisterMobileAlertsMobileAlerts.open();
	
	function country()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		var countryISD = [];
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCORRIDORDTL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"origCtyCurCode":"",'+
				'"destCtyCurCode":"",'+
				'"corridorRequestType":"3"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				for(var i=0;i<e.result.corridorData[0].origCtyCurData.length;i++)
				{
					if(e.result.corridorData[0].origCtyCurData[i].isActiveRegncountry === 'Y')
					{
						var splitArr = e.result.corridorData[0].origCtyCurData[i].origCtyCurName.split('~');
						countryOptions.push(splitArr[0]);
						countryISD.push(e.result.corridorData[0].origCtyCurData[i].isdnCode);
					}
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCountry.text = optionDialogCountry.options[evt.index];
						_obj.lblMobileNo.text = countryISD[evt.index];
						_obj.lblMobileNo.color = '#000';
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblCountry.text = 'Select Country*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_registermobilealerts();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.countryView.addEventListener('click',function(){
		country();
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_registermobilealerts();
	});
	
	_obj.winRegisterMobileAlertsMobileAlerts.addEventListener('androidback', function(){
		destroy_registermobilealerts();
	});
	
	function destroy_registermobilealerts()
	{
		try{
			
			require('/utils/Console').info('############## Remove mobile alerts registration start ##############');
			
			_obj.winRegisterMobileAlertsMobileAlerts.close();
			require('/utils/RemoveViews').removeViews(_obj.winRegisterMobileAlertsMobileAlerts);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_registermobilealerts',destroy_registermobilealerts);
			require('/utils/Console').info('############## Remove mobile alerts registration end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_registermobilealerts', destroy_registermobilealerts);
}; // RegisterMobileAlertsModal()