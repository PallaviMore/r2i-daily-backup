exports.PreConfirmationModal = function(conf)
{
	var getState = null;
	var getCountry = null;
	require('/lib/analytics').GATrackScreen('Transfer Money Pre-Confirmation');
	
	var _obj = {
		style : require('/styles/transfer/PreConfirmation').PreConfirmation,
		winPreConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblAddressHeader : null,
		lblAddress : null,
		lblDateHeader : null,
		lblDate : null,
		lblLimitHeader : null,
		lblLimit : null,
		paymentView : null,
		tblPaymentDetails : null,
		lblAmountConvertedHeader : null,
		lblAmountConverted : null,
		convertedView : null,
		tblConverted : null,
		lblRecipientAmountHeader : null,
		lblRecipientAmount : null,
		recipientAmountView : null,
		lblSummary : null,
		lblSummaryReceipt : null,
		summaryView : null,
		imgIndicator : null,
		indicatorView : null,
		tblSummary : null,
		imgInfo : null,
		lblInfo : null,
		infoTextView : null,
		imgTerms : null,
		lblTerms : null,
		lblTerms1 : null,
		termsView : null,

		terms : null,
		isFRT : 'Y',
		
		lblLimit1 : null,
		lblLimit2 : null,
		lblLimit3 : null,
		lblTransDetails : null
		};
	
	
/*	// FRT
	var paymodeData = Ti.App.Properties.getString('paymodeData');
	Ti.API.info("------>",paymodeData);
	for(var f=0; f<paymodeData.length; f++)
	{
		Ti.API.info("++***",JSON.stringify(paymodeData));
		Ti.API.info("++++++",JSON.stringify(paymodeData[0].paymodeCode));
		Ti.API.info("++++--",JSON.stringify(paymodeData[1].fxRateIndicativeOrGuaranteed));
		if(paymodeData[f].paymodeCode === conf['paymodeshort'])
		{
			Ti.API.info("******",paymodeData);
			_obj.isFRT = paymodeData[f].isFRTPaymode;
			Ti.API.info("/////",_obj.isFRT);
		}
	Ti.API.info("-----",_obj.isFRT);
	}
	Ti.API.info("=====",_obj.isFRT);*/
	
	// FRT
	var paymodeData = Ti.App.Properties.getString('paymodeData');
	
	for(var f=0; f<paymodeData.length; f++)
	{
		if(paymodeData[f].paymodeCode === conf['paymodeshort'])
		{
			_obj.isFRT = paymodeData[f].isFRTPaymode;
		}
	}
	
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winPreConfirmation = Ti.UI.createWindow(_obj.style.winPreConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winPreConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Pre-Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.lblAddressHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
	_obj.lblAddressHeader.text = 'Our Correspondence Address';
	
	_obj.lblAddress = Ti.UI.createLabel(_obj.style.lblAddress);
	_obj.lblAddress.text = 'TimesofMoney Ltd.\nLevel-4, B Wing Reliable Tech Park,\nAiroli, New Mumbai-400 708, India';
	
	 //cmn 9 
	_obj.lblAddress1 = Ti.UI.createLabel(_obj.style.lblAddress);
     _obj.lblAddress1.text ='TimesofMoney, Inc.\n830, Stewart Dr # 256 Sunnyvale, \nCA 94085';



	_obj.lblDateHeader = Ti.UI.createLabel(_obj.style.lblDateHeader);
	_obj.lblDateHeader.text = 'Today\'s Date & Time';
	
	_obj.lblDate = Ti.UI.createLabel(_obj.style.lblDate);
	_obj.lblDate.text = Date();
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	
/*	 function getProfileData()

	{

	activityIndicator.showIndicator();


	var xhr = require('/utils/XHR');

	xhr.call({

	url : TiGlobals.appURLTOML,

	get : '',

	post : '{' +

	'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+

	'"requestName":"GETUSERDETAILS",'+

	'"partnerId":"'+TiGlobals.partnerId+'",'+

	'"channelId":"'+TiGlobals.channelId+'",'+

	'"ipAddress":"'+TiGlobals.ipAddress+'",'+

	'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 

	'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+

	'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+

	'}',

	success : xhrSuccess,

	error : xhrError,

	contentType : 'application/json',

	timeout : TiGlobals.timer

	});


	function xhrSuccess(e) {

	activityIndicator.hideIndicator();

	if(e.result.responseFlag === "S")

	{

	getState= e.result.state;

	Ti.API.info("In State is--->"+getState);

	getCountry = e.result.country;

	Ti.API.info("In country is--->"+getCountry);

	}


	     }

	function xhrError(e) {

	activityIndicator.hideIndicator();

	require('/utils/Network').Network();

	xhr = null;

	}
	}

	getProfileData();

	Ti.API.info("out State is--->" + getState);

	Ti.API.info("Out country is--->" + getCountry);


	//if(getState == 'Colorado' || getState =='Washington' && getCountry =='US'){
	if(getCountry =='US' && getState =='Washington'){

	_obj.mainView.add(_obj.lblAddress1);

	}

	else

	{

	_obj.mainView.add(_obj.lblAddress);

	}*/
	

/*  _obj.mainView.add(_obj.lblAddressHeader); //1
	_obj.mainView.add(_obj.lblAddress);//2
	_obj.mainView.add(_obj.lblDateHeader);//3
	_obj.mainView.add(_obj.lblDate);//4*/
	_obj.globalView.add(_obj.mainView);
	_obj.winPreConfirmation.add(_obj.globalView);
	_obj.winPreConfirmation.open();
	
	
	// Check pre-confirmation
	
	function preconf()
	{
		activityIndicator.showIndicator();
	
		
		var xhr = require('/utils/XHR');
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"PRECONFTXN",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"originatingCountry":"'+origSplit[0]+'",'+ 
					'"originatingCurrency":"'+origSplit[1]+'",'+
					'"destinationCountry":"'+destSplit[0]+'",'+
					'"destinationCurrency":"'+destSplit[1]+'",'+
					'"paymodeCode":"'+conf['paymode']+'",'+
				    '"receiverNickName":"'+conf['receiverNickName']+'",'+
				    '"receiverOwnerId":"'+conf['receiverOwnerId']+'",'+
				    '"receiverLoginid":"'+conf['receiverLoginid']+'",'+
				    '"receiverParentChildflag":"'+conf['receiverParentChildflag']+'",'+
				    '"amount":"'+conf['amount']+'",'+
				    '"accountId":"'+conf['accountId']+'",'+
				    '"bankName":"'+conf['bankName']+'",'+
				    '"bankBranch":"'+conf['bankBranch']+'",'+
				    '"accountNo":"'+conf['accountNo']+'",'+
				    '"purpose":"'+conf['purpose']+'",'+
				    '"personalMessage":"",'+
				    '"promoCode":"'+conf['promoCode']+'",'+
				    '"mobileAlerts":"'+conf['mobileAlerts']+'",'+
				    '"transactionInsurance":"'+conf['transactionInsurance']+'",'+
				    '"fxVoucherRedeem":"'+conf['fxVoucherRedeem']+'",'+
				    '"agentSpecialCode":"'+conf['agentSpecialCode']+'",'+
				    '"morRefferalAmnt":"'+conf['morRefferalAmnt']+'",'+
				    '"programType":"'+conf['programType']+'",'+ 
				    '"txnRefId":"'+conf['txnRefId']+'",'+
				    '"contactDate":"'+conf['contactDate']+'",'+
				    '"timeFrom":"'+conf['timeFrom']+'",'+
				    '"timeTo":"'+conf['timeTo']+'",'+
				    '"timeFrom1":"'+conf['timeFrom1']+'",'+
				    '"timeTo1":"'+conf['timeTo1']+'",'+
				    '"voucherCode":"'+conf['voucherCode']+'",'+
				    '"voucherQty":"'+conf['voucherQty']+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				
				try{
				require('/utils/Console').info('Result ======== ' + e.result);
				
				activityIndicator.hideIndicator();
				
				if(e.result.message === "Please select KYC Contact Details")
				{
					require('/js/kyc/KYCCallingModal').KYCCallingModal();
					
					setTimeout(function(){
						destroy_preconfirmation();
					},500);
					return;
				}
			
				
				if(e.result.message === 'You have exceeded the compliance limit')
				{
					_obj.lblLimitHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
					_obj.lblLimitHeader.text = e.result.message;
					
					_obj.lblLimit1 = Ti.UI.createLabel(_obj.style.lblAddress);
					_obj.lblLimit1.text = 'You have reached your prescribed transaction limit. In order to send higher amounts, please fill in our KYC Form.';
					
					_obj.lblLimit2 = Ti.UI.createLabel(_obj.style.lblAddress);
					_obj.lblLimit2.text = "Remit2India allows you to send a minimum of AUD50 per transaction and a maximum of AUD2000 per transaction. In case you want to send amounts more than the specified limits. Kindly transfer funds through other paymodes.";
					
					_obj.lblLimit3 = Ti.UI.createLabel(_obj.style.lblAddress);
					_obj.lblLimit3.text ="Remit2India allows you to send a minimum of AUD50 per transaction and a maximum of AUD2000 per transaction. Maximum of AUD4000 can be sent in a day. In case you want to send amounts more than the specified limits. Kindly transfer funds through other pay modes.";
						
					_obj.lblTransDetails = Ti.UI.createLabel(_obj.style.lblAddressHeader);
					_obj.lblTransDetails.text = 'Transaction Details';
					
					_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
					_obj.btnSubmit.title = 'KYC Form';
	
					
					//_obj.mainView.add(_obj.lblLimit);
					try{
					if((conf['paymode'] === "POLIFRT") || (conf['paymode'] === 'ACHGFXI')){
						if(conf['amount'] > 2000){
						
							_obj.mainView.add(_obj.lblTransDetails);
							_obj.mainView.add(_obj.lblLimit2);
							
						}
						else{
							_obj.mainView.add(_obj.lblTransDetails);
							_obj.mainView.add(_obj.lblLimit3);
							
						}
					}
					
					if((conf['paymode'] !== "POLIFRT") && (conf['paymode'] !== 'ACHGFXI'))
					{
						_obj.mainView.add(_obj.lblAddressHeader); //1
						_obj.mainView.add(_obj.lblAddress);//2
						_obj.mainView.add(_obj.lblDateHeader);//3
						_obj.mainView.add(_obj.lblDate);//4
						_obj.mainView.add(_obj.lblLimitHeader);
						_obj.mainView.add(_obj.lblLimit1);
						_obj.mainView.add(_obj.btnSubmit);	
					    
						_obj.btnSubmit.addEventListener('click',function(e){
							require('/js/transfer/LimitEnhancementModal').LimitEnhancementModal(conf);
							
							setTimeout(function(){
								destroy_preconfirmation();
							},500);
						});
					}
					}catch(e){
						Ti.API.info(e);
					}
					
					return;
				}
				
				//try{
				if(e.result.responseFlag === 'S')
				{
					_obj.mainView.add(_obj.lblAddressHeader); //1
					_obj.mainView.add(_obj.lblAddress);//2
					_obj.mainView.add(_obj.lblDateHeader);//3
					_obj.mainView.add(_obj.lblDate);//4
					_obj.paymentView = Ti.UI.createView(_obj.style.paymentView);
					_obj.lblPaymentHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
					_obj.lblPaymentHeader.text = 'Payment Details';
					
					_obj.actionView = Ti.UI.createView(_obj.style.actionView);
					_obj.lblEdit = Ti.UI.createLabel(_obj.style.lblAction);
					_obj.lblEdit.text = 'EDIT';
					_obj.lblEdit.sel = 'edit';
					_obj.lblSeparator1 = Ti.UI.createLabel(_obj.style.lblAction);
					_obj.lblSeparator1.text = '-';
					Ti.API.info('Paymode code for bug.695 is:'+conf['paymode']);  //by Sanjivani on 26 Oct 16
					Ti.API.info('Paymode code for bug.695 is:'+conf['paymodeCode']);
					//if(conf['paymodeCode'] !== 'CCARD')//By BCM
					if(conf['paymode'] !== 'CCARD')// by Sanjivani on 26 Oct 16
					{
						_obj.lblSave = Ti.UI.createLabel(_obj.style.lblAction);
						_obj.lblSave.text = 'SAVE';
						_obj.lblSave.sel = 'save';
						_obj.lblSave.color = '#FFF';
						_obj.lblSeparator2 = Ti.UI.createLabel(_obj.style.lblAction);
						_obj.lblSeparator2.text = '-';
					}
					_obj.lblCancel = Ti.UI.createLabel(_obj.style.lblAction);
					_obj.lblCancel.text = 'CANCEL';	
					_obj.lblCancel.sel = 'cancel';
					
					_obj.tblPaymentDetails = Ti.UI.createTableView(_obj.style.tableView);
					_obj.tblPaymentDetails.height = 0;
					
					for(var i=0; i<=2; i++)
					{
						var rowPD = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : 'transparent',
							className : 'bank_account_details'
						});
						
						if(TiGlobals.osname !== 'android')
						{
							rowPD.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblKeyPD = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblValuePD = Ti.UI.createLabel({
							top : 30,
							height : 20,
							left : 20,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						switch(i)
						{
							case 0:
								if(e.result.hasOwnProperty('transferAmount'))
								{
									lblKeyPD.text = 'Transfer Amount';
									lblValuePD.text = origSplit[1] + ' ' + e.result.transferAmount;
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
								}
							break;
							
							case 1:
								if(e.result.hasOwnProperty('programFee') && e.result.programFee !== '')
								{
									var voucher = '';
									
									if(e.result.hasOwnProperty('benefitsData') && e.result.benefitsData !== '')
									{ 
										if(e.result.benefitsData[0].hasOwnProperty('ep'))
										{
											voucher = '(FXVoucher)';
										}
										else if(e.result.benefitsData[0].hasOwnProperty('fw'))
										{
											voucher = '(Freeway Voucher)';
										}  
									}
									
									lblKeyPD.text = 'Program Fee ' + voucher;
									lblValuePD.text = e.result.programFee;
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
								}
							break;
							
							case 2:
							    if(e.result.hasOwnProperty('netApplicableFee'))
								{
									if(e.result.netApplicableFee >= 0.0)
									{
									lblKeyPD.text = 'Net Applicable Fee';
									lblValuePD.text = e.result.netApplicableFee;
									
									rowPD.add(lblKeyPD);
									rowPD.add(lblValuePD);
									_obj.tblPaymentDetails.appendRow(rowPD);
									_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
									}
								}
							break;
						}
					}
					
					_obj.convertedView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblAmountConvertedHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblAmountConvertedHeader.text = 'Amount to be converted';
					_obj.lblAmountConverted = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblAmountConverted.text = origSplit[1] + ' ' + e.result.amountToBeConverted;
					
					_obj.tblConverted = Ti.UI.createTableView(_obj.style.tableView);
					_obj.tblConverted.height = 0;
					
					for(var i=0; i<=3; i++)
					{
						var rowCon = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : 'transparent',
							className : 'bank_account_details'
						});
						
						if(TiGlobals.osname !== 'android')
						{
							rowCon.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblKeyCon = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblValueCon = Ti.UI.createLabel({
							top : 30,
							height : 20,
							left : 20,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						switch(i)
						{
							case 0:
								if(e.result.hasOwnProperty('exchangeRate'))
								{
									lblKeyCon.text = 'Exchange Rate';
									lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.exchangeRate;
									
									rowCon.add(lblKeyCon);
									rowCon.add(lblValueCon);
									_obj.tblConverted.appendRow(rowCon);
									_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
								}
								
							break;
							
							case 1:
								if(e.result.hasOwnProperty('netExchangeRate'))
								{
									lblKeyCon.text = 'Exchange Rate with Benefit';
									lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.netExchangeRate;
									
									rowCon.add(lblKeyCon);
									rowCon.add(lblValueCon);
									_obj.tblConverted.appendRow(rowCon);
									_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
								}
							break;
							
							case 2:
								if(e.result.hasOwnProperty('convertedAmount'))
								{
									lblKeyCon.text = 'Converted Amount';
									lblValueCon.text = destSplit[1] + ' ' + e.result.convertedAmount;
									
									rowCon.add(lblKeyCon);
									rowCon.add(lblValueCon);
									_obj.tblConverted.appendRow(rowCon);
									_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
								}	
							break;
							
							case 3:
								if(e.result.hasOwnProperty('serviceTax'))
								{
									if((origSplit[0] === 'US' && ((conf['paymode'] === 'ACHGFXFRT') || (conf['paymode'] === 'WIREFRT'))) || (origSplit[0] === 'UK' && (conf['paymode'] === 'DEBFRT')))
									{
										if(_obj.isFRT === 'Y')
										{
											lblKeyCon.text = 'Service Tax';
											lblValueCon.text = '(-) ' + destSplit[1] + ' ' + e.result.serviceTax;
											
											rowCon.add(lblKeyCon);
											rowCon.add(lblValueCon);
											_obj.tblConverted.appendRow(rowCon);
											_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
										}
									}
									
									
									if(origSplit[0] === 'AUS' && ((conf['paymode'] === 'POLIFRT') || (conf['paymode'] === 'POLIFR')))
									{
										if(_obj.isFRT === 'Y')
										{
											lblKeyCon.text = 'Service Tax';
											lblValueCon.text = '(-) ' + destSplit[1] + ' ' + e.result.serviceTax;
											
											rowCon.add(lblKeyCon);
											rowCon.add(lblValueCon);
											_obj.tblConverted.appendRow(rowCon);
											_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
										}
									}
									
									
								}
							break;
						}
						
					}
					
					_obj.SwachhBharatView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblSwachhBharatHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblSwachhBharatHeader.text = 'Swachh Bharat Cess';
					_obj.lblSwachhBharatAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblSwachhBharatAmount.text = destSplit[1] + ' ' + e.result.sbcTax;
					
					_obj.recipientAmountView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblRecipientAmountHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblRecipientAmountHeader.text = 'Amount to Recipient';
					_obj.lblRecipientAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblRecipientAmount.text = destSplit[1] + ' ' + e.result.indicativeAmountRecipient;
					
					_obj.recipientAmountToView = Ti.UI.createView(_obj.style.convertedView);
					_obj.lblRecipientAmountToHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
					_obj.lblRecipientAmountToHeader.text = 'Indicative Destination Amount';
					_obj.lblRecipientAmountTo = Ti.UI.createLabel(_obj.style.lblAmountConverted);
					_obj.lblRecipientAmountTo.text = destSplit[1] + ' ' + e.result.convertedAmount;
					
					/*_obj.summaryView = Ti.UI.createView(_obj.style.summaryView);
					_obj.lblSummary = Ti.UI.createLabel(_obj.style.lblSummary);
					_obj.lblSummary.text = 'Summary of pre-confirmation';
					_obj.lblSummaryReceipt = Ti.UI.createLabel(_obj.style.lblSummaryReceipt);
					_obj.lblSummaryReceipt.text = 'Not a Receipt';*/
					
					//_obj.indicatorView = Ti.UI.createView(_obj.style.indicatorView);
					//_obj.imgIndicator = Ti.UI.createImageView(_obj.style.imgIndicator);
					
					_obj.tblSummary = Ti.UI.createTableView(_obj.style.tableView);
					_obj.tblSummary.height = 0;
					_obj.tblSummary.separatorColor = '#595959';
					_obj.tblSummary.backgroundColor = '#303030';
					
					for(var i=0; i<=4; i++)
					{
						var rowSummary = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : '#303030',
							className : 'bank_account_details'
						});
						
						if(TiGlobals.osname !== 'android')
						{
							rowSummary.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblKeySummary = Ti.UI.createLabel({
							top : 10,
							left : 20,
							height : 15,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblValueSummary = Ti.UI.createLabel({
							top : 30,
							height : 20,
							left : 20,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						switch(i)
						{
							case 0:
								if(e.result.hasOwnProperty('receiverData'))
								{
									lblKeySummary.text = 'Remittance transfer request received for';
									lblValueSummary.text = e.result.receiverData[0].firstName +' '+ e.result.receiverData[0].lastname +' '+ e.result.receiverData[0].micr;
									
									rowSummary.add(lblKeySummary);
									rowSummary.add(lblValueSummary);
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
								}
							break;
							
							case 1:
								if(e.result.hasOwnProperty('receiverData'))
								{
									lblKeySummary.text = 'Delivery Mode';
									lblValueSummary.text = e.result.receiverData[0].deliveryModeCodeDesc;
									
									rowSummary.add(lblKeySummary);
									rowSummary.add(lblValueSummary);
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
								}
							break;
							
							case 2:
								if(e.result.hasOwnProperty('purpose'))
							  {
								var purposeSplit = e.result.purpose.split('~');

                                Ti.API.info("Response is:------>",purposeSplit[1]);
								//lblKeySummary.text = 'Purpose of Remittance';
								//lblValueSummary.text = purposeSplit[1];	
								Ti.API.info("Purpose without splitting  is******:------>",e.result.purpose);
								Ti.API.info("Purpose with splitting  is****:------>",purposeSplit[0]+' '+purposeSplit[1]);
                                Ti.API.info("purposeSplit[1] is*******:------>",purposeSplit[1]);
								 
                                 lblKeySummary.text = 'Purpose of Remittance';

									//lblValueSummary.text= purposeSplit[0];
                                                                        lblValueSummary.text=e.result.purpose;
									Ti.API.info("Purpose on confirmation is:" + lblValueSummary.text);
								
								rowSummary.add(lblKeySummary);

								rowSummary.add(lblValueSummary);

								_obj.tblSummary.appendRow(rowSummary);

								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
							case 3:
								if(e.result.hasOwnProperty('paymodeData'))
								{
									lblKeySummary.text = 'Sending option selected';
									if(e.result.paymodeData[0].paymodeDesc === 'Wire FRT'){
										e.result.paymodeData[0].paymodeDesc = 'Wire Transfer';
										Ti.API.info("PAYMODE:---",e.result.paymodeData[0].paymodeDesc);
									lblValueSummary.text = e.result.paymodeData[0].paymodeDesc;
									}
									else{
										lblValueSummary.text = e.result.paymodeData[0].paymodeDesc;
									}
									
									
									rowSummary.add(lblKeySummary);
									rowSummary.add(lblValueSummary);
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
								}
							break;
							
							case 4:
								if(e.result.hasOwnProperty('deliveryDate') && (origSplit[0] === 'US'))
								{
									lblKeySummary.text = 'Date Available: ' + e.result.deliveryDate;
									lblValueSummary.text = '(The money may be available sooner to the receiver)';
									lblValueSummary.font = TiFonts.FontStyle('lblNormal12');
									
									rowSummary.add(lblKeySummary);
									rowSummary.add(lblValueSummary);
									_obj.tblSummary.appendRow(rowSummary);
									_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
								}
							break;
						}
						
					}
					
					
					if(origSplit[0] === 'US')
					{
						_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
						_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
						_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
						
						if(conf['paymodeshort'] === "WIRE")
						{
							_obj.lblInfo.text = "Recipient may receive less due to fees charged by recipient's bank and foreign taxes. Service Tax is applicable as per the Service Tax (Amendment) rules, 2012 of the Government of India & is recoverable from any foreign currency transfer into India. The Service Tax shall be computed and deducted from the converted amount based on the final exchange rate as indicated in the final receipt. We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds. You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, TOML may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.";	
						}
						else if(conf['paymodeshort'] === "ACH")
						{
							_obj.lblInfo.text = "Recipient may receive less due to fees charged by recipient's bank and foreign taxes. Service Tax is applicable as per the Service Tax (Amendment) rules, 2012 of the Government of India & is recoverable from any foreign currency transfer into India. The Service Tax shall be computed and deducted from the converted amount based on the final exchange rate as indicated in the final receipt. We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds.";
						}
					}
					
					if(origSplit[0] === 'UK')
					{
						_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
						_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
						_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
						
						_obj.lblInfo.text = "Our Nostro account details have changed from Axis Bank to Karnataka Bank. Kindly note the same before initiating the fund transfers.";
					}
					
					if(countryName[1] === 'EURO' || countryName[1] === 'Euro' || countryName[1] === 'euro')
					{
						_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
						_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
						_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
						
						_obj.lblInfo.text = "Our Correspondent Bank details have changed. Kindly note the same before initiating the fund transfers.";
					}
					
					_obj.btnServiceTax = Ti.UI.createButton(_obj.style.btnSubmit);
					_obj.btnServiceTax.backgroundColor = '#000';
					_obj.btnServiceTax.title = 'Service Tax';
					_obj.btnServiceTax.bottom = 20;
					
					_obj.btnServiceTax.addEventListener('click',function(e){
						require('/js/transfer/ServiceTaxModal').ServiceTaxModal();
					});
					
					_obj.termsView = Ti.UI.createView(_obj.style.termsView);
					_obj.imgTerms = Ti.UI.createImageView(_obj.style.imgTerms);
					_obj.lblTerms = Ti.UI.createLabel(_obj.style.lblTerms);
					_obj.lblTerms1 = Ti.UI.createLabel(_obj.style.lblTerms1);
					
					if(origSplit[0] === 'US')
					{
						_obj.lblTerms.text = 'I agree to the ';
						_obj.lblTerms1.text = 'Terms & Conditions';
					}
					else if (origSplit[0] === 'UK')
					{
						_obj.lblTerms.text = 'I hereby declare and confirm that the information provided herein above, including but not limited to my Bank account and/or transaction details are correct.';
					}
					
					_obj.terms = 'N';
					_obj.imgTerms.addEventListener('click',function(e){
						if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
						{
							_obj.imgTerms.image = '/images/checkbox_sel.png';
							_obj.terms = 'Y';
						}
						else
						{
							_obj.imgTerms.image = '/images/checkbox_unsel.png';
							_obj.terms = 'N';
						}
					});
					
					_obj.lblTerms.addEventListener('click',function(e){
						if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
						{
							_obj.imgTerms.image = '/images/checkbox_sel.png';
							_obj.terms = 'Y';
						}
						else
						{
							_obj.imgTerms.image = '/images/checkbox_unsel.png';
							_obj.terms = 'N';
						}
					});
					
					_obj.lblTerms1.addEventListener('click',function(e){
						require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','txn','');
					});
					
					_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
					_obj.btnSubmit.title = 'CONFIRM';
					
					_obj.paymentView.add(_obj.lblPaymentHeader);
					_obj.actionView.add(_obj.lblEdit);
					_obj.actionView.add(_obj.lblSeparator1);
					if(Ti.Platform.osname === 'android'){
					if(conf['paymode'] !== 'CCARD')
					{
						_obj.actionView.add(_obj.lblSave);
						_obj.actionView.add(_obj.lblSeparator2);
					}
                                         }
                                         else{
                                             if(conf['paymodeCode'] !== 'CCARD')
					{
						_obj.actionView.add(_obj.lblSave);
						_obj.actionView.add(_obj.lblSeparator2);
					}
                                        }

					_obj.actionView.add(_obj.lblCancel);
					_obj.paymentView.add(_obj.actionView);
					
					_obj.actionView.addEventListener('click',function(e){
						if(e.source.sel === 'edit')
						{
							require('/js/transfer/EditTransactionModal').EditTransactionModal(conf);
							destroy_preconfirmation();
						}
						
						if(e.source.sel === 'save')
						{
							activityIndicator.showIndicator();
							
							var xhr = require('/utils/XHR');
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"SAVEINCOMPLETETRANSACTION",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"originatingCountry":"'+origSplit[0]+'",'+ 
									'"originatingCurrency":"'+origSplit[1]+'",'+
									'"destinationCountry":"'+destSplit[0]+'",'+
									'"destinationCurrency":"'+destSplit[1]+'",'+
									'"paymodeCode":"'+conf['paymode']+'",'+
								    '"receiverNickName":"'+conf['receiverNickName']+'",'+
								    '"receiverOwnerId":"'+conf['receiverOwnerId']+'",'+
								    '"receiverLoginid":"'+conf['receiverLoginid']+'",'+
								    '"amount":"'+conf['amount']+'",'+
								    '"accountId":"'+conf['accountId']+'",'+
								    '"bankName":"'+conf['bankName']+'",'+
								    '"bankBranch":"'+conf['bankBranch']+'",'+
								    '"bankAccNo":"'+conf['accountNo']+'",'+
								    '"purpose":"'+conf['purpose']+'",'+
								    '"checkNumber":"",'+
								    '"checkBankName":"",'+
								    '"checkBankBranch":"",'+
								    '"clearingHouseId":"",'+
								    '"clearingHouseAddress":""'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
					
							function xhrSuccess(e) {
								activityIndicator.hideIndicator();
								if(e.result.responseFlag === "S")
								{
									if(TiGlobals.osname === 'android')
									{
										require('/utils/AlertDialog').toast(e.result.message);
									}
									else
									{
										require('/utils/AlertDialog').iOSToast(e.result.message);
									}
									
									require('/utils/RemoveViews').removeAllScrollableViews();
									destroy_preconfirmation();
								}
								else
								{
									if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
									{
										require('/lib/session').session();
										destroy_preconfirmation();
									}
									else
									{
										require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
									}
								}
							}
					
							function xhrError(e) {
								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhr = null;
							}
						}
						
						if(e.source.sel === 'cancel')
						{
							var alertDialog = Ti.UI.createAlertDialog({
								buttonNames:[L('btn_yes'), L('btn_no')],
								message:L('cancel_transaction')
							});
						
							alertDialog.show();
							
							alertDialog.addEventListener('click', function(e){
								alertDialog.hide();
								if(e.index === 0 || e.index === "0")
								{
									require('/utils/RemoveViews').removeAllScrollableViews();
									destroy_preconfirmation();
									alertDialog = null;
								}
							});
						}
					});
					
					
					_obj.mainView.add(_obj.paymentView);
					_obj.mainView.add(_obj.tblPaymentDetails);
					
					_obj.convertedView.add(_obj.lblAmountConvertedHeader);
					_obj.convertedView.add(_obj.lblAmountConverted);
					_obj.mainView.add(_obj.convertedView);
					_obj.mainView.add(_obj.tblConverted);
					
					if((origSplit[0] === 'US'&& ((conf['paymode'] === 'ACHGFXFRT') || (conf['paymode'] === 'WIREFRT'))) || (origSplit[0] === 'UK' && (conf['paymode'] === 'DEBFRT')))
					{
						if(_obj.isFRT === 'Y')
						{
							_obj.SwachhBharatView.add(_obj.lblSwachhBharatHeader);
							_obj.SwachhBharatView.add(_obj.lblSwachhBharatAmount);
							_obj.mainView.add(_obj.SwachhBharatView);
						}
					}
					
					if(origSplit[0] === 'AUS' && ((conf['paymode'] === 'POLIFRT') || (conf['paymode'] === 'POLIFR')))
					{
						if(_obj.isFRT === 'Y')
						{
							_obj.SwachhBharatView.add(_obj.lblSwachhBharatHeader);
							_obj.SwachhBharatView.add(_obj.lblSwachhBharatAmount);
							_obj.mainView.add(_obj.SwachhBharatView);
						}
					}
					
					
				//	if(_obj.isFRT === 'N')
					//{
					  if((origSplit[0] === 'AUS' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'POLI'))) ||(origSplit[0] === 'UK' && ((conf['paymode'] === 'WIRE') || (conf['paymode'] === 'DEB')/*||(conf['paymode'] === 'CIP')*/)))
					{
						
					 	_obj.recipientAmountToView.add(_obj.lblRecipientAmountToHeader);
						_obj.recipientAmountToView.add(_obj.lblRecipientAmountTo);
						_obj.mainView.add(_obj.recipientAmountToView);
				   //}
					}else //if(_obj.isFRT === 'Y')
					{
						_obj.recipientAmountView.add(_obj.lblRecipientAmountHeader);
						_obj.recipientAmountView.add(_obj.lblRecipientAmount);
						_obj.mainView.add(_obj.recipientAmountView);
					}
					
					//_obj.summaryView.add(_obj.lblSummary);
					//_obj.summaryView.add(_obj.lblSummaryReceipt);
					//_obj.mainView.add(_obj.summaryView);
					//_obj.indicatorView.add(_obj.imgIndicator);
					//_obj.mainView.add(_obj.indicatorView);
					_obj.mainView.add(_obj.tblSummary);
					
					if(origSplit[0] === 'US')
					{
						_obj.infoTextView.add(_obj.imgInfo);
						_obj.infoTextView.add(_obj.lblInfo);
						_obj.mainView.add(_obj.infoTextView);
					}
					
					if(origSplit[0] === 'UK')
					{
						_obj.infoTextView.add(_obj.imgInfo);
						_obj.infoTextView.add(_obj.lblInfo);
						_obj.mainView.add(_obj.infoTextView);
					}
					
					if(countryName[1] === 'EURO' || countryName[1] === 'Euro' || countryName[1] === 'euro')
					{
						_obj.infoTextView.add(_obj.imgInfo);
						_obj.infoTextView.add(_obj.lblInfo);
						_obj.mainView.add(_obj.infoTextView);
					}
					
					if(_obj.isFRT === 'Y' && origSplit[0] === 'US')
					{
						_obj.mainView.add(_obj.btnServiceTax);
					}
					
					if(origSplit[0] === 'US')
					{
						_obj.termsView.add(_obj.imgTerms);
						_obj.termsView.add(_obj.lblTerms);
						_obj.termsView.add(_obj.lblTerms1);
						_obj.mainView.add(_obj.termsView);
					}
					else if (origSplit[0] === 'UK')
					{
						if(conf['paymodeshort'] == 'DEB' || conf['paymodeshort'] == 'DEBFRT')
						{
							_obj.termsView.add(_obj.imgTerms);
							_obj.termsView.add(_obj.lblTerms);
							_obj.mainView.add(_obj.termsView);
						}
					}
					
					_obj.mainView.add(_obj.btnSubmit);
					
					_obj.btnSubmit.addEventListener('click',function(e){
					
						if(origSplit[0] === 'US')
						{
							if(_obj.terms === 'N')
							{
								require('/utils/AlertDialog').showAlert('','Please agree to the terms and conditions to proceed',[L('btn_ok')]).show();
					    		return;
							}
						}
						
						if(origSplit[0] === 'UK')
						{
							if(conf['paymodeshort'] == 'DEB' || conf['paymodeshort'] == 'DEBFRT')
							{
								if(_obj.terms === 'N')
								{
									require('/utils/AlertDialog').showAlert('','Please agree to the terms and conditions to proceed',[L('btn_ok')]).show();
						    		return;
								}
							}
						}
						
						Ti.API.info("conf is:"+ conf[0]);
						require('/js/transfer/ConfirmationModal').ConfirmationModal(conf);
						destroy_preconfirmation();
					});
				}
				
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_preconfirmation();
					}
					else
					{
						var alertDialog = Ti.UI.createAlertDialog({
							buttonNames:[L('btn_ok')],
							message:e.result.message
						});
					
						alertDialog.show();
						
						alertDialog.addEventListener('click', function(e){
							alertDialog.hide();
							if(e.index === 0 || e.index === "0")
							{
								require('/utils/RemoveViews').removeAllScrollableViews();
								destroy_preconfirmation();
								alertDialog = null;
							}
						});
					}
				}
				}catch(e){
					
					Ti.API.info("Exception is:---",e);
				}
			}
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
	
	preconf();
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('cancel_transaction')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				require('/utils/RemoveViews').removeAllScrollableViews();
				destroy_preconfirmation();
				alertDialog = null;
			}
		});
	});
	
	_obj.winPreConfirmation.addEventListener('androidback',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('cancel_transaction')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				require('/utils/RemoveViews').removeAllScrollableViews();
				destroy_preconfirmation();
				alertDialog = null;
			}
		});
	});
	
	function destroy_preconfirmation()
	{
		try{
			
			require('/utils/Console').info('############## Remove preconf start ##############');
			
			_obj.winPreConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winPreConfirmation);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_preconfirmation',destroy_preconfirmation);
			require('/utils/Console').info('############## Remove preconf end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_preconfirmation', destroy_preconfirmation);
}; // Login()
