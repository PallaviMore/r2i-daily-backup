exports.EditTransactionModal = function(conf)
{
	require('/lib/analytics').GATrackScreen('Edit Transaction');
	
	var _obj = {
		style : require('/styles/transfer/EditTransaction').EditTransaction, // style
		winEditTransaction : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblPaymentHeader : null,
		paymentView : null,
		tblPayment : null,
		lblTransactionHeader : null,
		transactionView : null,
		lblReceiver : null,
		imgReceiver : null,
		receiverView : null,
		borderView1 : null,
		lblPurpose : null,
		imgPurpose : null,
		purposeView : null,
		borderView2 : null,
		txtAmount : null,
		borderView3 : null,
		lblExchangeRate : null,
		btnSendMoney : null,
		
		purposeOptions : [],
		receiverOptions : [],
		receiverDetails : [],
		receiverSel : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winEditTransaction = Ti.UI.createWindow(_obj.style.winEditTransaction);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winEditTransaction);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Edit Transaction';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.paymentView = Ti.UI.createView(_obj.style.paymentView);
	_obj.lblPaymentHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
	_obj.lblPaymentHeader.text = 'Payment Details';
	
	_obj.tblPayment = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblPayment.height = 0;
	
	for(var i=0; i<=3; i++)
	{
		var rowPay = Ti.UI.createTableViewRow({
			top : 0,
			left : 0,
			right : 0,
			height : 60,
			backgroundColor : 'transparent',
			className : 'bank_account_details'
		});
		
		if(TiGlobals.osname !== 'android')
		{
			rowPay.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
		}
		
		var lblKeyPay = Ti.UI.createLabel({
			top : 10,
			left : 20,
			height : 15,
			width:Ti.UI.SIZE,
			textAlign: 'left',
			font : TiFonts.FontStyle('lblNormal12'),
			color : TiFonts.FontStyle('greyFont')
		});
		
		var lblValuePay = Ti.UI.createLabel({
			top : 30,
			height : 20,
			left : 20,
			width:Ti.UI.SIZE,
			textAlign: 'left',
			font : TiFonts.FontStyle('lblNormal14'),
			color : TiFonts.FontStyle('blackFont')
		});
		
		switch(i)
		{
			case 0:
				lblKeyPay.text = 'Paymode';
				lblValuePay.text = conf['paymode'];
				
				rowPay.add(lblKeyPay);
				rowPay.add(lblValuePay);
				_obj.tblPayment.appendRow(rowPay);
				_obj.tblPayment.height = parseInt(_obj.tblPayment.height + 60);
			break;
			
			case 1:
				lblKeyPay.text = 'Bank Name';
				lblValuePay.text = conf['bankName'];
				
				rowPay.add(lblKeyPay);
				rowPay.add(lblValuePay);
				_obj.tblPayment.appendRow(rowPay);
				_obj.tblPayment.height = parseInt(_obj.tblPayment.height + 60);
			break;
			
			case 2:
				lblKeyPay.text = 'Account No.';
				lblValuePay.text = conf['accountNo'];
				
				rowPay.add(lblKeyPay);
				rowPay.add(lblValuePay);
				_obj.tblPayment.appendRow(rowPay);
				_obj.tblPayment.height = parseInt(_obj.tblPayment.height + 60);	
			break;
			
			case 3:
				lblKeyPay.text = 'Account Id.';
				lblValuePay.text = conf['accountId'];
				
				rowPay.add(lblKeyPay);
				rowPay.add(lblValuePay);
				_obj.tblPayment.appendRow(rowPay);
				_obj.tblPayment.height = parseInt(_obj.tblPayment.height + 60);
			break;
		}
		
	}
	
	_obj.transactionView = Ti.UI.createView(_obj.style.paymentView);
	_obj.lblTransactionHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
	_obj.lblTransactionHeader.text = 'Enter Transaction Details';
	
	_obj.receiverView = Ti.UI.createView(_obj.style.ddView);
	_obj.lblReceiver = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblReceiver.text = conf['receiverNickName'];
	
	_obj.receiverSel = conf['receiverNickName'] +'~' + conf['receiverParentChildflag'] +'~' + conf['receiverOwnerId'] +'~' + conf['receiverLoginid'] +'~' + conf['receiverFName'] +'~' + conf['receiverLName'];
	_obj.imgReceiver = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.purposeView = Ti.UI.createView(_obj.style.ddView);
	_obj.lblPurpose = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblPurpose.text = conf['purpose'];
	_obj.imgPurpose = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView2 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneAmount = Ti.UI.createButton(_obj.style.done);
	_obj.doneAmount.addEventListener('click',function(){
		_obj.txtAmount.blur();
	});
	
	_obj.txtAmount = Ti.UI.createTextField(_obj.style.txtAmount);
	_obj.txtAmount.hintText = 'Send Amount ('+origSplit[1]+')';
	_obj.txtAmount.value = conf['amount'];
	
	_obj.txtAmount.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtAmount.keyboardToolbar = [_obj.doneAmount];
	
	_obj.borderView3 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtAmount.addEventListener('change',function(e){
		if(_obj.txtAmount.value === '')
		{
			_obj.lblExchangeRate.text = '1 '+origSplit[1]+' = --.-- ' + destSplit[1];
		}
		else if((parseInt(_obj.txtAmount.value) < TiGlobals.minExchangeValue))
		{
			if(_obj.txtAmount.value.length > 1)
			{
				_obj.lblExchangeRate.text = 'Value cannot be less than '+TiGlobals.minExchangeValue;
			}
		}
		else if((parseInt(_obj.txtAmount.value) > TiGlobals.maxExchangeValue))
		{
			_obj.lblExchangeRate.text = 'Value cannot be greater than '+TiGlobals.maxExchangeValue;
		}
		else
		{
			getExchangeRate(conf['paymode'],_obj.txtAmount.value);	
		}
	});
	
	function getExchangeRate(paymode,amt)
	{
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETEXCHANGERATE",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'",'+
				'"paymodeCode":"'+paymode+'",'+
				'"amount":"'+amt+'",'+
				'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(e.result.responseFlag === "S")
			{
				require('/utils/Console').info(e.result);
				_obj.lblExchangeRate.text = '1 '+origSplit[1]+' = ' + e.result.exchangeRate + ' ' + destSplit[1];
			}
			xhr = null;
		}

		function xhrError(e) {
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.purposeView.addEventListener('click',function(e){
			
		var purposeOptions = [];
		var pdataSplit = conf['purposeData'].split(',');
		
		for(var i=0; i<pdataSplit.length-1;i++)
		{
			purposeOptions[i] = pdataSplit[i];
		}
		
		var optionDialogPurpose = require('/utils/OptionDialog').showOptionDialog('Purpose of Remittance',purposeOptions,-1);
		optionDialogPurpose.show();
		
		setTimeout(function(){
			activityIndicator.hideIndicator();
		},200);
		
		optionDialogPurpose.addEventListener('click',function(evt){
			if(optionDialogPurpose.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblPurpose.text = optionDialogPurpose.options[evt.index];
				optionDialogPurpose = null;
				purposeOptions = null;
				pdataSplit = null;
			}
			else
			{
				_obj.lblPurpose.text = 'Purpose of Remittance*';
				optionDialogPurpose = null;
				purposeOptions = null;
				pdataSplit = null;
			}
		});
	});	
	
	_obj.receiverView.addEventListener('click',function(e){
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETTRANSACTIONINFO",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"payModeCode":"",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			_obj.receiverOptions = [];
			_obj.receiverDetails = [];
			if(e.result.responseFlag === "S")
			{
				try{ // if not added
					for(var i=0; i<e.result.receiverData.length; i++)
					{
						_obj.receiverOptions.push(e.result.receiverData[i].nickName);
						_obj.receiverDetails.push(e.result.receiverData[i].nickName +'~' + e.result.receiverData[i].parentChildFlag +'~' + e.result.receiverData[i].ownerId +'~' + e.result.receiverData[i].loginId +'~' + e.result.receiverData[i].firstName +'~' + e.result.receiverData[i].lastname);
					}
				}catch(e){
					
				}
				
				_obj.receiverOptions.push('ADD RECEIVER');
				
				var optionDialogReceiver = require('/utils/OptionDialog').showOptionDialog('Receiver',_obj.receiverOptions,-1);
				optionDialogReceiver.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogReceiver.addEventListener('click',function(evt){
					if(optionDialogReceiver.options[evt.index] !== L('btn_cancel'))
					{
						if(optionDialogReceiver.options[evt.index] === 'ADD RECEIVER')
						{
							require('/js/recipients/AddRecipientModal').AddRecipientModal();
						}
						else
						{
							_obj.lblReceiver.text = optionDialogReceiver.options[evt.index];
							_obj.receiverSel = _obj.receiverDetails[evt.index];
							optionDialogReceiver = null;
						}
					}
					else
					{
						optionDialogReceiver = null;
						_obj.lblReceiver.text = 'Select/Add Receiver*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_edittransaction();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.lblExchangeRate = Ti.UI.createLabel(_obj.style.lblExchangeRate); 
	_obj.lblExchangeRate.text = conf['exchangeRate'];
	
	_obj.btnSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
	_obj.btnSendMoney.title = 'SEND MONEY NOW';
	
	_obj.btnSendMoney.addEventListener('click',function(e){
		
		// Receiver
		if(_obj.lblReceiver.text === 'Select/Add Receiver*')
		{
			require('/utils/AlertDialog').showAlert('','Please select a receiver',[L('btn_ok')]).show();
			return;		
		}
		else
		{
			var receiverSplit = _obj.receiverSel.split('~');
			
			conf['receiverNickName'] = receiverSplit[0];
			conf['receiverParentChildflag'] = receiverSplit[1];
			conf['receiverOwnerId'] = receiverSplit[2];
			conf['receiverLoginid'] = receiverSplit[3];
			conf['receiverFName'] = receiverSplit[4];
			conf['receiverLName'] = receiverSplit[5];
		}
		
		// Purpose
		if(_obj.lblPurpose.text === 'Purpose of Remittance*')
		{
			require('/utils/AlertDialog').showAlert('','Please select purpose of remittance',[L('btn_ok')]).show();
			return;		
		}
		else
		{
			conf['purpose'] = _obj.lblPurpose.text;
		}
		
		// Amount
		if(_obj.txtAmount.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please add an amount to be transferred',[L('btn_ok')]).show();
			_obj.txtAmount.value = '';
			_obj.txtAmount.focus();
    		return;		
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAmount.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Amount',[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtAmount.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Amount',[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else if(_obj.txtAmount.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Amount must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else if(parseInt(_obj.txtAmount.value) < parseInt(_obj.minExchange))
		{
			require('/utils/AlertDialog').showAlert('','Please add an amount greater than '+_obj.minExchange,[L('btn_ok')]).show();
    		_obj.txtAmount.focus();
			return;
		}
		else if(parseInt(_obj.txtAmount.value) > parseInt(_obj.maxExchange))
		{
			require('/utils/AlertDialog').showAlert('','Please add an amount less than '+_obj.maxExchange,[L('btn_ok')]).show();
    		_obj.txtAmount.focus();
			return;
		}
		else if((_obj.fxfwVoucherSel === 1 && origSplit[0] === 'UK') && (parseInt(_obj.txtAmount.value) < parseInt(_obj.freewayMinAmt)))
		{
			require('/utils/AlertDialog').showAlert('','Freeway program is applicable on transactions above ' + _obj.freewayMinAmt,[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else
		{
			conf['amount'] = _obj.txtAmount.value;
			conf['exchangeRate'] = _obj.lblExchangeRate.text;
		}
		
		require('/utils/Console').info( 
		'paymode = ' + conf['paymode'] + 
		'\naccountId = ' + conf['accountId'] + 
		'\nbankName = ' + conf['bankName'] +
		'\nbankBranch = ' + conf['bankBranch'] +
		'\naccountNo = ' + conf['accountNo'] + 
		'\nreceiverNickName = ' + conf['receiverNickName'] +
		'\nreceiverOwnerId = ' + conf['receiverOwnerId'] +
		'\nreceiverLoginid = ' + conf['receiverLoginid'] +
		'\nreceiverFName = ' + conf['receiverFName'] +
		'\nreceiverLName = ' + conf['receiverLName'] +
		'\nreceiverParentChildflag = ' + conf['receiverParentChildflag'] +
		'\npurpose = ' + conf['purpose'] + 
		'\namount = ' + conf['amount'] + 
		'\nexchangeRate = ' + conf['exchangeRate'] +
		'\npromoCode = ' + conf['promoCode'] + 
		'\nagentSpecialCode = ' + conf['agentSpecialCode'] + 
		'\nmorRefferalAmnt = ' + conf['morRefferalAmnt'] + 
		'\nmobileAlerts = ' + conf['mobileAlerts'] + 
		'\ntransactionInsurance = ' + conf['transactionInsurance'] + 
		'\nfxVoucherRedeem = ' + conf['fxVoucherRedeem'] + 
		'\nprogramType = ' + conf['programType'] + 
		'\nvoucherCode = ' + conf['voucherCode'] + 
		'\nvoucherQty = ' + conf['voucherQty'] +
		'\ntxnRefId = ' + conf['txnRefId'] +
		'\ncontactDate = ' + conf['contactDate'] + 
		'\ntimeFrom = ' + conf['timeFrom'] + 
		'\ntimeTo = ' + conf['timeTo'] + 
		'\ntimeFrom1 = ' + conf['timeFrom1'] + 
		'\ntimeTo1 = ' + conf['timeTo1']);
		
		require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
		destroy_edittransaction();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	
	_obj.paymentView.add(_obj.lblPaymentHeader);
	_obj.mainView.add(_obj.paymentView);
	_obj.mainView.add(_obj.tblPayment);
	
	_obj.transactionView.add(_obj.lblTransactionHeader);
	_obj.mainView.add(_obj.transactionView);
	
	_obj.receiverView.add(_obj.lblReceiver);
	_obj.receiverView.add(_obj.imgReceiver);
	_obj.mainView.add(_obj.receiverView);
	_obj.mainView.add(_obj.borderView1);
	_obj.purposeView.add(_obj.lblPurpose);
	_obj.purposeView.add(_obj.imgPurpose);
	_obj.mainView.add(_obj.purposeView);
	_obj.mainView.add(_obj.borderView2);
	_obj.mainView.add(_obj.txtAmount);
	_obj.mainView.add(_obj.borderView3);
	_obj.mainView.add(_obj.lblExchangeRate);
	_obj.mainView.add(_obj.btnSendMoney);
	_obj.globalView.add(_obj.mainView);
	_obj.winEditTransaction.add(_obj.globalView);
	_obj.winEditTransaction.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('cancel_transaction')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
				destroy_edittransaction();
				alertDialog = null;
			}
		});
	});
	
	_obj.winEditTransaction.addEventListener('androidback',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('cancel_transaction')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
				destroy_edittransaction();
				alertDialog = null;
			}
		});
	});
	
	function destroy_edittransaction()
	{
		try{
			
			require('/utils/Console').info('############## Remove Edit Txn start ##############');
			
			_obj.winEditTransaction.close();
			require('/utils/RemoveViews').removeViews(_obj.winEditTransaction);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_edittransaction',destroy_edittransaction);
			require('/utils/Console').info('############## Remove Edit Txn end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_edittransaction', destroy_edittransaction);
}; // EditTransactionModal()