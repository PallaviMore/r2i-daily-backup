/*global exports,require*/
var _ = require('/utils/Underscore');

exports.MyAccount = {
	globalView : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgBack : {
		left:0,
		image:'/images/back.png',
	    width:35,
	    height:40
	},
	tableView : {
		top:40,
		left:0,
		bottom:TiGlobals.osname === 'android' ? 0 : -20,
		right:0,
		scrollable:true,
		showVerticalScrollIndicator:true,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#efeff0',
		separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
		backgroundColor:'transparent',
		footerTitle:TiGlobals.osname === 'android' ? null : '' // Fix for empty rows in iOS
	},
	
};