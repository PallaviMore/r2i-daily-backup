/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:5,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	top:40,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

var imgRadio = {
	image:'/images/radio_unsel.png',
    width:40,
    height:40
};

exports.VerifyBankAccount = {
	winVerifyBankAccount : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	
	bankAccountView : _.defaults({}, stepView),
	tableView : {
		top:0,
		left:0,
		bottom:0,
		right:0,
		scrollable:false,
		showVerticalScrollIndicator:false,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#eee',
		backgroundColor:'transparent'
	},
	txtAmount : _.defaults({top:5,right:20}, textField),
	lblNote : _.defaults({top:20,left:20,height:Ti.UI.SIZE,width:Ti.UI.SIZE,font:TiFonts.FontStyle('lblBold12')}, label),
	lblPt : _.defaults({top:10,left:20,height:Ti.UI.SIZE,right:20,font:TiFonts.FontStyle('lblNormal12'),color:TiFonts.FontStyle('greyFont'),}, label),
	btnSubmit : _.defaults({top:20}, button),
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};