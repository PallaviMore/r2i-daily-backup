/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

exports.RegiSuccess = {
	winRegiSuccess : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	mainView : {
		top:40,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
		scrollType:'vertical',
		showVerticalScrollIndicator:true
	},
	
	infoMail: {
		   top : 10,
		   left : 20,
		   height : 15,
		   width:Ti.UI.SIZE,
		   textAlign: 'left',
		   font : TiFonts.FontStyle('lblNormal14'),
		   color:TiFonts.FontStyle('redFont'),
				},
					
	lblAddressHeader : _.defaults({top:15,height:Ti.UI.SIZE,left:20,right:20,color:'#ed1b24'}, label),
	lblAddress : _.defaults({top:5,height:Ti.UI.SIZE,left:20,right:20,font:TiFonts.FontStyle('lblNormal12')}, label),
	
	lblDateHeader : _.defaults({top:15,height:Ti.UI.SIZE,left:20,right:20,color:'#ed1b24'}, label),
	lblDate : _.defaults({top:5,height:Ti.UI.SIZE,left:20,right:20,font:TiFonts.FontStyle('lblNormal12')}, label),
	
	
};
