/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont')
};

var optionsView = {
	top:15,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	top:0,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.TransferMoney = {
	transferView : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF'
	},
	mainView : {
		top:0,
		bottom:0,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgBack : {
		left:0,
		image:'/images/back.png',
	    width:35,
	    height:40
	},
	tabView : {
		top:40,
		height:40,
		left:0,
		right:0,
		backgroundColor:'#d1d1d1',
		layout:'horizontal',
		horizontalWrap:false
	},
	tabSelView : {
		top:40,
		left:0,
		height:2,
		width:'30%',
		zIndex:10,
		backgroundColor:'#FF243A',
		zIndex:2
	},
	lblQuickPay : {
		left:0,
		height:40,
		width:'30%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('whiteFont'),
		backgroundColor:'#6F6F6F',
		sel:'quick'
	},
	lblOneClickPay : {
		left:1,
		height:40,
		width:'30%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('blackFont'),
		backgroundColor:'#E9E9E9',
		sel:'one'
	},
	lblSavedTransactions : {
		left:1,
		height:40,
		width:'40%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('blackFont'),
		backgroundColor:'#E9E9E9',
		sel:'saved'
	},
	tabSelIconView : {
		top:80,
		left:0,
		height:7,
		width:'30%',
		zIndex:10
	},
	imgTabSel : {
		image:'/images/tab_sel.png',
		top:0,
		height:7,
		width:15
	},
	scrollableView : {
		left:0,
		right:0,
		top:80,
		bottom:0,
		disableBounce:true,
		showPagingControl:false,
		backgroundColor:'#FFF'
	},
	quickPayView : _.defaults({}, stepView),
	oneClickPayView : _.defaults({}, stepView),
	savedTransactionView : _.defaults({}, stepView),
	
	/////////// Quick Pay ////////////
	
	lblPaymentHeader : {
		top:20,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('redFont'),
	},
	
	ddView : _.defaults({}, optionsView),
	lblDD : _.defaults({top:0,left:0,right:20}, textField),
	imgDD : _.defaults({right:0}, imgArrow),
	
	paymentDetailsView : {
		top:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical'
	},
	
	transactionDetailsView : {
		top:20,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
		backgroundColor:'#f0f0f0'
	},
	lblTransactionHeader : {
		top:20,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('redFont'),
	},
	txtAmount : _.defaults({top:15,right:20}, textField), 
	lblExchangeRate : {
		top:15,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('redFont'),
	},
	lblNote : _.defaults({top:20,left:20,height:Ti.UI.SIZE,width:Ti.UI.SIZE,font:TiFonts.FontStyle('lblBold12')}, label),
	lblPt : _.defaults({top:10,left:20,height:Ti.UI.SIZE,right:20,font:TiFonts.FontStyle('lblNormal12')}, label),
	
	// Other Bank Modal
	
	otherBankView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		scrollType:'vertical',
		showVerticalScrollIndicator:true,
		zIndex:100
	}, 
	otherBankMainView : {
		backgroundColor:'#6f6f6f',
		height:Ti.UI.SIZE,
		left:20,
		right:20,
		borderRadius:5,
		layout:'vertical'
	},
	txtOtherBank : _.defaults({top:15,bottom:15,left:20,right:20,backgroundColor:'#FFF'}, textField),
	btnView : {
		top:15,
		height:35,
		left:20,
		right:20,
		bottom:15
	},
	btnCancel : {
		left:'0dp',
		height:35,
		width:'48%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold14'),
		color:TiFonts.FontStyle('whiteFont'),
		borderRadius:3,
		backgroundColor:'#202020'
	},
	btnProceed : {
		right:'0dp',
		height:35,
		width:'48%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold14'),
		color:TiFonts.FontStyle('whiteFont'),
		borderRadius:3,
		backgroundColor:'#f4343b'
	},
	
	btnSendMoney : _.defaults({}, button),
	collapsibleView : {
		top:0,
		height:50,
		left:0,
		right:0,
		backgroundColor:'#6f6f6f'
	},
	lblCollapsible : _.defaults({left:20,height:Ti.UI.SIZE,width:Ti.UI.SIZE,font:TiFonts.FontStyle('lblBold14'),color:TiFonts.FontStyle('whiteFont')}, label),
	imgCollapsible : {
		image:'/images/transfer_show.jpg',
		right:0,
	    width:36,
	    height:50
	},
	promoView : {
		top:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical'
	},
	lblPromoHead : _.defaults({top:0,left:20,right:20,height:45,textAlign:'center',font:TiFonts.FontStyle('lblNormal14'),color:TiFonts.FontStyle('redFont')}, label),
	promoSelView : {
		top:0,
		height:40,
		left:0,
		right:0
	},
	lblPromoType : _.defaults({left:20,right:20,height:Ti.UI.SIZE,font:TiFonts.FontStyle('lblNormal14'),color:TiFonts.FontStyle('blackFont')}, label),
	imgPromoCheck : {
		image:'/images/black_radio_unsel.png',
	    width:18,
	    height:18,
	    right:20
	},
	txtPromoCode : _.defaults({top:0,right:20}, textField),
	
	// FW View
	
	fwVoucherView : {
		top:5,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		bottom:10
	},
	imgFW : {
		top:0,
		left:0,
		image:'/images/checkbox_unsel.png',
	    width:20,
	    height:20
	},
	lblFW : {
		top:0,
		left:30,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	
	// FX View
	
	lblFXHeader : {
		top:10,
		height:Ti.UI.SIZE,
		left:10,
		right:10,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont')
	},
	voucherView : {
		top:0,
		left:0,
		right:0,
		layout:'vertical'
	},
	
	// MOR
	
	morRateView : {
		top:10,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:true
	}, 
	lblMOR1 : {
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont'),
	},
	lblMOR2 : {
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('redFont'),
	},
	lblMOR3 : {
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont'),
	},
	txtRedeemAmount : _.defaults({top:15,right:20}, textField),
	lblMORMax : {
		top: 10,
		left:20,
		right:20,
		bottom:10,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal10'),
		color:TiFonts.FontStyle('greyFont'),
	}, 
	// Assurance
	assuranceView : {
		top:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical'
	},
	lblAssuranceHead : _.defaults({top:20,left:20,right:20,height:Ti.UI.SIZE,textAlign:'center',font:TiFonts.FontStyle('lblNormal14'),color:TiFonts.FontStyle('redFont')}, label),
	lblAssuranceTxt : {
		top: 10,
		left:20,
		right:20,
		bottom:10,
		height:Ti.UI.SIZE,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	
	assuranceInfoView : {
		top:0,
		height:35,
		left:0,
		right:0,
		backgroundColor:'#f0f0f0'
	},
	assuranceInfoMainView : {
		top:0,
		height:35,
		width:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:true
	},
	lblAssuranceInfoTxt : {
		left:0,
		width:Ti.UI.SIZE,
		height:35,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont'),
	},
	lblMobileAlerts : {
		width:Ti.UI.SIZE,
		height:45,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('redFont'),
	}, 
	mobileAlertsView : {
		top:10,
		left:20,
		right:20,
		bottom:10,
		height:Ti.UI.SIZE
	},
	imgMobileAlerts : {
		top:0,
		left:0,
		image:'/images/checkbox_unsel.png',
	    width:20,
	    height:20
	},
	lblMobileAlertsTxt : {
		top:0,
		left:30,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont'),
	},
	
	// MOR
	
	MORBannerView : {
		top:5,
		left:0,
		right:0,
		height:100,
		backgroundColor:'#343434'
	},
	imgMORBanner : {
		top:0,
		left:0,
		width:100,
		height:100,
		image:'/images/mor_banner.jpg'
	},
	MORTxtView : {
		left:100,
		right:20,
		height:Ti.UI.SIZE,
		layout:'vertical'
	},
	lblMORBannerHead : {
		top:0,
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblBold16'),
		color:TiFonts.FontStyle('redFont'),
	},
	lblMORBannerTxt : {
		top:3,
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('whiteFont'),
	},
	
	borderPromoView : {
		top:0,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#c9c9c9'
	},
	
	////////// One Click Pay/ Saved Txt ////////////
	
	lblLastTransaction : {
		top: 0,
		width:Ti.UI.SIZE,
		height:40,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('greyFont'),
	},
	tableView : {
		top:0,
		left:0,
		bottom:0,
		right:0,
		scrollable:true,
		showVerticalScrollIndicator:true,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#eee',
		backgroundColor:'transparent',
		footerTitle:TiGlobals.osname === 'android' ? null : '' // Fix for empty rows in iOS
	},
	infoTextView : {
		top:10,
		height:Ti.UI.SIZE,
		left:20,
		right:20//,
		//backgroundColor:'#e9e9e9'
	},
	imgInfo: {
		top:0,
		left:0,
		height:25,
		width:25,
		image:'/images/info_icon.png'
	},
	lblInfo : {
		top:0,
		left:30,
		right:0,
		height:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    	font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('blackFont')
	},
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};