/*global exports,require*/
var _ = require('/utils/Underscore');

var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'#000',
	borderWidth:1,
	paddingLeft:0,
	paddingRight:0,
	textAlign:'center',
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

exports.CreateMPIN = {
	winCreateMPIN : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
		scrollType:'vertical',
		showVerticalScrollIndicator:true
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	lblMPIN : {
		top : 30,
		left:20,
		right:20,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    	font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('greyFont')
	},
	mPINView : {
		top:10,
		left:20,
		width:Ti.UI.SIZE,
		height:45,
		layout:'horizontal'
	},
	txtMPIN : _.defaults({top:0}, textField),
	btnSubmit : {
		top:10,
		left:20,
		right:20,
		height:40,
		textAlign:'center',
		font:TiFonts.FontStyle('lblSwissBold14'),
		color:TiFonts.FontStyle('whiteFont'),
		backgroundColor:'#ed1b24',
		borderRadius:15
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};