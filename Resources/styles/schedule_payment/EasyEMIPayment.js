/*global exports,require*/
var _ = require('/utils/Underscore');

var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:15,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var stepView = {
	top:40,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

var imgRadio = {
	image:'/images/radio_unsel.png',
    width:40,
    height:40
};

exports.EasyEMIPayment = {
	winEasyEMIPayment : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	
	addPaymentView : _.defaults({}, stepView),
	
	topView : {
		top:15,
		bottom:15,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
	},
	lblTopHeader : _.defaults({top:0,height:Ti.UI.SIZE,left:20,right:20,font:TiFonts.FontStyle('lblNormal12'),color:TiFonts.FontStyle('greyFont')}, label),
	lblTopTxt : _.defaults({top:3,height:Ti.UI.SIZE,left:20,right:20}, label),
	
	dobView : _.defaults({top:15,left:20,right:20}, label),
	lblDOB : _.defaults({top:0,left:0,right:55}, label),
	imgDOB : {top:0,right:0,height:30,width:30,image:'/images/calendar.png'},
	
	ddView : _.defaults({}, optionsView),
	lblDD : _.defaults({top:0,left:0,right:20}, textField),
	imgDD : _.defaults({right:0}, imgArrow),
	
	txtField : _.defaults({top:15,right:20}, textField),
	
	btnSubmit : _.defaults({top:20}, button),
	
	sectionBorder : {
		top:0,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};