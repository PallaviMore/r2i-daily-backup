exports.share = function(response){
	
	var result = response;
	
	var title = result.share.title;
	var description = result.share.description;
	var link = result.share.link;
	var appicon = result.share.appicon;
	var subject = result.share.subject;
	var message = result.share.message;
	var smstext = result.share.smstext;
	
	if(Ti.Network.online)
	{
		var optionDialogShare = require('/utils/OptionDialog').showOptionDialog('Share with',TiGlobals.osname === 'android' ? ['Facebook', 'Twitter', 'Email', 'More...'] : ['Facebook', 'Twitter', 'Email', 'SMS'],0);
		optionDialogShare.show();
		
		optionDialogShare.addEventListener('click',function(evt){
			if(optionDialogShare.options[evt.index] !== 'Cancel')
			{
				switch(optionDialogShare.options[evt.index])
				{
					case 'Facebook':
						
						///// POST to User Wall
									
						var data = {
							link: link,
							name: title,
							picture: appicon,
							description: description,
							message: description,
							caption: title
						};
						
						if (!facebook.loggedIn) 
						{
							activityIndicator.showPlainIndicator(winMain);
							
							facebook.permissions = ['email'];
							facebook.forceDialogAuth = false;
							facebook.authorize();
									
							facebook.addEventListener('login', FBLogin = function(evt) {
							    if (evt.success) {
							        facebook.requestWithGraphPath('me', {}, 'GET', function(evnt) {
									    if (evnt.success) {
									    	
									    	Ti.App.Properties.setString('fbData',evnt.result);
									    	var result = JSON.parse(Ti.App.Properties.getString('fbData'));
									    	
									    	Ti.App.Properties.setString('fbId',result.id);
									    	
								    		///////// Login //////////
													
											var xmlhttp = Ti.Network.createHTTPClient();
									    	xmlhttp.onload = function()
											{require('/utils/Console').info(xmlhttp.responseText);
												var res = JSON.parse(xmlhttp.responseText);
												if(res.data.error === 0)
												{
													activityIndicator.hidePlainIndicator(winMain);
													
													// if success - login
													Ti.App.Properties.setString('userId',res.data.uId);
													Ti.App.Properties.setString('username',res.data.username);
													Ti.App.Properties.setString('email',res.data.email);
													Ti.App.Properties.setString('fname',res.data.fname);
													Ti.App.Properties.setString('lname',res.data.lname);
													Ti.App.Properties.setString('dob',res.data.user_dob);
													Ti.App.Properties.setString('gender',res.data.gender);
													Ti.App.Properties.setString('profilepic',res.data.profile_pic);
													
													facebook.permissions = ['publish_stream'];
													
													facebook.dialog('feed', data, function(epost) {
										                if(epost.success && epost.result) {
										                	if(TiGlobals.osname === 'android')
															{
										                    	require('/utils/AlertDialog').toast('Message posted on your Facebook Wall');
										                   	}
										                    else
										                    {
										                   		require('/utils/AlertDialog').showAlert('','Message posted on your Facebook Wall',['OK']).show();
										                    }
										                } else {
										                    if(epost.error) {
										                        require('/utils/AlertDialog').showAlert('',epost.error,['OK']).show();
										                    } else {
										                        //alert('User canceled dialog.');
										                    }
										                }
										            });
												}
												else
												{
													activityIndicator.hidePlainIndicator(winMain);
													
													if(res.data.message === '')
													{
														
													}
													else
													{
														require('/utils/AlertDialog').showAlert('',res.data.message,['OK']).show();
													}
													
													require('/js/RegisterModal').RegisterModal();
													
												}
												
												xmlhttp = null;
									    	};
									
											xmlhttp.onerror = function(e)
											{
												activityIndicator.hidePlainIndicator(winMain);
												xmlhttp = null;
											};
											
											require('/utils/Console').info(TiGlobals.appURL + 'registration/facebook_login.php?source=mobile&platform=' + TiGlobals.osname + '&resolution=' + screen.resolution()+'&email='+result.email);
											xmlhttp.open('POST', TiGlobals.appURL + 'registration/facebook_login.php?source=mobile&platform=' + TiGlobals.osname + '&resolution=' + screen.resolution(),true);
											xmlhttp.send({email:result.email,deviceid:Ti.App.Properties.getString('deviceToken')});
											
									    } else if (evnt.error) {
									    	activityIndicator.hidePlainIndicator(winMain);
									    	require('/utils/AlertDialog').showAlert('', evnt.error,['OK']).show();
									    } else {
									    	activityIndicator.hidePlainIndicator(winMain);
									    }
									});    
							    } else if (evt.error) {
							    	activityIndicator.hidePlainIndicator(winMain);
							    	require('/utils/AlertDialog').showAlert('', evt.error,['OK']).show();
							    } else if (evt.cancelled) {
							    	activityIndicator.hidePlainIndicator(winMain);
							    }
							    
							    facebook.removeEventListener('login', FBLogin);
							    
							});
						}
						else
						{
							facebook.permissions = ['publish_stream'];
								
							facebook.dialog('feed', data, function(epost) {
				                if(epost.success && epost.result) {
				                	if(TiGlobals.osname === 'android')
									{
				                    	require('/utils/AlertDialog').toast('Message posted on your Facebook Wall');
				                    }
				                    else
				                    {
				                   		require('/utils/AlertDialog').showAlert('','Message posted on your Facebook Wall',['OK']).show();
				                    }
				                } else {
				                    if(epost.error) {
				                        require('/utils/AlertDialog').showAlert('',epost.error,['OK']).show();
				                    } else {
				                        //alert('User canceled dialog.');
				                    }
				                }
				            });
						}
					break;
					
					case 'Twitter':
						
						twitter.share({
					        message: message,
					        success: function() {
					            if(TiGlobals.osname === 'android')
								{
									require('/utils/AlertDialog').toast('Tweet posted');
								}
								else
								{
									require('/utils/AlertDialog').showAlert('','Tweet posted',['OK']).show();
								}
					        },
					        error: function(error) {
					        	
					        	var error = JSON.parse(error);
					        	require('/utils/AlertDialog').showAlert('Twitter',error.errors[0].message,['OK']).show();
					        }
					    });
					   
					   /*var messageContent = {
							'comment' : 'LinkedIn for Drink Square ' + Math.floor((Math.random() * 1000) + 1) + '-' + Ti.Platform.osname,
							'content' : {
								'title' : 'LinkedIn Drink Square ' + Math.floor((Math.random() * 1000) + 1) + '-' + Ti.Platform.osname,
								'submitted_url' : 'http://www.appcelerator.com',
								'submitted_image_url' : 'https://static.appcelerator.com/images/header/appc_logo.png',
								'description' : 'Drinksquare LinkedIn Share ' + Math.floor((Math.random() * 1000) + 1) + '-' + Ti.Platform.osname
							},
							'visibility' : {
								'code' : 'anyone'
							}
						};

					   
					   linkedIn.shareToLinkedin({
							message : messageContent,
							success: function() {
					            alert('LinkedIn Share!');
					        },
					        error: function(error) {
					        	alert(error);
					        	//var error = JSON.parse(error);
					        	//alert(error.errors[0].message);
					        }
					    });*/
						
						
						// Whatsapp
						// Ti.Platform.openURL('whatsapp://send?text=Hello')
						
						
						
					break;
					
					case 'Email':
						
						var emailDialog = Titanium.UI.createEmailDialog();
						emailDialog.subject = subject;
						emailDialog.messageBody = message;
						emailDialog.open();
						
					break;
					
					case 'SMS':
						if(TiGlobals.osname === 'android')
						{
							var intent = Ti.Android.createIntent({
								action: Ti.Android.ACTION_SENDTO,
								data: 'smsto:'
							});
							intent.putExtra('sms_body', smstext);
							Ti.Android.currentActivity.startActivity(intent);
						}
						else
						{
							var smsDialog = require('com.omorandi').createSMSDialog({ barColor:'#336699' });
							
							if (!smsDialog.isSupported())
						    {
						        require('/utils/AlertDialog').showAlert('','The required feature is not available on your device',['OK']).show();
						    }
						    else
						    {
						        smsDialog.messageBody = smstext;
						
						        smsDialog.addEventListener('complete', function(e){
						            
						            if (e.result == smsDialog.SENT)
						            {
						                require('/utils/AlertDialog').showAlert('',e.resultMessage,['OK']).show();
						            }
						            else if (e.result == smsDialog.FAILED)
						            {
						               require('/utils/AlertDialog').showAlert('',e.resultMessage,['OK']).show();
						            }
						            else if (e.result == smsDialog.CANCELLED)
						            {
						               //don't bother
						            }
						        });
						
						        //open the SMS dialog window with slide-up animation
						        smsDialog.open({animated: true});
						    }	
						}
					break;
					
					case 'More...':
						
						var activity = Ti.Android.currentActivity;
						var intent = Ti.Android.createIntent({
					        action: Ti.Android.ACTION_SEND,
					        type: 'text/plain'
					    });
						 
						intent.putExtra(Ti.Android.EXTRA_TEXT,message);
						intent.putExtra(Ti.Android.EXTRA_SUBJECT, subject);
						activity.startActivity(Ti.Android.createIntentChooser(intent,'Share'));
						
					break;
					
				}
			}
			
			optionDialogShare = null;
		});	
	}
	else
	{
		require('/utils/AlertDialog').showAlert('','Network Connection Required',['OK']).show();
	}
};