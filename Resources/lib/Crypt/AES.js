// AES using MCRYPT

var base64 = require('/lib/Crypt/Base64');
var Mcrypt = require('/lib/Crypt/Mcrypt');
var mcrypt = new Mcrypt();

exports.Crypt = function(encrypt, text, iv, key, cipher, mode) {
	mcrypt.Crypt(null,null,null, key, cipher, mode);
};

exports.Encrypt = function(encrypt, text, iv, key, cipher, mode) {
	var block = mcrypt.get_block_size(cipher,mode);
	//require('/utils/Console').info('Block Size : ' + block);
	
	var pad = block - ((text.length) % block);
	//require('/utils/Console').info('Pad : ' + pad);
	
	var plainText = text + require('/lib/Crypt/Helper').str_repeat(require('/lib/Crypt/Helper').chr(pad), pad);
	require('/utils/Console').info('PlainText : ' + plainText);
	
	//require('/utils/Console').info('Encrypted : ' + mcrypt.Encrypt(plainText, key));
    return mcrypt.Encrypt(plainText, key);
};

exports.Decrypt = function(decrypt, encrypted, iv, key, cipher, mode) {
	
	var decrypted = mcrypt.Decrypt(base64.decode(encrypted), key);
	
	var block = mcrypt.get_block_size(cipher,mode);
	//require('/utils/Console').info('Block Size : ' + block);
    
	var pad = require('/lib/Crypt/Helper').ord(decrypted[(decrypted.length) - 1]);
    
    var len = decrypted.length;
    
    var pad = require('/lib/Crypt/Helper').ord(decrypted[len-1]);
   // require('/utils/Console').info('Dec Pad : ' + pad);
    
    return decrypted.substr(0,len-pad);
};
