function starRating(options)
{
	var rateView = null;
	var star = null;
	star = [];
	
	rateView = Ti.UI.createView(options.rateView);
	
	for(var i=1; i<=options.max; i++){
		star[i] = Ti.UI.createImageView({
			top:'0dp',
			image:options.rate_off,
			height:screen.fit(20),
			width:screen.fit(20),
			left:i === 1 ? 0 : ((i-1)* (screen.fit(20)+screen.fit(12))),
			star:i,
			max:options.max
		});
		
		rateView.add(star[i]);
	}
	
	
	function rate(opt){
		if(opt.e.type === 'click')
		{
			for(var i = opt.e.source.max; i >= 1; i--){
				star[i].image = options.rate_off;
			}
			for(var i = 1; i<=opt.e.source.star; i++){
				star[i].image = options.rate_on;
			}
		}
		else
		{
			for(var i = 1; i <= opt.e.source.max; i++) {
				star[i].image = opt.e.x >= star[i].left ? options.rate_on : options.rate_off;
		    }
		}
	};
	
	rateView.addEventListener('click', function(e) {
		rate({e:e});      
    }); 
    
    /*rateView.addEventListener('touchmove', function(e) {
        rate({e:e});      
    });*/
    
     rateView.getValue = function() {
        var value = 0;
        for(var i = 1; i <= options.max; i++) {
            value = (star[i].image === options.rate_on) ? star[i].star : value;
        }
        return value;
    };  
    
    rateView.setValue = function(value) {
        for(var i = 1; i <= value; i++) {
            star[i].image = options.rate_on;
        }
        return value;
    };        
    
	return rateView;
};

module.exports = starRating;