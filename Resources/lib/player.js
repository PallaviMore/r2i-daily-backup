exports.player = function(url,s_id){
	
	if(Ti.Network.online)
	{
		var winModal = Titanium.UI.createWindow({
			fullscreen:false,
			navBarHidden:true,
			orientationModes : [/*Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT,*/ Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT]
		});
		
		var globalView = Ti.UI.createView({
			top:'0dp',
			bottom:'0dp',
			left:'0dp',
			right:'0dp',
			backgroundColor:'#000000'
		});
		
		var btnClose = Ti.UI.createImageView({
			image:'/images/close.png',
			top:'0dp',
			right:'0dp',
			height:screen.fit(40),
			width:screen.fit(40),
			zIndex:30
		});
		
		var movie = Titanium.Media.createVideoPlayer({
			url : url,
	        backgroundColor: 'transparent',
	        autoplay : true,
	        fullscreen : false,
	    	mediaControlStyle : Titanium.Media.VIDEO_CONTROL_DEFAULT,
	    	scalingMode: Titanium.Media.VIDEO_SCALING_ASPECT_FIT
	    });
	
		globalView.add(btnClose);
		globalView.add(movie);
		winModal.add(globalView);
	    winModal.open();
	    
	    var xmlhttp = Ti.Network.createHTTPClient();
		
		xmlhttp.onload = function()
		{	
			var result = JSON.parse(xmlhttp.responseText);
			xmlhttp = null;	
		};
	
		xmlhttp.onerror = function(e)
		{
			xmlhttp = null;
		};
		
		require('/utils/Console').info(TiGlobals.appURL + 'streams/update_stream_watch_list.php?source=mobile&platform='+TiGlobals.osname+'&resolution='+screen.resolution()+'&package_id='+Ti.App.Properties.getString('packageId')+'&s_id='+s_id+'&unique_id='+Ti.App.Properties.getString('UUID'));
		xmlhttp.open('POST', TiGlobals.appURL + 'streams/update_stream_watch_list.php?source=mobile&platform='+TiGlobals.osname+'&resolution='+screen.resolution()+'&package_id='+Ti.App.Properties.getString('packageId')+'&s_id='+s_id+'&unique_id='+Ti.App.Properties.getString('UUID'),true);
		xmlhttp.send({user:Ti.App.Properties.getString('userId')});
	    
	   function closeWindow(){
	    	try{
	    		
	    		var xmlhttp = Ti.Network.createHTTPClient();
		
				xmlhttp.onload = function()
				{	
					var result = JSON.parse(xmlhttp.responseText);
					xmlhttp = null;
					
					movie.release();
		    		winModal.close();
		    		require('/utils/RemoveViews').removeViews(winModal,globalView);
		    		winModal = null;
		    		
		    		require('/utils/Console').info('Video Modal Delete End');
				};
			
				xmlhttp.onerror = function(e)
				{
					xmlhttp = null;
					movie.release();
		    		winModal.close();
		    		require('/utils/RemoveViews').removeViews(winModal,globalView);
		    		winModal = null;
		    		
		    		require('/utils/Console').info('Video Modal Delete End');
				};
				
				require('/utils/Console').info(TiGlobals.appURL + 'streams/delete_stream_watch_list.php?source=mobile&platform='+TiGlobals.osname+'&resolution='+screen.resolution()+'&package_id='+Ti.App.Properties.getString('packageId')+'&s_id='+s_id+'&unique_id='+Ti.App.Properties.getString('UUID'));
				xmlhttp.open('POST', TiGlobals.appURL + 'streams/delete_stream_watch_list.php?source=mobile&platform='+TiGlobals.osname+'&resolution='+screen.resolution()+'&package_id='+Ti.App.Properties.getString('packageId')+'&s_id='+s_id+'&unique_id='+Ti.App.Properties.getString('UUID'),true);
				xmlhttp.send({user:Ti.App.Properties.getString('userId')});
	    		require('/utils/Console').info('Video Modal Delete Start');
	        }catch(e){}
	    }
	    
	    btnClose.addEventListener('click', function (e) {
	    	closeWindow();
	    });
	    
	    function onComplete() {
			movie.fullscreen = false;
			closeWindow();
	    }
	      
	    function onFullScreen(e) {
	        if (e.entering == 0) {
	            movie.stop();
	            delay = e.duration*1000;
	            
	            //Since this is a modal window have to wait for fullscreen controller to dismiss before closing the window
	            setTimeout(function(){
	            	closeWindow();
	            },delay);
	        }
	    }
	    
	    function onError(){
		    closeWindow();
		}
	    
	    movie.addEventListener('complete', onComplete);
	    
	    movie.addEventListener('fullscreen', onFullScreen);
	    
	    movie.addEventListener('error', onError);
	 
	    var fsset = false;
	    movie.addEventListener('loadstate',function(e){
	    	if(!fsset){
	    		activityIndicator.showPlainIndicator(globalView);
	    	}else{
	    		activityIndicator.hidePlainIndicator(globalView);
	    	}
	    	
	        if(e.loadState = Ti.Media.VIDEO_LOAD_STATE_PLAYTHROUGH_OK && fsset == false) {
	        	fsset = true;
	            setTimeout(function(){movie.fullscreen = true;},300);
	        }
	    });
	    
	    winModal.addEventListener('android:back', function (e) {
	    	closeWindow();
	    });
	    
	}
	else
	{
		require('/utils/AlertDialog').showAlert('','Sorry, there are no live streams available at this moment',['OK']).show();
	}
};