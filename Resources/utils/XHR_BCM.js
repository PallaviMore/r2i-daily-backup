exports.call = function(options) {
	
	var cipher = 'rijndael-128';
	var mode = 'ecb';
	var iv = null;
	var base64 = require('/lib/Crypt/Base64');
	
	require('/lib/Crypt/AES').Crypt(null,null,null, key, cipher, mode);
	var ciphertext = require('/lib/Crypt/AES').Encrypt(true, options.post, iv, key, cipher, mode);
	ciphertext = base64.encode(ciphertext);
    require('/utils/Console').info('Encrypt : ' + ciphertext);
	
	var xhr = Titanium.Network.createHTTPClient();
	
	xhr.setTimeout(options.timeout);
	TiGlobals.osname !== 'android' ? xhr.enableKeepAlive = false : '';
	
	xhr.onload = function() {
		//require('/utils/Console').info('STATUS '+ xhr.status);
		require('/utils/Console').info(xhr.responseText);
		
		// Decryption Call
		var decrypted = require('/lib/Crypt/AES').Decrypt(false, xhr.responseText, iv, key, cipher, mode);
		require('/utils/Console').info('Decrypt : ' + decrypted);
		
		var result = JSON.parse(decrypted);
		
		// Success callback
		if(xhr.status === 200 || xhr.status === '200'){
			options.success({
	            result: JSON.parse(decrypted)
	        });
	   }else{
	   		// Handle 404 errors
		   	var Network = require('/utils/Network');
			var network = new Network(winMain, 0);
			winMain.add(network);
	   }
		
		xhr = null;
	};
	
	xhr.onerror = function() {
		// Error callback
		options.error({
            result: 'error'
        });
		
		xhr = null;
	};
	
	// Open the HTTP connection
	xhr.open('POST', options.url, true);
	xhr.setRequestHeader("Content-Type","application/json");
	//xhr.send(options.post);
	/*xhr.send(JSON.stringify({"remitWS":{
					"request":ciphertext
				}}));*/
	xhr.send(JSON.stringify({"request":ciphertext}));
};