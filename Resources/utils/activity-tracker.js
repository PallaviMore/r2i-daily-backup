//By Sanjivani on 01 Jan 17
//reference-http://stackoverflow.com/questions/34777572/detect-user-inactivity-in-titanium-appcelerator-ios-application

 Ti.API.info(" in activity tracker");
var timeoutID;
exports.didActivity = function() {
    if (timeoutID) {
        clearTimeout(timeoutID);
    }
    timeoutID = setTimeout(userIsInactive,  10 * 60 * 1000);
};

function userIsInactive() {
    // alert('WHY YOU LEAVE ME?! COME BACK!');
	if(Ti.App.Properties.getString('loginStatus') !== '0'){
	 Ti.App.Properties.setString('loginStatus','0');
	 Ti.API.info("Session expired in activity tracker");
	 var alertDialog = require('/utils/AlertDialog').showAlert('', 'Your session has expired. Please log in again.', [L('btn_ok')]);
     alertDialog.show();
     alertDialog.addEventListener('click', function(e) {
			alertDialog.hide();
			//setTimeout(function(){
			    Ti.App.fireEvent('loginData');
			    try{
			    require('/utils/RemoveViews').removeAllScrollableViews(); // Move to dashboard
			    }
			    catch(e){
			    	Ti.API.info("in catch");
			    }
				alertDialog=null;
			});

 }
	else{
		
	}
}