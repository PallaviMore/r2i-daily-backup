// Remove Elements

exports.removeViews = function(globalView)
{
	globalView.removeAllChildren(); 
	
	if (TiGlobals.osname === 'android') {
		var gc = require('prakash.garbagecollector');
		gc.callGC();
		require('/utils/Console').info("Max VM Heap Size = " + gc.getMaxVMHeapSize().toFixed(2) + " MB");
		require('/utils/Console').info("Total VM Heap Size = " + gc.getTotalHeapSize().toFixed(2) + " MB");
		require('/utils/Console').info("Allocated VM Heap Size = " + gc.getVMHeapTotalAllocatedSize().toFixed(2) + " MB");
		require('/utils/Console').info("Available VM Heap Size = " + gc.getVMHeapTotalFreeMemory().toFixed(2) + " MB");
		require('/utils/Console').info("isOutOfMemoryException = " + gc.isOutOfMemoryException());
	}
};

exports.removeAllScrollableViews = function()
{  
	try{
	var vLen = _scrollableView.views.length;
 	for( var i=vLen-1; i>=0; i--){
 		Ti.App.fireEvent('destroy_'+viewArr[i]);
    	Ti.API.info('destroy_'+viewArr[i]);
	}
	
 	try{
	winMain.remove(_scrollableView);
 	}
 	catch(e){
 		Ti.API.info("In catch of remove view js file");
 	}
	_scrollableView = Ti.UI.createScrollableView({
		left:0,
		right:0,
		top:59,
		bottom:59,
		disableBounce:true,
		scrollingEnabled:false,
		showPagingControl:false,
		backgroundColor:'transparent'
	});
	
	winMain.add(_scrollableView);
	
	emptyView();
	require('/utils/PageController').pageController('dashboard');
	}catch(e){
		Ti.API.info("In catch of remove view js file");
	}
};