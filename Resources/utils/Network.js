exports.Network = function()
{
	require('/utils/Console').info('Network ========== ' + Ti.Network.online);
	
	if(TiGlobals.osname === 'android')
	{
		require('/utils/AlertDialog').toast('Request timeout. Please try again');
	}
	else
	{
		require('/utils/AlertDialog').iOSToast('Request timeout. Please try again');
	}
};